package doll_delivery2;

import doll_delivery2.Location;

public class Edge {
	
	
	private int edgeId;
	private Location location1;
	private Location location2;
	private int distanceBetween;
	
	
	Edge(int edgeId, Location location1, Location location2, int distanceBetween)
	{
		this.edgeId = edgeId;
		this.location1 = location1;
		this.location2 = location2;
		this.distanceBetween = distanceBetween;
	}
	
	
	private Edge()
	{
		
	}
	
	
	public int getEdgeId()
	{
		return edgeId;
	}
	
	
	public Location getLocation1()
	{
		return location1;
	}
	
	
	public Location getLocation2()
	{
		return location2;
	}
	
	
	public int getDistanceBetween()
	{
		return distanceBetween;
	}
	
	
	public boolean containsLocation(Location location)
	{
		return location1.getId() == location.getId() || location2.getId() == location.getId();
	}
	
	
	public Location getOtherLocation(Location location)
	{
		return location1.getId() == location.getId() ? location2 : location1;
	}

}
