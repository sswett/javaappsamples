package doll_delivery2;


// Algorithm comes from: http://www.graph-magics.com/articles/shortest_path.php 


import java.util.ArrayList;

import doll_delivery2.Edge;
import doll_delivery2.Location;

public class RunDeliveryApp {

	private static final Location kruthika = new Location(1, "Kruthika's abode");
	private static final Location greg = new Location(2, "Greg's casa");
	private static final Location brian = new Location(3, "Brian's apartment"); 
	private static final Location mark = new Location(4, "Mark's crib"); 
	private static final Location wesley = new Location(5, "Wesley's condo"); 
	private static final Location matt =  new Location(6, "Matt's pad"); 
	private static final Location bryce = new Location(7, "Bryce's den"); 
	private static final Location cam = new Location(8, "Cam's dwelling"); 
	private static final Location kirk = new Location(9, "Kirk's farm"); 
	private static final Location nathan = new Location(10, "Nathan's flat"); 
	private static final Location craig = new Location(11, "Craig's haunt"); 
	private static final Location mike = new Location(12, "Mike's digs"); 


	private static final Location[] locations = {
		kruthika,
		greg,
		brian,
		mark,
		wesley,
		matt,
		bryce,
		cam,
		kirk,
		nathan,
		craig,
		mike
	};


	private static final Edge[] edges = {
		new Edge(1, kruthika, mark, 9),
		new Edge(2, kruthika, greg, 4),
		new Edge(3, kruthika, matt, 18),
		new Edge(4, kruthika, brian, 8),
		new Edge(5, brian, wesley, 7),
		new Edge(6, brian, cam, 17),
		new Edge(7, greg, cam, 13),
		new Edge(8, greg, mike, 19),
		new Edge(9, greg, matt, 14),
		new Edge(10, wesley, kirk, 10),
		new Edge(11, wesley, nathan, 11),
		new Edge(12, wesley, bryce, 6),
		new Edge(13, matt, mark, 19),
		new Edge(14, matt, nathan, 15),
		new Edge(15, matt, craig, 14),
		new Edge(16, mark, kirk, 9),
		new Edge(17, mark, nathan, 12),
		new Edge(18, bryce, craig, 10),
		new Edge(19, bryce, mike, 9),
		new Edge(20, mike, cam, 20),
		new Edge(21, mike, nathan, 12),
		new Edge(22, cam, craig, 18),
		new Edge(23, nathan, kirk, 3)
	};


	public static void main(String[] args) {

		String outcome = getLocationsFollowed(kruthika, craig);
		System.out.println(outcome);

	}


	private static String getLocationsFollowed(Location source, Location destination)
	{
		String result = "result will go here";

		/*
		# N - number of nodes
		# weight(i,j) - weight of the edge from node i to j ; equal to infinity if such an edge doesn't exist
		# dist(i) - the shortest path from source node to i
		# parent(i) - node that precedes i in a shortest path, used to reconstruct the shortest path
		 */

		// initialialization is already properly done in Location constructor

		source.setDist(0);

		// Location currentLocation = source;

		while (true)   // not all nodes have been processed
		{
			/*
			if (doAllUnprocessedLocationsHaveInfinityDistance())   // if "distances to all unprocessed nodes are equal to infinity" 
			{
				return "no path from source to destination node exists";
				break;
			}
			 */

			// Let i be an unprocessed node for which dist(i) is the smallest among all unprocessed nodes.
			
			// DAMN IT!  THIS PART IS CONFUSING.  ARE WE LOOKING AT ALL NODES OR JUST NEIGHBOR NODES???
			
			// Location i = getUnprocessedNeighborLocationWithSmallestDistance(currentLocation);
			Location i = getUnprocessedLocationWithSmallestDistance();   // should pick "source" first time since it's distance is 0.

			if (i == null)
			{
				return "no path from source to destination node exists";
			}

			// If i is the destination, then exit.
			if (i.getId() == destination.getId())
			{
				break;   // shortest path from source to destination has been found
			}

			i.markAsProcessed();

			// DAMN IT!  THIS PART IS CONFUSING.  ARE WE LOOKING AT ALL NODES OR JUST NEIGHBOR NODES???

			// for (Location j : getUnprocessedNeighborLocations(currentLocation))
			for (Location j : getUnprocessedLocations())
			{	
				int proposedDistance = i.getDist() + getDistanceBetweenLocationsOnEdge(i, j); 

				if (proposedDistance < j.getDist())
				{
					// a shorter path from source node to j has been found ; update it
					j.setDist(proposedDistance);
					j.setParent(i);
				}
			}
		}
		
		// # the length of the shortest path is thus dist(destination)
		// System.out.println("Length of shortest path: " + destination.getDist());
		
		// Path reconstruction:
		StringBuilder path = new StringBuilder();
		Location i = destination;
		
		while (i.getId() != source.getId())
		{
			path.insert(0, " > " + i.getName());
			i = i.getParent();
		}
		
		path.insert(0,  source.getName());
		
		path.insert(0,  "Distance: " + destination.getDist() + "; ");
		
		return path.toString();
	}


	// TODO: consider optimizing
	private static int getDistanceBetweenLocationsOnEdge(Location location1, Location location2)
	{
		for (Edge edge : edges)
		{
			if (edge.containsLocation(location1) && edge.containsLocation(location2))
			{
				return edge.getDistanceBetween();
			}
		}

		return Integer.MAX_VALUE;
	}


	// Am I really supposed to use this one (without it being a neighbor)?
	private static Location getUnprocessedLocationWithSmallestDistance()
	{
		Location result = null;
		int smallestDistance = Integer.MAX_VALUE;

		for (Location location : locations)
		{
			if (!location.isProcessed() && location.getDist() < smallestDistance)
			{
				smallestDistance = location.getDist();
				result = location;
			}
		}

		return result;
	}


	private static Location getUnprocessedNeighborLocationWithSmallestDistance(Location locationIn)
	{
		Location result = null;
		int smallestDistance = Integer.MAX_VALUE;

		for (Location neighbor : getNeighborLocations(locationIn))
		{
			if (!neighbor.isProcessed() && neighbor.getDist() < smallestDistance)
			{
				smallestDistance = neighbor.getDist();
				result = neighbor;
			}
		}

		return result;
	}


	private static ArrayList<Location> getUnprocessedNeighborLocations(Location locationIn)
	{
		ArrayList<Location> neighbors = new ArrayList<Location>();

		for (Location neighbor : getNeighborLocations(locationIn))
		{
			if (!neighbor.isProcessed())
			{
				neighbors.add(neighbor);
			}
		}

		return neighbors;
	}


	private static ArrayList<Location> getUnprocessedLocations()
	{
		ArrayList<Location> result = new ArrayList<Location>();

		for (Location location : locations)
		{
			if (!location.isProcessed())
			{
				result.add(location);
			}
		}

		return result;
	}


	private static ArrayList<Location> getNeighborLocations(Location locationIn)
	{
		ArrayList<Location> neighbors = new ArrayList<Location>();

		for (Edge edge : edges)
		{
			if (edge.containsLocation(locationIn))
			{
				neighbors.add(edge.getOtherLocation(locationIn));
			}
		}

		return neighbors;
	}


	private static boolean doAllUnprocessedLocationsHaveInfinityDistance()
	{
		for (Location location : locations)
		{
			if (!location.isProcessed() && location.getDist() != Integer.MAX_VALUE)
			{
				return false;
			}
		}

		return true;
	}


}
