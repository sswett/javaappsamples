package doll_delivery2;

public class Location {

	private int id;
	private String name;
	private int dist;
	private boolean processed;
	private Location parent;
	
	
	Location(int id, String name)
	{
		this.id = id;
		this.name = name;
		this.dist = Integer.MAX_VALUE;   // Simulates infinity
		processed = false;
		parent = null;
	}

	
	private Location()
	{
		
	}
	
	
	public int getId()
	{
		return id;
	}
	
	
	public String getName()
	{
		return name;
	}
	
	
	public int getDist()
	{
		return dist;
	}
	
	
	public boolean isProcessed()
	{
		return processed;
	}
	
	
	public Location getParent()
	{
		return parent;
	}
	
	
	protected void setDist(int dist)
	{
		this.dist = dist;
	}
	
	
	protected void setParent(Location parent)
	{
		this.parent = parent;
	}
	
	
	protected void markAsProcessed()
	{
		processed = true;
	}
	
	
}
