package numbers;

import utilities.WeirdMathUtils;

public class WeirdMathApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		for (int x = 1; x <= 10; x++)
		{
			int modifiedX;
			
			if ( x > 0 && x <= 3 )
			{
				modifiedX = getModifiedInteger(x, "add", 1);
			}
			else if ( x > 3 && x <= 6 )
			{
				modifiedX = getModifiedInteger(x, "subtract", 2);
			}
			else if ( x > 6 && x <= 9 )
			{
				modifiedX = getModifiedInteger(x, "multiply", 3);
			}
			else
			{
				modifiedX = getModifiedInteger(x, "squeeze", 4);
			}
			
			outputIntegerToPower(modifiedX, x + 1);
		}
		
	}

	
	private static void outputIntegerToPower(int number, int power)
	{
		double result = 0;
		
		boolean numberIsEven = number % 2 == 0; 
		
		if (numberIsEven)
		{
			result = WeirdMathUtils.getPower(number, power);
		}
		else
		{
			result = WeirdMathUtils.getPower(number, power, true);
		}
		
		System.out.print(Integer.toString(number) + " to the power" +
				" of " + (numberIsEven ? "" : "-") + power + " is: ");
		
		System.out.println(result);
	}

	
	private static int getModifiedInteger(int incomingInteger, 
			String action, int factor)
	{
		int result;
		
		if (action.equals("add"))
		{
			result = incomingInteger + factor;
		}
		else if (action.equals("subtract"))
		{
			result = incomingInteger - factor;
			
		}
		else if (action.equals("multiply"))
		{
			result = incomingInteger * factor;
			
		}
		else
		{
			result = incomingInteger;
		}
		
		return result;
	}
	
	
}
