package numbers;


public class TestIntDivisionApp {

public static void main(String[] args)
{

    int rowLength = 50;
    int dataLength = 112;

    int result = dataLength / rowLength;
    int mod = dataLength % rowLength;

    int rows = result + (mod > 0 ? 1 : 0);

    System.out.println("result = " + result);   // 2
    System.out.println("mod = " + mod);   // 12

    System.out.println("rows = " + rows);   // 3

}


}