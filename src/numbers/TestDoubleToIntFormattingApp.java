package numbers;

import java.text.NumberFormat;
import java.util.Locale;

public class TestDoubleToIntFormattingApp {

public static void main(String[] args)
{

    double myDouble = 12345.6789;
    double myOtherDouble = 12345.2789;
    double myPercentage = .56;
    long myMemoryUsage = 123456789012345L;

    String myOutput = String.format("%.0f", myOtherDouble);

    System.out.printf("%.0f", myDouble);
    System.out.println();
    System.out.printf("%.0f", myOtherDouble);
    System.out.println();
    System.out.printf("%.2f", myPercentage);
    System.out.println();
    System.out.println(myOutput);

    int int1 = 5;
    int int2 = 66;
    System.out.println();
    System.out.printf("%5d\n", int1);
    System.out.printf("%5d\n", int2);

    System.out.println("a" + String.format("%05d", int2) );

    System.out.println();
    NumberFormat fmt = NumberFormat.getNumberInstance(Locale.US);
    System.out.println(fmt.format(myMemoryUsage));


}


}