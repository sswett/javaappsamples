package numbers;
import java.util.*;

public class TestAPIntegerQuestionApp {

    private static ArrayList<Integer> nums = new ArrayList<Integer>();
    /** Precondition: nums.size > 0
     */

    public static void main(String[] args) {

        nums.add(new Integer(0));
        nums.add(new Integer(0));
        nums.add(new Integer(4));
        nums.add(new Integer(2));
        nums.add(new Integer(5));
        nums.add(new Integer(0));
        nums.add(new Integer(3));
        nums.add(new Integer(0));
        nums.add(new Integer(3));

        System.out.println("Original input:");
        System.out.print(nums);

        removeZeroValues();

        System.out.println("\nAfter removing 0 values (theoretically):");
        System.out.print(nums);

    }

    public static void removeZeroValues()
    {
        int k = 0;
        Integer zero = new Integer(0);

        while (k < nums.size())
        {
            if (nums.get(k).equals(zero))
            {
                nums.remove(k);
            }
            k++;
        }

    }

}