package numbers;

public class TestFloatDivisionApp
{

public static void main(String[] args)
{

    long executionStartTime = 123456789012345L;
    long executionStopTime1 = 123456789020000L;
    long executionStopTime2 = 123456789039876L;

    float diff1 = (float) (executionStopTime1 - executionStartTime) / 1000;
    float diff2 = (float) (executionStopTime2 - executionStartTime) / 1000;

    System.out.println("diff1 = " + diff1);
    System.out.println("diff2 = " + diff2);

}


}