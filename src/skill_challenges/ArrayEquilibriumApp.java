/*
 * See comment at bottom for skill challenge.
 */

package skill_challenges;

import java.util.ArrayList;
import java.util.Arrays;


public class ArrayEquilibriumApp 
{
	
	private static int[] TEST_CASE_1 = { -1, 3, -4, 5, 1, -6, 2, 1 };

	private static int[] TEST_CASE_2 = { Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE - 1 };

	private static int[] TEST_CASE_3 = { Integer.MAX_VALUE, 0, Integer.MAX_VALUE };

	private static int[] TEST_CASE_4 = { -Integer.MAX_VALUE, -Integer.MAX_VALUE, -Integer.MAX_VALUE, Integer.MAX_VALUE };

	private static int[] TEST_CASE_5 = { 500, 1, -2, -1, 2 };

	private static int[] TEST_CASE_6 = { 100 };

	private static int[] TEST_CASE_7 = { -1, 0 };

	private static int[] TEST_CASE_8 = { -1, -1, -1 };
	
	private static int[] TEST_CASE_9 = { -1, -1, 0, -3, 1 };

	private static int[] TEST_CASE_10 = { Integer.MAX_VALUE, Integer.MAX_VALUE, 0, Integer.MAX_VALUE, Integer.MAX_VALUE };
	
	// private static int[] TEST_CASE_BIG_ONES = new int[Integer.MAX_VALUE / 10000];

	
	public static void main(String[] args) 
	{
		printTestResults(TEST_CASE_1, true, false, "");
		printTestResults(TEST_CASE_2, false, false, "");
		printTestResults(TEST_CASE_3, false, false, "");
		printTestResults(TEST_CASE_4, false, false, "");
		printTestResults(TEST_CASE_5, false, false, "");
		printTestResults(TEST_CASE_6, false, false, "");
		printTestResults(TEST_CASE_7, false, false, "");
		printTestResults(TEST_CASE_8, false, false, "");
		printTestResults(TEST_CASE_9, false, false, "");
		printTestResults(TEST_CASE_10, false, false, "");

		/*
		Arrays.fill(TEST_CASE_BIG_ONES, 1);
		printTestResults(TEST_CASE_BIG_ONES, true, "TEST_CASE_BIG_ONES");
		*/
		
	}
	
	
	private static void printTestResults(
			int[] A, 
			boolean printFirstHit, 
			boolean printCaptionInsteadOfTestData, 
			String caption)
	{
		System.out.print("Test case data: ");
		System.out.println(printCaptionInsteadOfTestData ? caption + " (length " + A.length + ")" : Arrays.toString(A));
		
		ArrayList<Integer> results = getAllEquilibriumIndices(A);
		
		System.out.print("Results: ");
		System.out.println(results.toString());
		
		if (printFirstHit)
		{
			System.out.printf("First hit: %s\n", getFirstEquilibriumIndex(TEST_CASE_1));
		}
		
		System.out.println();
	}
	
	
    private static ArrayList<Integer> getAllEquilibriumIndices(int[] A) 
    {
    	ArrayList<Integer> result = new ArrayList<Integer>();
    	
        for (int x = 0; x < A.length; x++)
        {
        	int lowerSum = 0;
        	int upperSum = 0;
        	
        	if (x > 0)
        	{
        		lowerSum = getSum(A, 0, x - 1);
        	}
        	
        	if (x < A.length - 1)
        	{
        		upperSum = getSum(A, x + 1, A.length - 1);
        	}

        	if (lowerSum == upperSum)
            {
                result.add(x);
            }
        }
        
        return result;
    }
	
	
    private static int getFirstEquilibriumIndex(int[] A) 
    {
        for (int x = 0; x < A.length; x++)
        {
        	int lowerSum = 0;
        	int upperSum = 0;
        	
        	if (x > 0)
        	{
        		lowerSum = getSum(A, 0, x - 1);
        	}
        	
        	if (x < A.length - 1)
        	{
        		upperSum = getSum(A, x + 1, A.length - 1);
        	}

        	if (lowerSum == upperSum)
            {
                return x;
            }
        }
        
        return -1;
    }
    
    
    private static int getSum(int[] A, int startIndexInclusive, int endIndexInclusive)
    {
    	int result = 0;
    	
    	for (int x = startIndexInclusive; x <= endIndexInclusive; x++)
    	{
    		result += A[x];
    	}
    	
    	return result;
    }
	

}



/*

This is a demo task. You can read about this task and its solutions in this blog post.

A zero-indexed array A consisting of N integers is given. An equilibrium index of this array is any integer P such 
that 0 = P < N and the sum of elements of lower indices is equal to the sum of elements of higher indices, i.e.
 
A[0] + A[1] + ... + A[P-1] = A[P+1] + ... + A[N-2] + A[N-1].

Sum of zero elements is assumed to be equal to 0. This can happen if P = 0 or if P = N-1.

For example, consider the following array A consisting of N = 8 elements:

  A[0] = -1
  A[1] =  3
  A[2] = -4
  A[3] =  5
  A[4] =  1
  A[5] = -6
  A[6] =  2
  A[7] =  1

P = 1 is an equilibrium index of this array, because:

A[0] = -1 = A[2] + A[3] + A[4] + A[5] + A[6] + A[7]
P = 3 is an equilibrium index of this array, because:

A[0] + A[1] + A[2] = -2 = A[4] + A[5] + A[6] + A[7]
P = 7 is also an equilibrium index, because:

A[0] + A[1] + A[2] + A[3] + A[4] + A[5] + A[6] = 0
and there are no elements with indices greater than 7.

P = 8 is not an equilibrium index, because it does not fulfill the condition 0 = P < N.

Write a function:

int solution(vector<int> &A);

that, given a zero-indexed array A consisting of N integers, returns any of its equilibrium indices. The function 
should return -1 if no equilibrium index exists.

For example, given array A shown above, the function may return 1, 3 or 7, as explained above.

Assume that:

N is an integer within the range [0..100,000];
each element of array A is an integer within the range [-2,147,483,648..2,147,483,647].

Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input 
arguments).

Elements of input arrays can be modified.

 */
