package skill_challenges;

import java.util.Arrays;
import java.util.HashSet;

public class SimplifyAndSortArrayOfIntegersApp {

	
	public static void main(String[] args) {

		Integer[] unsimplifiedArray = { 5, 3, 20, 4, 3, 1, 17, 5 };
		
		Integer[] simplifiedArray = simplify(unsimplifiedArray);
		
		System.out.print("Simplified array: ");
		
		for (Integer x : simplifiedArray)
		{
			System.out.print(x + " ");
		}
		
	}

	
	private static Integer[] simplify(Integer[] inputArray)
	{
	    HashSet<Integer> set = new HashSet<Integer>();
	    
	    // Unique-ify:
	    for (int x = 0; x < inputArray.length; x++)
	    {
	        set.add(inputArray[x]);
	    }
	    
	    Integer[] array = set.toArray(new Integer[set.size()]);
	    
	    Arrays.sort(array);
	    return array;
	}

}
