package skill_challenges;


public class ListTenPrimesApp {

public static void main(String[] args)
{

    long primeNumber = 2L;

    for (int x = 0; x < 10; x++)
    {
        System.out.println(primeNumber);
        primeNumber = getNextPrime(primeNumber);
    }

}


private static long getNextPrime(long prevPrimeNumber)
{
    long primeCandidate = prevPrimeNumber;

    while (true)
    {
        primeCandidate++;

        for (long x = 2; x <= primeCandidate; x++)
        {
            if (primeCandidate % x == 0L)
            {
                if (x == primeCandidate)
                {
                    return primeCandidate;
                }
                else
                {
                    break;
                }
            }
        }

    }
}


}