package skill_challenges;

import java.util.HashMap;
import java.util.regex.Pattern;

public class RegexMatchApp {
	
    
    private static final InputAndPattern[] INPUTS_AND_PATTERNS = { 
    	new InputAndPattern("aa", "a"),   		// false 
    	new InputAndPattern("aa", "aa"),   		// true
    	new InputAndPattern("aaa", "aa"),		// false
    	new InputAndPattern("aa", "a*"),		// true
    	new InputAndPattern("aa", ".*"),		// true
    	new InputAndPattern("ab", ".*"),		// true
    	new InputAndPattern("ba", "a*"),		// false
    	new InputAndPattern("aa", "a."),		// true
    	new InputAndPattern("ba", "a."),		// false
    	new InputAndPattern("aab", "c*a*b*"),	// true -- this means c* or a* or b*
    	new InputAndPattern("aab", "c*e*b*")	// false -- this means c* or e* or b*
    	};
    
    
    private static class InputAndPattern
    {
    	private String input;
    	private String pattern;
    	
    	InputAndPattern(String input, String pattern)
    	{
    		this.input = input;
    		this.pattern = pattern;
    	}
    	
    	String getInput()
    	{
    		return input;
    	}
    	
    	String getPattern()
    	{
    		return pattern;
    	}
    }
    

	public static void main(String[] args) {
		
		for (InputAndPattern inputAndPattern : INPUTS_AND_PATTERNS)
		{
			String input = inputAndPattern.getInput();
			String pattern = inputAndPattern.getPattern();
			boolean matches = isMatch(input, pattern);
			System.out.println(String.format("Input: %s, Pattern: %s, Matches: %s", input, pattern, matches));
		}

	}
	
	
	private static boolean isMatch(String input, String pattern)
	{
		if (pattern.contains(".") || pattern.contains("*"))
		{
			// Note: "Pattern.quote(something)" is how to quote something inside a regular expression.
			
			String[] starLetters = pattern.split(Pattern.quote("*"));
			
			if (starLetters.length == 0)
			{
				return isSubMatch(input, pattern);
			}
			else
			{
				for (String starLetter : starLetters)
				{
					boolean match = isSubMatch(input, starLetter + "*");
					
					if (match)
					{
						return true;
					}
				}
			}
			
			return false;
			
		}
		else
		{
			return input.equals(pattern);
		}
	}
	
	
	private static boolean isSubMatch(String input, String pattern)
	{
		// Handle the characters before the * :
		int starPos = pattern.indexOf("*");
		int maxRightPos = starPos != -1 ? starPos - 1 : pattern.length() - 1;
		
		for (int x = 0; x <= maxRightPos; x++)
		{
			if (pattern.charAt(x) != '.' && input.charAt(x) != pattern.charAt(x) )
			{
				return false;
			}
		}
		
		// TEMPORARY:
		return true;
	}

}
