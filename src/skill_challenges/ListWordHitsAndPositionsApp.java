package skill_challenges;
import java.util.StringTokenizer;

public class ListWordHitsAndPositionsApp {

public static void main(String[] args)
{

    String text = "The story is about  the theme that  the little girl of  the Indian tribe of  the north concocted.";

    System.out.println(text);

    String searchWord = "the";

    /*
        Below:
            2nd parm = default delimiter set
            3rd parm = true forces the white space between words to be returned as tokens so that their positions
                       could be counted using token.length()
     */
    StringTokenizer st = new StringTokenizer(text, " \t\n\r\f", true);

    int hitCount = 0;
    int pos = 0;

    while (st.hasMoreTokens())
    {
        String token = st.nextToken().toLowerCase();

        if (token.equals(searchWord))
        {
            for (int x = 0; x < pos; x++) System.out.print(" ");
            
            System.out.println("^ (" + ++hitCount + "," + pos + ") ");
        }

        pos += token.length();
    }

}


}