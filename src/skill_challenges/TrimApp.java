package skill_challenges;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TrimApp {
	
	
	private static final Pattern trimmer = Pattern.compile("[^\\s]+.*[^\\s]+");
	
	
	private static enum TrimMethod
	{
		RECURSION,
		LOOP,
		REGEX
	}
	

	public static void main(String[] args) {
		
		for (TrimMethod method : TrimMethod.values())
		{
			System.out.printf("Tests using %s:\n", method.name().toLowerCase());
			displayTrimmedString(trim("    centered    ", method));
			displayTrimmedString(trim("left justified    ", method));
			displayTrimmedString(trim("    right justified", method));
			displayTrimmedString(trim("single", method));
			displayTrimmedString(trim(" ", method));
			System.out.println();
		}
		
	}
	
	
	private static void displayTrimmedString(String str)
	{
		System.out.printf("x--%s--x\n", str);
	}

	
	private static String trim(String str, TrimMethod method)
	{
		if (str == null || str.length() == 0)
		{
			return "";
		}
		
		if (method == TrimMethod.RECURSION)
		{
			return trimRecursively(str);
		}
		else if (method == TrimMethod.LOOP)
		{
			return trimUsingLoop(str);
		}
		else
		{
			return trimUsingRegex(str);
		}
	}
	
	
	private static String trimRecursively(String str)
	{
		if (str.length() == 0)
		{
			return "";
		}
		
		if (str.substring(0, 1).equals(" "))
		{
			str = str.substring(1);
			return trimRecursively(str);
		}
		else if (str.substring(str.length() - 1, str.length()).equals(" "))
		{
			str = str.substring(0, str.length() - 1);
			return trimRecursively(str);
		}
		else
		{
			return str;
		}
	}
	
	
	private static String trimUsingRegex(String str)
	{
		Matcher matcher = trimmer.matcher(str);
		String result =  matcher.find() ? matcher.group() : str;
		
		// If found match is all blanks, return empty string:
		return result.matches("\\s+") ? "" : result;
	}
	
	
	private static String trimUsingLoop(String str)
	{
		int firstNonBlank = -1;
		int lastNonBlank = -1;

		for (int x = 0; x < str.length(); x++)
		{
			boolean charIsNonBlank = !str.substring(x, x + 1).equals(" ");
			
			if (charIsNonBlank)
			{
				if (firstNonBlank == -1)
				{
					firstNonBlank = x;
				}
				
				lastNonBlank = x;
			}
		}
		
		if (firstNonBlank == -1 && lastNonBlank == -1)
		{
			return "";
		}
		
		if (firstNonBlank == -1)
		{
			firstNonBlank = 0;
		}
		
		if (lastNonBlank == -1)
		{
			lastNonBlank = str.length() - 1;
		}
		
		return str.substring(firstNonBlank, lastNonBlank + 1);
	}

}
