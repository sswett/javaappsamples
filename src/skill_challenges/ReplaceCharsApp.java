package skill_challenges;

public class ReplaceCharsApp {
	
	final static String SAMPLE_STRING = "The quick brown fox jumps over the lazy dog";

	public static void main(String[] args) {

		System.out.println("Original string:");
		System.out.println(SAMPLE_STRING);
		
		System.out.println("\nReplaced string:");
		System.out.println(replaceChar(SAMPLE_STRING, 'o', 'X'));

	}
	
	
	private static String replaceChar(String str, char find, char replace)
	{
		if (str == null || str.length() < 1)
		{
			return str;
		}
		
		StringBuilder sb = new StringBuilder();
		
		for (int x = 0; x < str.length(); x++)
		{
			char currentChar = str.charAt(x);
			
			if (currentChar == find)
			{
				sb.append(replace);
			}
			else
			{
				sb.append(currentChar);
			}
		}
		
		return sb.toString();
	}

}
