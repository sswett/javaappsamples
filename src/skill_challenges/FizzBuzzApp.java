package skill_challenges;

public class FizzBuzzApp {
	

	public static void main(String[] args) {

		for (int x = 1; x <= 15; x++)
		{
			if (isMultipleOf3and5(x))
			{
				System.out.println("FizzBuzz");
			}
			else if (isMultipleOf3(x))
			{
				System.out.println("Fizz");
			}
			else if (isMultipleOf5(x))
			{
				System.out.println("Buzz");
			}
			else
			{
				System.out.println(x);
			}
		}
	}
	
	
	private static boolean isMultipleOf3(int number)
	{
		return number % 3 == 0;
	}
	
	
	private static boolean isMultipleOf5(int number)
	{
		return number % 5 == 0;
	}
	
	
	private static boolean isMultipleOf3and5(int number)
	{
		return isMultipleOf3(number) && isMultipleOf5(number);
	}

}
