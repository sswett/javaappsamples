package skill_challenges;

import java.util.ArrayList;

public class PalindromeApp {
	
	public static final String SAMPLE1 = "abcdefghgfedcbalevel one mom madam rotor rotator pilgrim";
	public static final String SAMPLE2 = "mom one level madam rotor rotator pilgrimabcdefgfedcba";

	public static void main(String[] args) {
		
		ArrayList<String> singleWordPalindromes = getSingleWordPalindromes(SAMPLE1);
		System.out.println("Single word palindromes:");
		printPalindromes(singleWordPalindromes);

		System.out.println("\nLongest of the \"Single Word\" palindromes:");
		System.out.println(getLongestPalindrome(singleWordPalindromes));
		
		ArrayList<String> anyPalindromes = getAnyPalindromes(SAMPLE1);
		System.out.println("\nAny palindromes:");
		printPalindromes(anyPalindromes);

		System.out.println("\nLongest of the \"Any\" palindromes:");
		System.out.println(getLongestPalindrome(anyPalindromes));
	
	}
	
	
	private static ArrayList<String> getAnyPalindromes(String str)
	{
		ArrayList<String> palindromes = new ArrayList<String>();
		
		int strLength = str.length();
		
		// This technique focuses on the center letter of a palindrome, such as "v" in "level".  It looks to left and right.
		
		// First and last letter of string obviously cannot be center, so skip.
		for (int x = 1; x < strLength - 1; x++)
		{
			int longestSide = Math.max(x - 1 , strLength - x);
			
			for (int s = 1; s <= longestSide; s++)
			{
				int leftmost = x - s;
				int rightmost = x + s;
				
				if (leftmost < 0 || rightmost > strLength - 1)
				{
					break;
				}
					
				String substr = str.substring(leftmost, rightmost + 1);

				if (isPalindrome(substr))
				{
					palindromes.add(substr);
				}
				else
				{
					break;
				}
			}
		}
		
		return palindromes;
	}
	
	
	private static ArrayList<String> getSingleWordPalindromes(String str)
	{
		ArrayList<String> palindromes = new ArrayList<String>();
		
		String[] words = str.split(" ");
		
		for (String word : words)
		{
			if (isPalindrome(word))
			{
				palindromes.add(word);
			}
		}
		
		return palindromes;
	}
	
	
	private static String getLongestPalindrome(ArrayList<String> palindromes)
	{
		int longestPalindromeLocation = -1;
		int longestLength = 0;
		
		for (int x = 0; x < palindromes.size(); x++)
		{
			int palindromeLength = palindromes.get(x).length();
			
			if (palindromeLength > longestLength)
			{
				longestPalindromeLocation = x;
				longestLength = palindromeLength;
			}
		}
		
		return longestPalindromeLocation == -1 ? "" : palindromes.get(longestPalindromeLocation);
	}
	
	
	
	private static void printPalindromes(ArrayList<String> palindromes)
	{
		for (String palindrome : palindromes)
		{
			System.out.println(palindrome);
		}
	}
	
	
	private static boolean isPalindrome(String str)
	{
		String reversed = new StringBuilder(str).reverse().toString();
		return str.equals(reversed);
	}

}
