package skill_challenges;

public class RecursiveFactorialApp {

	
	public static void main(String[] args) {
		
		System.out.println("NOTE: if factorial cannot be computed for a number, -1 will be returned.\n");

		for (int x = -3; x < 15; x++)
		{
			System.out.printf("Factorial of %s = %s\n", x, getFactorial(x));
		}
	}
	
	
	// Note: recursive calls typically follow a pattern like: call > call > call > termination condition > return > return > return 
	
	private static int getFactorial(int input)
	{
		if (input < 0 || input > 13)   // Numbers over 13 are too big for int types
		{
			return -1;   // in other words, an invalid input has been given
		}
		else if (input == 0)
		{
			return 1;
		}
		else
		{
			return input * getFactorial(input - 1);
		}
	}

}
