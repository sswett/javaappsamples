package skill_challenges;

import java.util.Stack;

public class ReversePolishNotationApp {
	
	private static final String OPERATORS = "+-*/";
	private static final String[] INPUT1 = { "4", "13", "5", "/", "+" };
	private static final String[] INPUT2 = { "2", "1", "+", "3", "*" };

	
	public static void main(String[] args) {

		double answer = evaluateInput(INPUT1);
		System.out.println("Answer is: " + answer);
	}
	
	
	private static double evaluateInput(String[] inputArray)
	{
		Stack<String> stack = new Stack<String>();
		
		for (String input : inputArray)
		{
			boolean isOperator = OPERATORS.contains(input);
			
			if (isOperator)
			{
				// Operator encountered; time to get previous 2 numbers off stack, perform operation, and put result back on stack
				double a = Double.valueOf(stack.pop());
				double b = Double.valueOf(stack.pop());
				
				// Note: switch using a String is only available in JDK 1.7 or later
				switch (input)
				{
				case "+":
					stack.push(String.valueOf(a + b));
					break;
				case "-":
					stack.push(String.valueOf(b - a));
					break;
				case "*":
					stack.push(String.valueOf(a * b));
					break;
				case "/":
					stack.push(String.valueOf(b / a));
					break;
				}
			}
			else
			{
				// If a number, just push onto stack and do nothing else yet
				stack.push(input);
			}
		}
		
		double result = Double.valueOf(stack.pop());
		
		return result;
	}

}
