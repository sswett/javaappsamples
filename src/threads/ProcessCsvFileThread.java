package threads;

import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.DataInputStream;

public class ProcessCsvFileThread implements Runnable {

	private final int threadNo;
	private final boolean writeDetailsToConsole;


	public ProcessCsvFileThread(int threadNo, boolean writeDetailsToConsole)
	{
		this.threadNo = threadNo;
		this.writeDetailsToConsole = writeDetailsToConsole;
	}


	public void run()
	{
		int linesRead = 0;

		try
		{
			FileInputStream fileInputStream = new FileInputStream(ProcessCsvFileApp.PATH_TO_CSV_FILES + "csv" + threadNo + ".txt");
			DataInputStream dataInputStream = new DataInputStream(fileInputStream);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));

			/*
            I like the following approach to reading data:

			FileReader reader = new FileReader(filename);
			BufferedReader buffReader = new BufferedReader(reader);

            Uses 2 objects instead of 3
			 */

			String oneLine;

			while ((oneLine = bufferedReader.readLine()) != null)
			{
				linesRead++;
				processOneLine(oneLine, linesRead);

				/*
                // Temporarily read only a few lines
                if (linesRead >= 3)
                {
                    break;
                }
				 */
			}

			bufferedReader.close();
			dataInputStream.close();

		}

		catch (Exception e)
		{
			System.err.println("\nTHREAD# " + threadNo + " Error: " + e.getMessage() + "\n");
			e.printStackTrace();
		}

		System.out.println("\nTHREAD# " + threadNo + " FINISHED.  " + linesRead + " lines have been read.");
	}


	public void processOneLine(String oneLine, int lineNo)
	{
		// System.out.println(oneLine);
		String[] fields = oneLine.split(",");

		StringBuilder sb = new StringBuilder();
		sb.append("THREAD# ").append(threadNo).append(" LINE# ").append(lineNo).append(": ");

		/*
        // Process select fields
        sb.append(getInfoForOneField(fields[2], 2, "ID"));
        sb.append(getInfoForOneField(fields[11], 11, "Last Name"));
        sb.append(getInfoForOneField(fields[12], 12, "First Name"));
        sb.append(getInfoForOneField(fields[4], 4, "Course#"));
        sb.append(getInfoForOneField(fields[5], 5, "Course Title"));
		 */

		// Process every field
		for (int f = 0; f < fields.length; f++)
		{
			sb.append(getInfoForOneField(fields[f], f, ""));
		}

		if (writeDetailsToConsole)
		{
			System.out.println(sb.toString());
		}
	}


	public String getInfoForOneField(String rawField, int fieldNo, String fieldName)
	{
		String trimmedField = rawField;

		if (rawField.length() > 1 && 
				rawField.substring(0, 1).equals("\"") &&
				rawField.substring(rawField.length() - 1).equals("\""))
		{
			trimmedField = rawField.substring(1, rawField.length() - 1);
		}

		return fieldName +  "(" + fieldNo + ") = " + trimmedField + "  ";
	}


}