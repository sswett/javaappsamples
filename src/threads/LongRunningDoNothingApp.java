package threads;

public class LongRunningDoNothingApp
{
    /*
        By default, all compiled output goes to the following directory, unless an output path is specified:

        C:\Programming\IdeaProjects\nonWebTests\classes

      */

    public static void main(String[] args)
    {
        // This will slowly write to the console for 10 hrs -- 1 entry every 5 seconds
        for (int x = 0; x < 7200; x++)
        {
            System.out.println("Output " + x);

            try
            {
                Thread.sleep(5000);   // wait 5 seconds
            }
            catch (InterruptedException e)
            {
                // do nothing
            }
        }

        System.out.println("The LongRunningDoNothing application ended.");
    }

}
