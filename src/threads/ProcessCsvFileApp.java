package threads;

/*
 * This app splits a single CSV file into multiple files.  Then separate threads are used to READ each of the multiple CSV files.
 */


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.Scanner;
import java.util.Date;
import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Writer;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.OutputStreamWriter;

public class ProcessCsvFileApp {

	public static final String PATH_TO_CSV_FILES = "./resources/";
	public static final String MAIN_FILE_NAME = PATH_TO_CSV_FILES + "csv.txt";


	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);

		System.out.print("How many threads do you want to use? ");
		final int threadsToUse = input.nextInt();

		System.out.print("How many minutes max to wait for process to finish? ");
		final int minutesToWait = input.nextInt();

		System.out.print("Do you want to write line-by-line details to console? Y or N ");
		String consoleResponse = input.next();
		final boolean writeDetailsToConsole = consoleResponse.equals("Y") || consoleResponse.equals("y");

		Date overallStartDate = new Date();

		createSeparateFilesForThreads(threadsToUse);

		ExecutorService threadExecutor = Executors.newCachedThreadPool();

		Date threadProcessingStartDate = new Date();

		for (int t = 0; t < threadsToUse; t++) 
		{
			startThread(threadExecutor, t, writeDetailsToConsole);
		}

		threadExecutor.shutdown();

		// Wait for all threads to finish so we can report time taken to run
		try
		{
			boolean threadsFinishedOk = threadExecutor.awaitTermination(minutesToWait, TimeUnit.MINUTES);

			if (threadsFinishedOk)
			{
				Date overallEndDate = new Date();
				long overallDiffMs = overallEndDate.getTime() - overallStartDate.getTime();
				long threadProcessingDiffMs = overallEndDate.getTime() - threadProcessingStartDate.getTime();

				System.out.printf("\nRun times (ms): Overall = %s, Thread processing = %s\n", overallDiffMs, 
						threadProcessingDiffMs);
			}
			else
			{
				System.out.printf("Process timed out while waiting up to %s minutes for threads to finish.\n", 
						minutesToWait);
			}
		}
		catch (InterruptedException ex)
		{
			System.out.println("Interrupted while waiting for threads to finish.");
		}
		finally
		{
			input.close();
		}
	}


	public static void createSeparateFilesForThreads(int threadsToUse)
	{
		// First create new empty files:
		createNewEmptyThreadFiles(threadsToUse);

		// Count # of records in main input file:
		int totalLineCnt = getLineCountForFile(MAIN_FILE_NAME);

		System.out.println("Total line count for file " + MAIN_FILE_NAME + ": " + totalLineCnt + "\n");

		// Open all thread files and store references in an array:
		Writer[] writers = new Writer[threadsToUse];

		for (int t = 0; t < threadsToUse; t++)
		{
			try
			{
				FileOutputStream outStream = new FileOutputStream(PATH_TO_CSV_FILES + "csv" + t + ".txt");
				writers[t] = new OutputStreamWriter(outStream);
			}

			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
		}

		// Prepare to read through the main file once, with intention to write portions to individual thread files
		int maxLinesPerThread = totalLineCnt /  threadsToUse;

		int fileThreadNo = 0;
		int lineCounter = 0;

		// Loop through records in main file:

		try
		{
			FileInputStream fileInputStream = new FileInputStream(MAIN_FILE_NAME);
			DataInputStream dataInputStream = new DataInputStream(fileInputStream);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));

			String oneLine;

			while ((oneLine = bufferedReader.readLine()) != null)
			{
				lineCounter++;
				writers[fileThreadNo].write(oneLine + "\r\n");

				if (lineCounter >= maxLinesPerThread)
				{
					lineCounter = 0;

					if (fileThreadNo < threadsToUse - 1)
					{
						fileThreadNo++;
					}
				}
			}

			bufferedReader.close();
			dataInputStream.close();

		}

		catch (Exception e)
		{
			System.err.println("Error: " + e.getMessage());
		}

		// Close thread files:
		for (Writer writer : writers)
		{
			try {
				writer.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}

	}


	public static void createNewEmptyThreadFiles(int threadsToUse)
	{
		for (int t = 0; t < threadsToUse; t++)
		{
			File threadFile = new File("/files/csv" + t + ".txt");
			threadFile.delete();

			try
			{
				threadFile.createNewFile();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}


	public static int getLineCountForFile(String fileNameIncludingFullPath)
	{
		int linesRead = 0;

		try
		{
			FileInputStream fileInputStream = new FileInputStream(fileNameIncludingFullPath);
			DataInputStream dataInputStream = new DataInputStream(fileInputStream);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));

			while (bufferedReader.readLine() != null)
			{
				linesRead++;
			}

			bufferedReader.close();
			dataInputStream.close();

		}

		catch (Exception e)
		{
			System.err.println("Error: " + e.getMessage());
		}

		return linesRead;
	}


	public static void startThread(ExecutorService threadExecutor, int threadNo, boolean writeDetailsToConsole)
	{
		ProcessCsvFileThread thread = new ProcessCsvFileThread(threadNo, writeDetailsToConsole);
		threadExecutor.execute(thread);
		System.out.println("THREAD# " + threadNo + " STARTED.");
	}


}