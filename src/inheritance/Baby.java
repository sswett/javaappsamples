package inheritance;

public class Baby extends Person {
	
	public int getStrideLength()
	{
		return 1;
	}
	
	public String getFootprint()
	{
		return "B";
	}

}
