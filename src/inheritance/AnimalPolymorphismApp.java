package inheritance;

import java.util.ArrayList;

public class AnimalPolymorphismApp {

	public static void main(String[] args) {
		
		ArrayList<Object> objects = new ArrayList<Object>();
		objects.add(new Cat());
		objects.add(new Dog());
		objects.add(new Animal());
		objects.add(new String());
		
		for (Object object : objects)
		{
			if (object instanceof Animal)
			{
				Animal animal = (Animal) object;
				System.out.println(String.format("Type: %s, Sound: %s", object.getClass(), animal.getSound()));
			}
			else
			{
				System.out.println(String.format("Type: %s, Sound: %s", object.getClass(), "N/A"));
			}
		}
		
		
		
	}

}
