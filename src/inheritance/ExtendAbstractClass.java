package inheritance;

import java.util.Date;

public class ExtendAbstractClass extends AbstractClass 
{
	private long creationTime;
	
	ExtendAbstractClass()
	{
		creationTime = new Date().getTime();
	}
	
	int getProduct(int a, int b)
	{
		return a * b;
	}

}
