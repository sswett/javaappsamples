package inheritance;

public class Man extends Person {
	
	public int getStrideLength()
	{
		return 3;
	}
	
	public String getFootprint()
	{
		return "M";
	}

}
