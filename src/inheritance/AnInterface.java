package inheritance;

public interface AnInterface {

	final String HELLO = "hello";
	final String GOODBYE = "good bye";
	
	void displayGreeting();
	
	abstract void displayGoodBye();
	
}
