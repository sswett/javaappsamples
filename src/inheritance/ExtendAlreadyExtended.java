package inheritance;

import java.util.Date;

public class ExtendAlreadyExtended extends ExtendAbstractClass 
{
	private final String MY_NAME = "Steve";
	
	private void showMyName()
	{
		System.out.println(MY_NAME);
	}
}
