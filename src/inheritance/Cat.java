package inheritance;

public class Cat extends Animal 
{

	@Override
	public String getSound()
	{
		return "meow";
	}
}
