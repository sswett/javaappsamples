package inheritance;

public abstract class AbstractClass implements AnInterface {
	
	protected int NUMBER_FIVE = 5;
	private int NUMBER_THREE = 3;

	@Override
	public void displayGreeting() {
		// TODO Auto-generated method stub
		System.out.println("hi");

	}

	@Override
	public void displayGoodBye() {
		// TODO Auto-generated method stub
		System.out.println("bye");
	}
	
	void displayGibberish() {
		System.out.println("some gibberish");
	}
	
	private int addTwoNumbers(int a, int b)
	{
		return a + b;
	}
	
	protected boolean isDivisibleByFive(int a)
	{
		return a % NUMBER_FIVE == 0;
	}
	
	boolean isDivisibleByThree(int a)
	{
		return a % NUMBER_THREE == 0;
	}
	
	abstract int getProduct(int a, int b);
	
	

}
