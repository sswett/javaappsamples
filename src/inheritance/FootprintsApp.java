package inheritance;

import java.util.Scanner;

public class FootprintsApp {

	public static void main(String[] args) {

		Baby baby = new Baby();
		Girl girl = new Girl();
		Man man = new Man();
		
		Person[] personArray = { baby, girl, man };
		
		Scanner input = new Scanner( System.in );
		
		while (true)
		{
			System.out.println();
			System.out.println("Type 'c' to continue or 'q' to quit. ");
			
			// When Enter is pressed, the following line reads what was typed
			String response = input.nextLine();
			
			if (!response.equals("c"))
			{
				break;
			}
			
			// Grab a random person and have him take a step
			int index = getRandomIndex(); 
			Person person = personArray[index];
			person.takeStep();
			
			printEverybodysFootprints(personArray);
		}
		
		System.out.println();
		System.out.print("Program has ended.");
		
	}
	
	private static int getRandomIndex()
	{
		// Math.random() returns a number between 0.000 and 0.999
		double randomNumber = Math.random();
		
		if (randomNumber > 0.66)
		{
			return 2;
		}
		else if (randomNumber > 0.33)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	private static void printEverybodysFootprints(Person[] personArray)
	{
		System.out.println();
		System.out.println("Footprints: race to here-->X");
		
		for (Person onePerson : personArray)
		{
			onePerson.showFootprints();
			System.out.println();
		}
	}

}
