package inheritance;

public class Girl extends Person {
	
	public int getStrideLength()
	{
		return 2;
	}
	
	public String getFootprint()
	{
		return "G";
	}

}
