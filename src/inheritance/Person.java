package inheritance;

public class Person {

	// Since footprints is really dynamic in size, an ArrayList object would be a
	// better choice.  However, that topic hasn't been covered yet, so we'll use
	// an array for now and save ArrayList for another time.
	
	private String[] footprints = new String[0];
	
	public void takeStep()
	{
		/* Since an array can't be resized, rebuild a new footprints
		 * array with a larger size, and mark the last element in
		 * the array with a footprint character -- see getFootprint().
		 */
		
		int previousSize = footprints.length;
		int newSize = previousSize + getStrideLength();
		
		String[] newFootprints = new String[newSize];   // new empty array
		
		// Copy values from old array to new array
		System.arraycopy(footprints, 0, newFootprints, 0, footprints.length);
		
		newFootprints[newSize - 1] = getFootprint();
		
		// Replace footprints array with new array
		footprints = newFootprints;
	}
	
	// This method could be defined as abstract but I wanted to test this
	// class independently of subclasses.
	public int getStrideLength()
	{
		return 1;
	}
	
	// This method could be defined as abstract but I wanted to test this
	// class independently of subclasses.
	public String getFootprint()
	{
		return "F";
	}
	
	public void showFootprints()
	{
		for (String footprint : footprints)
		{
			if (footprint == null)
			{
				System.out.print(" ");
			}
			else
			{
				System.out.print(footprint);
			}
		}
	}
}
