package utilities;

public class WeirdMathUtils {

	
	public static double getPower(int number, int power)
	{
		return Math.pow(number, power);
	}

	
	public static double getPower(int number, int power, boolean useNegativePower)
	{
		return useNegativePower ? Math.pow(number, power * -1) : Math.pow(number, power);
	}
	
}
