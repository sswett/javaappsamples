package utilities;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtils {
	
	
    public static String getStackTraceFullFromThrowable(Throwable throwable)
    {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter  = new PrintWriter(stringWriter);

        throwable.printStackTrace(printWriter);

        printWriter.flush();

        final String result = stringWriter.getBuffer().toString();
        printWriter.close();

        return result;
    }
	
	
    public static String getCauseMessage(Throwable throwable)
    {
        if (throwable == null)
        {
            return "Can't determine cause for throwable because throwable is null.";
        }
        else
        {
            Throwable causeThrowable = throwable.getCause();
            Throwable throwableToUse = causeThrowable == null ? throwable : causeThrowable;

            StringBuilder sb = new StringBuilder();
            String msg = throwableToUse.getMessage();

            if (msg == null)
            {
                sb.append("Can't determine message for throwable because message is null.");
            }
            else
            {
                sb.append(msg);
            }

            sb.append("\n").append(getStackTraceFullFromThrowable(throwableToUse));
            return sb.toString();
        }
    }
	

}
