package elevator;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ElevatorApp
{

    public static void main(String[] args)
    {
    	// args example: ./resources/elevator_commands.txt A

    	
    	if (args.length != 2)
        {
            processStartFailure();
        }

        String mode = args[1];

        if (!mode.equalsIgnoreCase("A") && !mode.equalsIgnoreCase("B"))
        {
            processStartFailure();
        }

        String inputFileName = args[0];
        OperatingMode operatingMode = mode.equalsIgnoreCase("A") ? OperatingMode.MODE_A : OperatingMode.MODE_B;

        try
        {
            FileInputStream fileInputStream = new FileInputStream(inputFileName);
            DataInputStream dataInputStream = new DataInputStream(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));

            String oneLine;

            while ((oneLine = bufferedReader.readLine()) != null)
            {
                CommandSet commandSet = new CommandSet(oneLine);
                CommandSetProcessor.processCommandSet(commandSet, operatingMode);
            }

            bufferedReader.close();
            dataInputStream.close();
            fileInputStream.close();
        }
        catch (IOException e)
        {
            System.err.println("An error occurred trying to read file \"" + inputFileName + "\".  " +
                    "A stack trace has been printed for diagnostic purposes.\n");

            e.printStackTrace();
        }

    }


    public static void processStartFailure()
    {
        System.err.println("Application did not start properly.  Two arguments are expected: input-file-name mode " +
                "(where mode is \"A\" or \"B\").  Example: \\some_directory\\elevator.txt A");

        System.exit(0);
    }

}
