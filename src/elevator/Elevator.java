package elevator;

public class Elevator
{
    private int currentFloor;
    private TravelStatus travelStatus;
    private int floorsTraveled;


    public Elevator(int currentFloor)
    {
        this.currentFloor = currentFloor;
        travelStatus = TravelStatus.PARKED;
        floorsTraveled = 0;
    }


    public void goToFloor(int targetFloor)
    {
        if (targetFloor == currentFloor)
        {
            return;
        }

        travelStatus = targetFloor > currentFloor ? TravelStatus.GOING_UP : TravelStatus.GOING_DOWN;

        while (currentFloor != targetFloor)
        {
            floorsTraveled++;
            currentFloor += travelStatus == TravelStatus.GOING_UP ? 1 : -1;
        }

        travelStatus = TravelStatus.PARKED;

        System.out.print(targetFloor + " ");

    }


    public int getFloorsTraveled()
    {
        return floorsTraveled;
    }

}
