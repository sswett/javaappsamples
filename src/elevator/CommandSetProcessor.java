package elevator;

public class CommandSetProcessor
{

    public static void processCommandSet(CommandSet commandSet, OperatingMode operatingMode)
    {
        Elevator elevator = new Elevator(commandSet.getStartingFloor());

        System.out.print(commandSet.getStartingFloor() + " ");

        Integer[] targetStops = operatingMode == OperatingMode.MODE_A ?
                commandSet.getTargetStopsInModeA() : commandSet.getTargetStopsInModeB()  ;

        for (int floorToStop : targetStops)
        {
            elevator.goToFloor(floorToStop);
        }

        System.out.println("(" + elevator.getFloorsTraveled() + ")");
    }
}
