package elevator;

public enum TravelStatus
{
    PARKED,
    GOING_UP,
    GOING_DOWN
}
