package elevator;

import java.util.ArrayList;
import java.util.Collections;

public class CommandSet
{
    private int startingFloor;
    private ArrayList<Integer[]> boardingInstructions;

    /*

    If input data is "9:1-5,1-6,1-5" then:

    startingFloor = 9
    boardingInstructions =
        { 1, 5 }
        { 1, 6 }    <-- One of these Integer[] objects is what I call an "instruction pair"
        { 1, 5 }

     */


    public CommandSet(String commandSetInputData)
    {
        String[] majorFields = commandSetInputData.split(":");
        startingFloor = Integer.valueOf(majorFields[0]);
        String[] instructionPairs = majorFields[1].split(",");
        boardingInstructions = new ArrayList<Integer[]>();

        for (String instructionPair : instructionPairs)
        {
            String[] floorNumbersStr = instructionPair.split("-");
            Integer[] floorNumbers = { Integer.valueOf(floorNumbersStr[0]), Integer.valueOf(floorNumbersStr[1]) };
            boardingInstructions.add(floorNumbers);
        }
    }


    public int getStartingFloor()
    {
        return startingFloor;
    }


    public Integer[] getTargetStopsInModeA()
    {
        ArrayList<Integer> stopsList = new ArrayList<Integer>();

        for (Integer[] instructionPair : boardingInstructions)
        {
            addStopsToList(stopsList, instructionPair);
        }

        Integer[] stopsArray = stopsList.toArray(new Integer[ stopsList.size() ]);
        return stopsArray;
    }


    public Integer[] getTargetStopsInModeB()
    {
        ArrayList<Integer> overallStopsList = new ArrayList<Integer>();
        ArrayList<Integer> sameDirectionStopsList = new ArrayList<Integer>();

        TravelStatus overallTravelStatus = TravelStatus.PARKED;

        for (int x = 0; x < boardingInstructions.size(); x++)
        {
            Integer[] instructionPair = boardingInstructions.get(x);
            TravelStatus instructionPairTravelStatus = getTravelStatusForInstructionPair(instructionPair);

            if (x > 0)
            {
                // Has travel status changed (i.e. direction)?
                if (instructionPairTravelStatus != overallTravelStatus)
                {
                    consumeSameDirectionStopsList(sameDirectionStopsList, overallStopsList, overallTravelStatus);
                }
            }

            addStopsToList(sameDirectionStopsList, instructionPair);
            overallTravelStatus = instructionPairTravelStatus;
        }

        if (sameDirectionStopsList.size() > 0)
        {
            consumeSameDirectionStopsList(sameDirectionStopsList, overallStopsList, overallTravelStatus);
        }

        Integer[] overallStopsArray = overallStopsList.toArray(new Integer[ overallStopsList.size() ]);
        return overallStopsArray;
    }


    private static void consumeSameDirectionStopsList(ArrayList<Integer> sameDirectionStopsList,
                                               ArrayList<Integer> overallStopsList,
                                               TravelStatus overallTravelStatus)
    {
        Collections.sort(sameDirectionStopsList);

        if (overallTravelStatus == TravelStatus.GOING_DOWN)
        {
            Collections.reverse(sameDirectionStopsList);
        }

        Integer[] sameDirectionStopsArray =
                sameDirectionStopsList.toArray(new Integer[ sameDirectionStopsList.size() ]);

        addStopsToList(overallStopsList, sameDirectionStopsArray);
        sameDirectionStopsList.clear();
    }


    private static void addStopsToList(ArrayList<Integer> targetStopsList, Integer[] stops)
    {
        for (int i = 0; i < stops.length; i++)
        {
            int stopToAdd = stops[i];

            // Don't add stop if same as previous stop
            if ( targetStopsList.size() == 0 || stopToAdd != targetStopsList.get(targetStopsList.size() - 1))
            {
                targetStopsList.add(stopToAdd);
            }
        }
    }


    private static TravelStatus getTravelStatusForInstructionPair(Integer[] instructionPair)
    {
        return instructionPair[0] < instructionPair[1] ?
                TravelStatus.GOING_UP : TravelStatus.GOING_DOWN;
    }


}
