package collections;

import java.util.HashMap;
import java.util.Map.Entry;

public class MapIterationApp {

	public static void main(String[] args) {

		HashMap<String,String> map = new HashMap<String,String>();
		
		map.put("1", "one");
		map.put("2", "two");
		map.put("3", "three");
		
		for (Entry<String,String> entry : map.entrySet())
		{
			System.out.println(
					String.format("Key=%s Value=%s", entry.getKey(), entry.getValue())
					);
		}
	}

}
