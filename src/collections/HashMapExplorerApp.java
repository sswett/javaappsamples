package collections;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class HashMapExplorerApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		HashMap myHashMap = getPopulatedHashMap();
		printHashMap(myHashMap);
	}

	private static HashMap getPopulatedHashMap()
	{
		HashMap result = new HashMap();
		
		String name = "Steve";
		
		Calendar dobAsCalendar = Calendar.getInstance();
		dobAsCalendar.set(1963, 8 - 1, 29);   // The month parm is 0-based
		
		Date dobAsDate = dobAsCalendar.getTime();
		
		Calendar nowAsCalendar = Calendar.getInstance();
		
		long msAlive = nowAsCalendar.getTimeInMillis() - dobAsCalendar.getTimeInMillis();
		
		// Below, "(double)" is an example of casting an expression to a data type.
		// Without this, it would have calculated as an integer since all the
		// variables within the expression are integers
		double approxYrsAlive = (double) msAlive / 1000 / 60 / 60 / 24 / 365;
		
		// Notice that the data type is Object for the two arguments to the "put" method
		result.put("Name", name);
		result.put("DOB", dobAsDate);
		result.put("Age", approxYrsAlive);
		
		return result;
	}
	
	private static void printHashMap(HashMap map)
	{
		Iterator iter = map.keySet().iterator();
		
		while (iter.hasNext())
		{
			// Below, "(String)" is needed to cast the HashMap's key, an Object, to
			// a String type
			String key = (String) iter.next();
			
			// In this case, I didn't cast the HashMap's value to a String, Date, or
			// Double type -- because I'm not sure what I have just yet.
			Object value = map.get(key);
			
			String dataType = "unknown";
			
			// "instanceof" can be used to check what kind of object you have
			if (value instanceof String)
			{
				dataType = "String";
				String aString = (String) value;
				
				System.out.println(key + " is a " + dataType + " object and has" +
						" a value of " + aString );
			}
			else if (value instanceof Date)
			{
				dataType = "Date";
				Date aDate = (Date) value;
				DateFormat format = DateFormat.getDateInstance();
				
				System.out.println(key + " is a " + dataType + " object and has" +
						" a value of " + format.format(aDate) );
			}
			else if (value instanceof Double)
			{
				dataType = "Double";
				Double aDouble = (Double) value;
				
				System.out.println(key + " is a " + dataType + " object and has" +
						" a value of " + aDouble.toString() );
			}
			else
			{
				System.out.println(key + " is an " + dataType + " object and has" +
						" a value of " + value.toString() );
			}
			
		}
	}
	
	
}
