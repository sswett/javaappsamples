package collections;
import java.util.*;

public class TestLinkedHashSetApp {
	
  public static void main(String[] args) {
    String[] list = { "John", "Penelope", "April", "Zachary", "George" };

    System.out.println("entry order: " + Arrays.toString(list) + "\n");

    Set<String> people;

    people = new HashSet<String>();          // put into a HashSet
    for (String elt: list) people.add(elt);

    System.out.println("HashSet order: " + people);

    // print the set by iteration
    for (String person: people)
      System.out.println( "\t" + person);
    System.out.println();

    people = new LinkedHashSet<String>();    // put into a LinkedHashSet
    for (String elt: list) people.add(elt);

    System.out.println("LinkedHashSet original order: " + people + "\n");


          Iterator<String> it = people.iterator();
          String firstItem = it.next();

      System.out.println("LinkedHashSet first item: " + firstItem + "\n");


    people = new TreeSet<String>();       // put into a TreeSet
    for (String elt: list) people.add(elt);

    System.out.println("TreeSet order: " + people);
  }
}
