package collections;

import java.util.*;

public class TestArraySortingApp {

public static void main(String[] args)
{

    HashMap itemsMap = new HashMap();
    itemsMap.put("3", "run");
    itemsMap.put("5", "walk");
    itemsMap.put("4", "jog");
    itemsMap.put("1", "swim");
    itemsMap.put("2", "dive");

    // Put HashMap keys into an array:
    String[] commentNumberStrings = new String[itemsMap.size()];
    int[] commentNumbers = new int[itemsMap.size()];
    Set myKeys = itemsMap.keySet();

    // Java API syntax for Set.toArray =
    //    <T> T[] toArray(T[] a)

    commentNumberStrings = (String[]) myKeys.toArray(new String[itemsMap.size()]);

    for (int x = 0; x < commentNumberStrings.length; x++)
    {
        commentNumbers[x] = Integer.parseInt(commentNumberStrings[x]);
    }

    // Sort the array:
    Arrays.sort(commentNumbers);

    // List sorted keys and values:
    for (int s = 0; s < commentNumbers.length; s++)
    {
        int commentNumber = commentNumbers[s];
        String commentNumberString = "" + commentNumber;
        String myValue = (String) itemsMap.get(commentNumberString);
        System.out.println("The value of comment key " + commentNumberString + " is " + myValue);
    }

}


}
