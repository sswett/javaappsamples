package collections;
import java.util.*;

public class TestTreeMapApp {

	
	public static void main(String[] args) {

		HashMap effProps = new HashMap();

		effProps.put("planter.large.red", "0.99");
		effProps.put("planter.medium.red", "0.89");
		effProps.put("planter.small.red", "0.69");

		effProps.put("plant.tree.oak", "winter leaves");
		effProps.put("plant.tree.maple", "fall colorful");
		effProps.put("plant.flower.rose", "thorny");
		effProps.put("plant.flower.lily", "graceful");

		effProps.put("animal.mammal.bear", "big");
		effProps.put("animal.mammal.leopard", "fast");
		effProps.put("animal.reptile.frog", "water");
		effProps.put("animal.reptile.snake", "coils");
		effProps.put("animal.bird.hawk", "eyes");
		effProps.put("animal.bird.eagle", "bald");

		TreeMap sortedMap = new TreeMap(effProps);

		// tryOne(sortedMap);

		// tryAnother(sortedMap);

		tryThird(sortedMap);

	}

	public static void tryOne(TreeMap sortedMap)
	{
		SortedMap subsetMap = sortedMap.subMap("animal.bird.eagle", "plant.flower.rose");

		Iterator iter = subsetMap.keySet().iterator();

		while (iter.hasNext())
		{
			String key = (String) iter.next();
			String value = (String) subsetMap.get(key);

			System.out.println("key: " + key + "  value: " + value);

		}   // mdss
	}


	public static void tryAnother(TreeMap sortedMap)
	{

		String prefix = "animal.";
		SortedMap subsetMap = sortedMap.tailMap(prefix);
		Iterator iter = subsetMap.keySet().iterator();

		while (iter.hasNext())
		{
			String key = (String) iter.next();

			if (!key.startsWith(prefix))
			{
				break;
			}

			String value = (String) subsetMap.get(key);

			System.out.println("key: " + key + "  value: " + value);

		}   // mdss


	}


	public static void tryThird(TreeMap sortedMap)
	{
		String prefix = "plant.";
		SortedMap subsetMap = sortedMap.subMap(prefix, prefix + "~");
		Iterator  iter      = subsetMap.keySet().iterator();

		while (iter.hasNext())
		{
			final String key = (String) iter.next();

			if (!key.startsWith(prefix))
			{
				break;
			}

			String value = (String) subsetMap.get(key);

			System.out.println("key: " + key + "  value: " + value);
		}

	}



}