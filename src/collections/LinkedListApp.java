package collections;

import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedListApp {

	public static void main(String[] args) {

		LinkedList<String> list = new LinkedList<String>();
		
		list.add("one");
		list.add("three");
		list.add(1, "two");
		
		System.out.printf("Item at 1 = %s\n\n", list.get(1));
		
		list.addAll(list);
		
		ListIterator<String> iter = list.listIterator();
		
		while (iter.hasNext())
		{
			int i = iter.nextIndex();
			String item = iter.next();
			System.out.printf("Item at %s = %s\n", i, item);
		}
		
		
		list.push("four");
		
		System.out.printf("\nItem popped after a push of \"four\": %s", list.pop());
	}

}
