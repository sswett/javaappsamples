package collections;

import java.util.Arrays;
import java.util.List;

public class TestArrayCollectionBridgingApp
{

public static void main(String[] args)
{
    // Converting back and forth between a collection and a string array

    List<String> stoogesList = Arrays.asList("Larry", "Moe", "Curly");
    String[] stoogesArray = stoogesList.toArray(new String[ stoogesList.size() ]);

    String[] stoogesAppendedArray = stoogesList.toArray(new String[ 5 ]);
    stoogesAppendedArray[3] = "Steve";
    stoogesAppendedArray[4] = "Andy";

    // When using toArray, you should pass a new Array (vs. an existing one).  This tells toArray what type of array
    // you want it to return as well as the target size to return.  It seems like you should always use a target size
    // that matches the object for which toArray is being run.  You're going to get all the elements in that object
    // returned regardless.  I suppose it could be a larger number, in case you planned to then stick a couple
    // more elements in the returned array.

    System.out.println("stoogesList output:");

    for (String listItem : stoogesList)
    {
        System.out.println(listItem);
    }

    System.out.println("\nstoogesArray output:");

    for (String arrayItem : stoogesArray)
    {
        System.out.println(arrayItem);
    }

    System.out.println("\nstoogesAppendedArray output:");

    for (String arrayItem : stoogesAppendedArray)
    {
        System.out.println(arrayItem);
    }



}


}