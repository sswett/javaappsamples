package my_math_api;

public interface MyMath {

	
	static final int x = 5;   // An interface can have static data but not methods
	
	/**
	 * 
	 * @param intValues - one or more int values to add together
	 * @return - sum of all int values
	 */
	int sum(int... intValues);

	
	/**
	 * 
	 * @param floatValues - one or more float values to add together
	 * @return - sum of all float values
	 */
	float sum(float... floatValues);

	
	/**
	 * 
	 * @param doubleValues - one or more double values to add together
	 * @return - sum of all double values
	 */
	double sum(double... doubleValues);
}
