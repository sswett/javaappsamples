package my_math_api;

public class MyMathImpl implements MyMath {
	
	private String author = "Steve Swett";
	
	
	public String getAuthor()
	{
		return author;
	}

	
	@Override
	public int sum(int... intValues) {
		int total = 0;
		
		for (int x : intValues)
		{
			total += x;
		}
		
		return total;
	}

	
	@Override
	public float sum(float... floatValues) {
		float total = 0;
		
		for (float x : floatValues)
		{
			total += x;
		}
		
		return total;
	}

	
	@Override
	public double sum(double... doubleValues) {
		double total = 0;
		
		for (double x : doubleValues)
		{
			total += x;
		}
		
		return total;
	}
	
	
	public int sumFive(int int1, int int2, int int3, int int4, int int5)
	{
		return sum(int1, int2, int3, int4, int5);
	}
	

}
