package my_math_api;

public class MyMathTestApp {

	public static void main(String[] args) {

		MyMathImpl math = new MyMathImpl();
		
		System.out.printf("MyMathImpl.sum - The sum of 1-5 = %d\n", math.sum(1, 2, 3, 4, 5));
		
		System.out.printf("MyMathImpl.sumFive - The sum of 1-5 = %d\n", math.sumFive(1, 2, 3, 4, 5));
		
		System.out.printf("MyMathImpl.sum - The sum of 1.25F, 1.4F and 5.05F = %f\n", math.sum(1.25F, 1.4F, 5.05F));
		
		System.out.printf("MyMathImpl.sum - The sum of 1.25, 1.4 and 5.05 = %f\n", math.sum(1.25, 1.4, 5.05));
		
		System.out.printf("Addition with + sign - The sum of 1.25, 1.4 and 5.05 = %f\n", 1.25 + 1.4 + 5.05);
		
		System.out.printf("Author = %s", math.getAuthor() );
		
		// %d works on integers, %f works on floats and doubles
		
		
	}

}
