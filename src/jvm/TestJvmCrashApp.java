package jvm;
public class TestJvmCrashApp
{

    public static void main(String[] args) throws Exception
    {
        java.lang.reflect.Field field = sun.misc.Unsafe.class.getDeclaredField("theUnsafe");
        field.setAccessible(true);
        sun.misc.Unsafe $ = (sun.misc.Unsafe) field.get(null);
        $.putAddress(0, 0);
    }


    /*
    This is used in conjunction with the following VM options, specified individually, separately:

    -XX:+ShowMessageBoxOnError -XX:OnError="jstack.exe %p > \junk\jstack.txt"

    The hs_err*.log file will be generated in c:\programming\ideaprojects\nonwebtests .  In this case,
    this crash log does pinpoint the problem pretty well.

     */

}
