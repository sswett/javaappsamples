package jvm;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;

public class GetCpuInfoApp {

public static void main(String[] args)
{

    // Not good enough:
    // System.getProperties().list(System.out);

    // Here is an API ($$) that might let you get at CPU speed:
    // http://www.hyperic.com/products/sigar

    // Another way to get at CPU speed:
    // 1. Write a bit of C or C++ code that will do it
    // 2. Wrap that code in a bit of JNI (Java Native Interface) so Java app can execute it
    //    Simple example of writing C code and using JNI: http://www.java-tips.org/other-api-tips/jni/simple-example-of-using-the-java-native-interface.html 

    StringBuilder sb = new StringBuilder();

    OperatingSystemMXBean osBean = ManagementFactory.getOperatingSystemMXBean();

    sb.append("Operating system info:");
    sb.append("\n  Name:            " + osBean.getName());
    sb.append("\n  Architecture:    " + osBean.getArch());
    sb.append("\n  Processors:      " + osBean.getAvailableProcessors());
    sb.append("\n  Version:         " + osBean.getVersion());
    sb.append("\n  System Load Avg: " + osBean.getSystemLoadAverage());


    ThreadMXBean threadBean = ManagementFactory.getThreadMXBean();

    long[] threadIds = threadBean.getAllThreadIds();

    long threadCpuTime = 0L;

    for (long threadId : threadIds)
    {
        threadCpuTime += threadBean.getThreadCpuTime(threadId);
    }

    sb.append("\n\nThread info:");
    sb.append("\n  Total CPU time (nanoseconds): " + threadCpuTime);   // nanosecond = 1 billionth of a second
    sb.append("\n  Total CPU time (seconds):     " + (threadCpuTime / 1000000000D) );
    sb.append("\n  Thread count:      " + threadBean.getThreadCount());
    sb.append("\n  Peak thread count: " + threadBean.getPeakThreadCount());


    MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();

    MemoryUsage heapMemoryUsage = memoryBean.getHeapMemoryUsage();

    sb.append("\n\nHeap memory info:");
    sb.append("\n  Max (bytes):  " + heapMemoryUsage.getMax());
    sb.append("\n  Used (bytes): " + heapMemoryUsage.getUsed());
    sb.append("\n  Max (MB):     " + (heapMemoryUsage.getMax() / 1048576D) );
    sb.append("\n  Used (MB):    " + (heapMemoryUsage.getUsed() / 1048576D) );


    System.out.print(sb.toString());

}


}