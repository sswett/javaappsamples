package doll_delivery;

import java.util.ArrayList;
import java.util.HashMap;

public class RunDeliveryApp {
	

	public static void main(String[] args) {

		// Setup test data:
		
		String startingLocation = "Kruthika's abode";
		String targetLocation = "Craig's haunt";
		
		ArrayList<HashMap<String,String>> edgeList = new ArrayList<HashMap<String,String>>();
		
		edgeList.add(RouteCalculator.createEdgeMap("Kruthika's abode", "Mark's crib", 9));
		edgeList.add(RouteCalculator.createEdgeMap("Kruthika's abode", "Greg's casa", 4));
		edgeList.add(RouteCalculator.createEdgeMap("Kruthika's abode", "Matt's pad", 18));
		edgeList.add(RouteCalculator.createEdgeMap("Kruthika's abode", "Brian's apartment", 8));
		edgeList.add(RouteCalculator.createEdgeMap("Brian's apartment", "Wesley's condo", 7));
		edgeList.add(RouteCalculator.createEdgeMap("Brian's apartment", "Cam's dwelling", 17));
		edgeList.add(RouteCalculator.createEdgeMap("Greg's casa", "Cam's dwelling", 13));
		edgeList.add(RouteCalculator.createEdgeMap("Greg's casa", "Mike's digs", 19));
		edgeList.add(RouteCalculator.createEdgeMap("Greg's casa", "Matt's pad", 14));
		edgeList.add(RouteCalculator.createEdgeMap("Wesley's condo", "Kirk's farm", 10));
		edgeList.add(RouteCalculator.createEdgeMap("Wesley's condo", "Nathan's flat", 11));
		edgeList.add(RouteCalculator.createEdgeMap("Wesley's condo", "Bryce's den", 6));
		edgeList.add(RouteCalculator.createEdgeMap("Matt's pad", "Mark's crib", 19));
		edgeList.add(RouteCalculator.createEdgeMap("Matt's pad", "Nathan's flat", 15));
		edgeList.add(RouteCalculator.createEdgeMap("Matt's pad", "Craig's haunt", 14));
		edgeList.add(RouteCalculator.createEdgeMap("Mark's crib", "Kirk's farm", 9));
		edgeList.add(RouteCalculator.createEdgeMap("Mark's crib", "Nathan's flat", 12));
		edgeList.add(RouteCalculator.createEdgeMap("Bryce's den", "Craig's haunt", 10));
		edgeList.add(RouteCalculator.createEdgeMap("Bryce's den", "Mike's digs", 9));
		edgeList.add(RouteCalculator.createEdgeMap("Mike's digs", "Cam's dwelling", 20));
		edgeList.add(RouteCalculator.createEdgeMap("Mike's digs", "Nathan's flat",12));
		edgeList.add(RouteCalculator.createEdgeMap("Cam's dwelling", "Craig's haunt", 18));
		edgeList.add(RouteCalculator.createEdgeMap("Nathan's flat", "Kirk's farm", 3));
		
		// Run various scenarios:
		
		RouteCalculator rc = new RouteCalculator();
		
		HashMap<String,String> outputMap = rc.getDistanceAndPathToDestination(startingLocation, targetLocation, edgeList);
		System.out.println(RouteCalculator.getOutputMapAsString(outputMap));
		
		outputMap = rc.getDistanceAndPathToDestination(targetLocation, startingLocation, edgeList);
		System.out.println(RouteCalculator.getOutputMapAsString(outputMap));
		
		outputMap = rc.getDistanceAndPathToDestination("Greg's casa", "Wesley's condo", edgeList);
		System.out.println(RouteCalculator.getOutputMapAsString(outputMap));
		
		outputMap = rc.getDistanceAndPathToDestination("Mark's crib", "Bryce's den", edgeList);
		System.out.println(RouteCalculator.getOutputMapAsString(outputMap));
	}

}
