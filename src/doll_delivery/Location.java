package doll_delivery;

import java.util.ArrayList;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Location {

	private String name;
	private HashMap<Location,Edge> neighborsWithEdgeReferences; 
	private int tentativeDistance;
	private boolean visited;
	private Location parent;
	
	
    private static final Logger _traceLogger = LoggerFactory.getLogger("location.unvisited.neighbors.trace");
    private static final Logger _errorLogger = LoggerFactory.getLogger("neighbor.distance.error");

	
	
	Location(String name)
	{
		this.name = name;
		neighborsWithEdgeReferences = new HashMap<Location,Edge>();
		tentativeDistance = Integer.MAX_VALUE;   // Simulates infinity
		visited = false;
		parent = null;
	}

	
	private Location()
	{
		
	}
	
	
	public String getName()
	{
		return name;
	}
	
	
	public int getTentativeDistance()
	{
		return tentativeDistance;
	}
	
	
	public boolean isVisited()
	{
		return visited;
	}
	
	
	protected void addNeighbor(Location neighbor, Edge edge)
	{
		neighborsWithEdgeReferences.put(neighbor, edge);
	}
	
	
	public ArrayList<Location> getUnvisitedNeighbors()
	{
		ArrayList<Location> result = new ArrayList<Location>();
		
		for (Location neighbor : neighborsWithEdgeReferences.keySet())
		{
			if (!neighbor.isVisited())
			{
				result.add(neighbor);
			}
		}

		
		_traceLogger.trace("\n***Unvisited neighbors for location: {}",  getName());
		
		for (Location location : result)
		{
			_traceLogger.trace(location.getName());
		}

		return result;
	}
	
	
	public int getDistanceToNeighbor(Location neighbor)
	{
		Edge edge = neighborsWithEdgeReferences.get(neighbor);
		
		if (edge == null)
		{
			// TODO - this shouldn't happen; perhaps throw exception
			_errorLogger.error("Location.getDistancetoNeighbor -- got unexpected 0 result.");
			return 0;   // Apparently this is being reached.  It's important to return 0 here.
		}
		else
		{
			return edge.getDistanceBetween();
		}
	}

	
	
	protected void setTentativeDistance(int tentativeDistance)
	{
		this.tentativeDistance = tentativeDistance;
	}
	
	
	protected void markAsVisited()
	{
		visited = true;
	}
	
	
	protected void setParent(Location parent)
	{
		this.parent = parent;
	}
	
	
	public Location getParent()
	{
		return parent;
	}
	
	
}
