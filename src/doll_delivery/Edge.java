package doll_delivery;

public class Edge {
	
	
	private Location location1;
	private Location location2;
	private int distanceBetween;
	
	
	Edge(Location location1, Location location2, int distanceBetween)
	{
		this.location1 = location1;
		this.location2 = location2;
		this.distanceBetween = distanceBetween;
	}
	
	
	private Edge()
	{
		
	}
	
	
	public int getDistanceBetween()
	{
		return distanceBetween;
	}
	
	
	/*
	public Location getLocation1()
	{
		return location1;
	}
	
	
	public Location getLocation2()
	{
		return location2;
	}
	
	
	public boolean containsLocation(Location location)
	{
		return location1.equals(location) || location2.equals(location);
	}
	*/

}
