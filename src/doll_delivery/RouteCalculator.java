package doll_delivery;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RouteCalculator {
	
    private static final Logger _infoLogger = LoggerFactory.getLogger("route.calculator.info");
    private static final Logger _traceLogger = LoggerFactory.getLogger("route.calculator.trace");

    private static final String START_LOCATION_KEY = "startLocation";
    private static final String END_LOCATION_KEY = "endLocation";
    private static final String DISTANCE_KEY = "distance";
    private static final String PATH_KEY = "path";

    private static final String[] LOCATION_KEYS = { START_LOCATION_KEY, END_LOCATION_KEY };
    private static final String PATH_ELEMENT_SEPARATOR = " => ";
    
    // This calculator can be instantiated so that its post-run results can be easily analyzed/formatted (from unit tests, for example).
    
    // Inputs:
    private String savedStartingLocation;
    private String savedTargetLocation;
    private ArrayList<HashMap<String,String>> savedEdges;
    
    // Output:
    private HashMap<String,String> savedOutputMap;
    
    // Other state:
    private boolean calculated = false;
    
    
    public RouteCalculator()
    {
    	// Intentionally does nothing
    }

	
	public HashMap<String,String> getDistanceAndPathToDestination(
			String startingLocation, 
			String targetLocation, 
			ArrayList<HashMap<String,String>> edges)
	{
		// Save inputs for instance access (perhaps from unit tests, if needed)
		savedStartingLocation = startingLocation;
		savedTargetLocation = targetLocation;
		savedEdges = edges;
		
		// Build list of Location objects:
		
		ArrayList<Location> locations = new ArrayList<Location>();
		
		// 		First get unique map of locations
		
		HashMap<String,Location> namedLocations = new HashMap<String,Location>();
		
		for (HashMap<String,String> map : edges)
		{
			for (String locationKey : LOCATION_KEYS)
			{
				String locationName = map.get(locationKey);
				
				if (!namedLocations.containsKey(locationName))
				{
					Location location = new Location(locationName); 
					namedLocations.put(locationName, location);
					
					// Also add to the master list of locations:
					locations.add(location);
				}
			}
		}
		
		// Build edge objects (and use to note a Location's neighbors).
		
		for (HashMap<String,String> map : edges)
		{
			String startLocationName = map.get(START_LOCATION_KEY); 
			String endLocationName = map.get(END_LOCATION_KEY); 
			String distance = map.get(DISTANCE_KEY);
			
			Location startLocation = namedLocations.get(startLocationName);
			Location endLocation = namedLocations.get(endLocationName);
			
			Edge edge = new Edge(startLocation, endLocation, Integer.valueOf(distance));
			startLocation.addNeighbor(endLocation, edge);
			endLocation.addNeighbor(startLocation, edge);
		}
		
		savedOutputMap = getDistanceAndPathToDestination(namedLocations.get(startingLocation), namedLocations.get(targetLocation), locations);
		
		return savedOutputMap;
	}
	
	
	private HashMap<String,String> getDistanceAndPathToDestination(Location startLocation, Location endLocation, ArrayList<Location> locations)
	{
		_infoLogger.info("Begin... Start: {}, End: {}", startLocation.getName(), endLocation.getName() );
		
		
		// (1.1 - set initial node distance to zero)
		startLocation.setTentativeDistance(0);
		
		// (1.2 - set all other node distances to infinity)
		// done during construction
		
		// (2 - set initial node as current)
		Location currentLocation = startLocation;
		
		// (2.1 -- mark all other nodes as unvisited)
		// done during construction
		
		// Assume that means the current location should be marked as visited.
		currentLocation.markAsVisited();
		
		// (2.2 -- create a set of all the unvisited nodes called the unvisited set)
		ArrayList<Location> unvisitedLocations = new ArrayList<Location>(locations);
		unvisitedLocations.remove(currentLocation);
		
		while (true)
		{
			// (3 -- for the current node ...)
			
			_traceLogger.trace("Current location: {}", currentLocation.getName() );
			
			// (3.1 -- consider each of the node's unvisited neighbors and calculate their tentative distances)
			
			for (Location unvisitedNeighbor : currentLocation.getUnvisitedNeighbors())
			{
				// (3.1.1 and 3.1.2)
				int proposedDistance = currentLocation.getTentativeDistance() + currentLocation.getDistanceToNeighbor(unvisitedNeighbor);

				_traceLogger.trace("unvisitedNeighbor: {}, proposedDistance: {}; unvisitedNeighbor tent dist: {}", 
						unvisitedNeighbor.getName(), proposedDistance, unvisitedNeighbor.getTentativeDistance());
				
				if (proposedDistance < unvisitedNeighbor.getTentativeDistance())
				{
					_traceLogger.trace("Set unvisitedNeigbhor: {} setTentativeDistance to: {}", unvisitedNeighbor.getName(), proposedDistance);
					unvisitedNeighbor.setTentativeDistance(proposedDistance);
					unvisitedNeighbor.setParent(currentLocation);
				}
				
			}
			
			// (4 -- mark current node as visited and remove from unvisited set)
			currentLocation.markAsVisited();
			unvisitedLocations.remove(currentLocation);
			
			// (5.1)
			if (currentLocation.equals(endLocation))
			{
				_infoLogger.info("Found path to end location.");
				
				break;   // Calculations complete
			}
			
			// Calculate bestNextLocation.  Consider all nodes with computed distances (not just neighbors)
			Location bestNextLocation = getUnvisitedLocationWithSmallestTentativeDistance(unvisitedLocations);
			
			// (5.2)
			if (bestNextLocation == null)
			{
				_infoLogger.info("bestNextLocation is null.  endLocation parent is: {}; all of the unvisited locations have a tentative distance of: {}",
						endLocation.getParent(),
						Integer.MAX_VALUE);
				
				break;   // Calculations complete
			}
			
			// (6)
			currentLocation = bestNextLocation;
		}
		
		calculated = true;

		return getOutputMapAfterCalculations(startLocation, endLocation);
	}
	
	
	private Location getUnvisitedLocationWithSmallestTentativeDistance(ArrayList<Location> unvisitedLocations)
	{
		int smallest = Integer.MAX_VALUE;
		Location bestLocation = null;
		
		for (Location location : unvisitedLocations)
		{
			if (location.getTentativeDistance() < smallest)
			{
				smallest = location.getTentativeDistance();
				bestLocation = location;
			}
		}
		
		return bestLocation;
	}
	
	
	private HashMap<String,String> getOutputMapAfterCalculations(Location startLocation, Location endLocation)
	{
		HashMap<String,String> outputMap = new HashMap<String,String>();
		
		if (!calculated || endLocation.getParent() == null)
		{
			return outputMap;
		}
		
		// Path reconstruction:
		StringBuilder path = new StringBuilder();
		Location i = endLocation;
		
		while (!i.equals(startLocation))
		{
			path.insert(0, PATH_ELEMENT_SEPARATOR + i.getName());
			i = i.getParent();
		}
		
		path.insert(0,  startLocation.getName());
		
		outputMap.put(DISTANCE_KEY, Integer.toString(endLocation.getTentativeDistance()) );
		outputMap.put(PATH_KEY, path.toString());
		
		return outputMap;
	}
	
	
	public static HashMap<String,String> createEdgeMap(String startLocation, String endLocation, int distance)
	{
		HashMap<String,String> result = new HashMap<String,String>();
		result.put(START_LOCATION_KEY, startLocation);
		result.put(END_LOCATION_KEY, endLocation);
		result.put(DISTANCE_KEY, Integer.toString(distance));
		return result;
	}
	
	
	public String getSavedOutputMapAsString()
	{
		if (!calculated)
		{
			return "Calculation has not been run.";
		}
		
		return getOutputMapAsString(savedOutputMap);
	}
	
	
	// Static version, for alternative way to display function's results
	public static String getOutputMapAsString(HashMap<String,String> outputMap)
	{
		String result;
		
		if (outputMap.size() == 0)
		{
			result = String.format("Calculation has not been run or was unable to reach the end location.");
		}
		else
		{
			result = String.format("%s -> %s, %s -> %s", DISTANCE_KEY, outputMap.get(DISTANCE_KEY), PATH_KEY, outputMap.get(PATH_KEY));
		}
		
		return result;
	}
	
	
	public int getDistanceFromSavedOutputMap()
	{
		if (!calculated)
		{
			return -1;
		}
		
		String distanceStr = savedOutputMap.get(DISTANCE_KEY);
		return distanceStr == null ? -1 : Integer.parseInt(distanceStr);
	}
	
	
	public String getPathFromSavedOutputMap()
	{
		if (!calculated)
		{
			return "Calculation has not been run.";
		}
		
		String path = savedOutputMap.get(PATH_KEY);
		return path == null ? "" : path;
	}
	
	
	public String[] getPathAsArrayFromSavedOutputMap()
	{
		if (!calculated)
		{
			return new String[0];
		}
		
		String path = savedOutputMap.get(PATH_KEY);
		return path.split(PATH_ELEMENT_SEPARATOR);
	}
	
	

}
