package tests;

import org.junit.*;
import java.util.Date;
import java.text.SimpleDateFormat;

public class PersonTest extends Assert {
    public static SimpleDateFormat df;
    private Person person;

    // Descriptions of the annotations and meanings are here:
    // http://www.asjava.com/junit/junit-tutorial-%E2%80%93-basic-usage-and-annotation/

    // Before each test, it will do a setup.  After each test, it will do a teardown.

    @BeforeClass
    public static void BeforeClass() {
        df = new SimpleDateFormat("yyyy-mm-dd");
        System.out.println("BeforeClass");
    }

    @Before
    public void setUp() throws Exception {
        Date date = df.parse("2005-10-14");
        person = new Person("Jammy", date);
        System.out.println("setUp");
    }

    @Test
    public void getName() {
        assertEquals(person.getName(), "Jammy");
        System.out.println("Test getName");
    }

    @Test
    public void getAge() {
        assertEquals(person.getAge(), 6);
        System.out.println("Test getAge");
    }

    @After
    public void tearDown() throws Exception {
        person = null;
        System.out.println("tearDown");
    }

    @AfterClass
    public static void AfterClass() {
        df = null;
        System.out.println("AfterClass");
    }
}