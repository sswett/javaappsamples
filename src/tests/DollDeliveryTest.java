package tests;

// junit javadoc: http://junit.org/javadoc/latest/

// IMPORTANT NOTE: The following static import doesn't come in automatically!!!! ... and it was key to fix compile
// errors.

import static org.junit.Assert.*;

import org.junit.Test;

import doll_delivery.RouteCalculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


// Doesn't seem to be needed
// @RunWith(JUnit4.class)

public class DollDeliveryTest
{
	
	static ArrayList<HashMap<String,String>> edgeList = new ArrayList<HashMap<String,String>>();
	
	static {
		edgeList.add(RouteCalculator.createEdgeMap("Kruthika's abode", "Mark's crib", 9));
		edgeList.add(RouteCalculator.createEdgeMap("Kruthika's abode", "Greg's casa", 4));
		edgeList.add(RouteCalculator.createEdgeMap("Kruthika's abode", "Matt's pad", 18));
		edgeList.add(RouteCalculator.createEdgeMap("Kruthika's abode", "Brian's apartment", 8));
		edgeList.add(RouteCalculator.createEdgeMap("Brian's apartment", "Wesley's condo", 7));
		edgeList.add(RouteCalculator.createEdgeMap("Brian's apartment", "Cam's dwelling", 17));
		edgeList.add(RouteCalculator.createEdgeMap("Greg's casa", "Cam's dwelling", 13));
		edgeList.add(RouteCalculator.createEdgeMap("Greg's casa", "Mike's digs", 19));
		edgeList.add(RouteCalculator.createEdgeMap("Greg's casa", "Matt's pad", 14));
		edgeList.add(RouteCalculator.createEdgeMap("Wesley's condo", "Kirk's farm", 10));
		edgeList.add(RouteCalculator.createEdgeMap("Wesley's condo", "Nathan's flat", 11));
		edgeList.add(RouteCalculator.createEdgeMap("Wesley's condo", "Bryce's den", 6));
		edgeList.add(RouteCalculator.createEdgeMap("Matt's pad", "Mark's crib", 19));
		edgeList.add(RouteCalculator.createEdgeMap("Matt's pad", "Nathan's flat", 15));
		edgeList.add(RouteCalculator.createEdgeMap("Matt's pad", "Craig's haunt", 14));
		edgeList.add(RouteCalculator.createEdgeMap("Mark's crib", "Kirk's farm", 9));
		edgeList.add(RouteCalculator.createEdgeMap("Mark's crib", "Nathan's flat", 12));
		edgeList.add(RouteCalculator.createEdgeMap("Bryce's den", "Craig's haunt", 10));
		edgeList.add(RouteCalculator.createEdgeMap("Bryce's den", "Mike's digs", 9));
		edgeList.add(RouteCalculator.createEdgeMap("Mike's digs", "Cam's dwelling", 20));
		edgeList.add(RouteCalculator.createEdgeMap("Mike's digs", "Nathan's flat",12));
		edgeList.add(RouteCalculator.createEdgeMap("Cam's dwelling", "Craig's haunt", 18));
		edgeList.add(RouteCalculator.createEdgeMap("Nathan's flat", "Kirk's farm", 3));
	}
	

	@Test
    public void testThatReturnedAndSavedOutputMapsAreTheSame()
    {
		RouteCalculator rc = new RouteCalculator();
		HashMap<String,String> outputMap = rc.getDistanceAndPathToDestination("Kruthika's abode", "Craig's haunt", edgeList);
		assertTrue(RouteCalculator.getOutputMapAsString(outputMap).equals(rc.getSavedOutputMapAsString()));
    }
	

	@Test
    public void testVariousDistances()
    {
		RouteCalculator rc = new RouteCalculator();
		HashMap<String,String> outputMap = rc.getDistanceAndPathToDestination("Kruthika's abode", "Craig's haunt", edgeList);
		assertTrue(outputMap.size() == 2);
		assertTrue(rc.getDistanceFromSavedOutputMap() == 31);
		
		rc.getDistanceAndPathToDestination("Greg's casa", "Wesley's condo", edgeList);
		assertTrue(rc.getDistanceFromSavedOutputMap() == 19);
		
		rc.getDistanceAndPathToDestination("Mark's crib", "Bryce's den", edgeList);
		assertTrue(rc.getDistanceFromSavedOutputMap() == 25);
    }
	

	@Test
    public void testSameDistanceForwardBackward()
    {
		RouteCalculator rc = new RouteCalculator();
		
		rc.getDistanceAndPathToDestination("Kruthika's abode", "Craig's haunt", edgeList);
		int distance1 = rc.getDistanceFromSavedOutputMap();
		
		rc.getDistanceAndPathToDestination("Craig's haunt", "Kruthika's abode", edgeList);
		int distance2 = rc.getDistanceFromSavedOutputMap();
		
		assertTrue(distance1 == distance2);

		
		rc.getDistanceAndPathToDestination("Mark's crib", "Bryce's den", edgeList);
		distance1 = rc.getDistanceFromSavedOutputMap();
		
		rc.getDistanceAndPathToDestination("Bryce's den", "Mark's crib", edgeList);
		distance2 = rc.getDistanceFromSavedOutputMap();
    }
	

	@Test
    public void testSameLocationsForwardBackward()
    {
		RouteCalculator rc = new RouteCalculator();
		
		rc.getDistanceAndPathToDestination("Kruthika's abode", "Craig's haunt", edgeList);
		String[] locationNames1 = rc.getPathAsArrayFromSavedOutputMap();
		
		rc.getDistanceAndPathToDestination("Craig's haunt", "Kruthika's abode", edgeList);
		String[] locationNames2 = rc.getPathAsArrayFromSavedOutputMap();
		
		assertTrue(locationNames1.length == locationNames2.length);
		assertTrue(doReverseArraysMatch(locationNames1, locationNames2));

    
		rc.getDistanceAndPathToDestination("Mark's crib", "Mike's digs", edgeList);
		locationNames1 = rc.getPathAsArrayFromSavedOutputMap();
		
		rc.getDistanceAndPathToDestination("Mike's digs", "Mark's crib", edgeList);
		locationNames2 = rc.getPathAsArrayFromSavedOutputMap();
		
		assertTrue(locationNames1.length == locationNames2.length);
		assertTrue(doReverseArraysMatch(locationNames1, locationNames2));
    
    }
	
	
	private boolean doReverseArraysMatch(String[] array1, String[] array2)
	{
		List<String> list1 = Arrays.asList(array1);
		List<String> list2 = Arrays.asList(array2);
		
		Collections.reverse(list2);
		
		int matches = 0;
		
		for (int x = 0; x < array1.length; x++)
		{
			if (list1.get(x).equals(list2.get(x)))
			{
				matches++;
			}
		}
		
		return array1.length == matches;
	}

	
    // An article that takes the above a little further:
    // http://www.javacodegeeks.com/2012/10/testing-custom-exceptions-with-junits.html


}
