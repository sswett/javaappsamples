package tests;

// junit javadoc: http://junit.org/javadoc/latest/

// IMPORTANT NOTE: The following static import doesn't come in automatically!!!! ... and it was key to fix compile
// errors.

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


// Doesn't seem to be needed
// @RunWith(JUnit4.class)

// class-level Ignore doesn't seem to work
// @Ignore("not today")
public class SimpleTest
{
    @Ignore("not today")
    @Test
    public void testEmptyCollection()
    {
        System.out.println("test :: testEmptyCollection");
        Collection collection = new ArrayList();

        assertNotNull(collection);
        assertTrue(collection.isEmpty());
    }


    // Example of catching an exception the old fashioned way
    @Test
    public void testIndexOutOfBoundsExceptionOne()
    {
        try
        {
            System.out.println("test :: IndexOutOfBoundsException One");
            List list = new ArrayList();
            list.get(1);

            // Normally, the following won't be reached because an exception should be given
            fail("This should throw IndexOutOfBoundsException");
        }
        catch (IndexOutOfBoundsException e)
        {
            // Validate  exception
            assertEquals(e.getMessage(),  "Index: 1, Size: 0");
        }
    }


    // Test fails for some reason
    @Test(expected = IndexOutOfBoundsException.class)
    public void testIndexOutOfBoundsExceptionTwo()
    {
        System.out.println("test :: IndexOutOfBoundsException Two");
        List list =  new ArrayList();
        list.get(1);
    }

    // Test fails for some reason.  The advantage of this test over above is ability to test the exception msg.
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testIndexOutOfBoundsExceptionThree()
    {
        System.out.println("test :: IndexOutOfBoundsException Three");
        exception.expect(IndexOutOfBoundsException.class);
        exception.expectMessage("Index: 1, Size: 0");
        List list =  new ArrayList();
        list.get(1);
    }

    // An article that takes the above a little further:
    // http://www.javacodegeeks.com/2012/10/testing-custom-exceptions-with-junits.html


}
