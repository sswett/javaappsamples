package tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class ParameterizedTest extends Assert
{
    String name;
    int age;


    public ParameterizedTest(String name, int age)
    {
        this.name = name;
        this.age =  age;
    }


    @Parameterized.Parameters
    public static Collection<Object[]> data()
    {
        Object[][] data = new  Object[][]
                { {"Parameterized test1", 10}, {"Parameterized test2", 20} };

        return Arrays.asList(data);
    }


    /*
        When the following test method is run, the "data" method is called first.  Then, for each object in the
        parameter collection, the constructor is called followed by "testOut" being called.
     */

    @Test
    public void testOut()
    {
        System.out.println(name + " " + age);
    }
}