package json;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReadGraphiteJSONDataApp
{
    final static boolean makeRealRequestToGraphiteServer = false;
    
    final static String graphiteServer = "graphite.server.com";   // fake one
    final static String serverToAnalyze = "server.domain";   // fake one  

    final static String fixedGraphiteResponseData = "[{\"target\": \"" + serverToAnalyze + ".GenericJMX.operating_system.gauge.system_load_average\", \"datapoints\": [[0.52, 1406318450], [0.59, 1406318460], [0.58, 1406318470], [0.81, 1406318480], [0.84, 1406318490], [0.79, 1406318500], [0.66, 1406318510], [0.64, 1406318520], [0.69, 1406318530], [0.74, 1406318540], [0.7, 1406318550], [0.75, 1406318560], [0.71, 1406318570], [0.68, 1406318580], [0.65, 1406318590], [0.63, 1406318600], [0.61, 1406318610], [0.59, 1406318620], [0.5, 1406318630], [0.5, 1406318640], [null, 1406318650], [null, 1406318660], [null, 1406318670], [null, 1406318680], [null, 1406318690], [null, 1406318700], [null, 1406318710], [null, 1406318720], [null, 1406318730], [null, 1406318740], [null, 1406318750], [null, 1406318760], [null, 1406318770], [null, 1406318780], [null, 1406318790], [null, 1406318800], [null, 1406318810], [null, 1406318820], [null, 1406318830], [null, 1406318840], [null, 1406318850], [null, 1406318860], [null, 1406318870], [null, 1406318880], [null, 1406318890], [null, 1406318900], [null, 1406318910], [null, 1406318920], [null, 1406318930], [null, 1406318940], [null, 1406318950], [null, 1406318960], [null, 1406318970], [null, 1406318980], [null, 1406318990], [null, 1406319000], [null, 1406319010], [null, 1406319020], [null, 1406319030], [null, 1406319040]]}]";


    /**
     *
     * @param args - first argument is user, second is password (to Graphite server)
     */
    public static void main(String[] args)
    {
        final JSONArray jsonArray;

        try
        {
            if (makeRealRequestToGraphiteServer)
            {
                jsonArray = new JSONArray(getResponseFromGraphiteRequest(args[0], args[1]));
            }
            else
            {
                jsonArray = new JSONArray(fixedGraphiteResponseData);
            }

            processRootArray(jsonArray);
        }
        catch (JSONException e)
        {
            System.out.println("main error: " + e.getMessage());
            e.printStackTrace();
        }
    }


    private static String getResponseFromGraphiteRequest(String user, String pwd)
    {
        SimpleHttpConnectionManager httpConnMgr = new SimpleHttpConnectionManager();
        HttpClient httpClient = new HttpClient(httpConnMgr);
        Credentials credentials = new UsernamePasswordCredentials(user, pwd);
        httpClient.getState().setCredentials(AuthScope.ANY, credentials);

        org.apache.commons.httpclient.HttpMethod method = null;

        try
        {
            method = new GetMethod("http://" + graphiteServer + "/render?target=*" + serverToAnalyze + ".GenericJMX.operating_system.gauge.system_load_average&from=-10minutes&format=json");
            httpClient.executeMethod(method);
            String response = method.getResponseBodyAsString();
            return response;
        }
        catch(Exception e)
        {
            System.out.println("response error: " + e.getMessage());
            e.printStackTrace();
            return "[]";
        }
        finally
        {
            if (method != null)
            {
                try
                {
                    method.releaseConnection();
                }
                catch(Exception e2)
                {

                }
            }
        }
    }


    private static void processRootArray(JSONArray rootArray)
    {
        System.out.println("# of root array elements: " + rootArray.length());

        try
        {
            for (int x = 0; x < rootArray.length(); x++)
            {
                Object obj = rootArray.get(x);
                JSONObject jsonObject = (JSONObject) obj;
                Object target = jsonObject.get("target");   // object type is String

                String metricName = (String) target;
                System.out.println("metricName: " + metricName);

                Object datapoints = jsonObject.get("datapoints");   // object type is JSONArray

                // System.out.println(target.toString());
                // System.out.println(datapoints.toString());

                JSONArray dataPtsArray = (JSONArray) datapoints;
                processDataPointsArray(dataPtsArray);
            }
        }
        catch(JSONException e)
        {
            System.out.println("Root error: " + e.getMessage());
            e.printStackTrace();
        }
    }



    private static void processDataPointsArray(JSONArray dataPtsArray)
    {
        System.out.println("# of datapoints array elements: " + dataPtsArray.length());

        try
        {
            for (int x = 0; x < dataPtsArray.length(); x++)
            {
                Object objX = dataPtsArray.get(x);
                JSONArray valuesArray = (JSONArray) objX;

                // System.out.println(valuesArray.toString());

                int metricTime = valuesArray.getInt(1);

                if (valuesArray.isNull(0))
                {
                    System.out.println("x: " + x + "  metricValue: null" + "  metricTime: " + metricTime);
                }
                else
                {
                    double metricValue = valuesArray.getDouble(0);
                    System.out.println("x: " + x + "  metricValue: " + metricValue + "  metricTime: " + metricTime);
                }
            }
        }
        catch(JSONException e)
        {
            System.out.println("Datapoints error: " + e.getMessage());
            e.printStackTrace();
        }
    }


}
