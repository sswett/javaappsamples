package strings;

public class TestObjectCountActions
{
    StringBuilder sb = new StringBuilder();


    public void testObjectCount(boolean useTempObject, int outerTimes, int innerTimes)
    {
        int overallCount = 0;

        for (int o = 0; o < outerTimes; o++)
        {
            for (int i = 0; i < innerTimes; i++)
            {
                String outStr;

                if (useTempObject)
                {
                    String tempObj = getStringObject(o, i);
                    outStr = new String(tempObj);

                }
                else
                {
                    outStr = new String(getStringObject(o, i));
                }

                if (overallCount++ < 10)
                {
                    System.out.println(outStr);
                }

            }
        }
    }


    private String getStringObject(int outerCount, int innerCount)
    {
        // return "Outer count =" + outerCount + " Inner count =" + innerCount;

        // StringBuilder sb = new StringBuilder();

        // This was definitely the best approach, with an "sb" defined as a non-static class instance
        sb.setLength(0);
        sb.append("Outer count =").append(outerCount).append(" Inner count =").append(innerCount);
        return sb.toString();

        // return getConcatenatedString("Outer count =", outerCount, " Inner count =", innerCount );
    }


    private String getConcatenatedString(Object... object)
    {
        sb.setLength(0);

        // These 2 loop types had about the same footprint

        /*
        for (Object obj : object)
        {
            sb.append(obj);
        }
         */

        for (int x = 0; x < object.length; x++)
        {
            sb.append(object[x]);
        }

        return sb.toString();

    }

}
