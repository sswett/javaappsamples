package strings;

import java.util.Scanner;

public class TestStringMemoryAndPerformanceApp
{

    final static int OUTER_LOOP_TIMES = 500;
    final static int INNER_LOOP_TIMES = 100;


    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        TestStringActions testStringActions = new TestStringActions();

        while (true)
        {
            System.out.println();

            for (TestStringEnum tse : TestStringEnum.values())
            {
                System.out.println(tse.menuLetter() + " = " + tse.description());
            }

            System.out.println("q = quit");
            String consoleResponse = input.next();

            if (consoleResponse.equalsIgnoreCase("q"))
            {
                break;
            }

            TestStringEnum tse = TestStringEnum.getEnum(consoleResponse);

            if (tse == null)
            {
                System.out.println("\nInvalid input (case sensitive).  Please try again.");
            }
            else
            {
                testStringActions.testString(tse, OUTER_LOOP_TIMES, INNER_LOOP_TIMES);
                System.out.println("\nFinished\n");
            }
        }
    }


}