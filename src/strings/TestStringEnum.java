package strings;

public enum TestStringEnum
{
    A_LITERAL_PLUS_VARIABLE_OF_WIDTH_5("a", "literal plus variable of width 5")
    ,
    B_LITERAL_PLUS_VARIABLE_OF_VARIABLE_WIDTH("b", "literal plus variable of variable width")
    ,
    C_LITERAL_PLUS_LITERAL_OF_WIDTH_5("c", "literal plus literal of width 5")
    ,
    D_LITERAL_PLUS_VARIABLE_OF_WIDTH_5_WITH_REUSE("d", "literal plus variable of width 5 - with re-use")
    ,
    E_LITERAL_PLUS_VARIABLE_OF_VARIABLE_WIDTH_WITH_REUSE("e", "literal plus variable of variable width - with re-use")
    ,
    F_LITERAL_PLUS_LITERAL_OF_WIDTH_5_WITH_REUSE("f", "literal plus literal of width 5 - with re-use")
    ;


    private final String menuLetter;
    private final String description;


    TestStringEnum(String menuLetter, String description)
    {
        this.menuLetter = menuLetter;
        this.description = description;
    }


    String menuLetter()
    {
        return menuLetter;
    }


    String description()
    {
        return description;
    }


    static TestStringEnum getEnum(String menuLetter)
    {
        for (TestStringEnum tse : TestStringEnum.values())
        {
            if (tse.menuLetter().equals(menuLetter))
            {
                return tse;
            }
        }

        return null;
    }

}
