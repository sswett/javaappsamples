package strings;

import java.util.HashMap;

public class TestStringActions
{

    private HashMap<String,String> reusedStrings = new HashMap<String, String>(256);


    public void testString(TestStringEnum tse, int outerTimes, int innerTimes)
    {
        for (int o = 0; o < outerTimes; o++)
        {
            for (int i = 0; i < innerTimes; i++)
            {
                CustomString cs = new CustomString();

                /*
                A - C are without reuseString
                D - F are with reuseString

                Compare A to D, B to E, C to F -- I don't see an improvement

                 */

                if (tse == TestStringEnum.A_LITERAL_PLUS_VARIABLE_OF_WIDTH_5)
                {
                    cs.value = "output " + String.format("%5d", i);
                    // String formatter chews up memory; triggered 2 GCs (before I increased memory to prevent)

                    // Results in 50,000 CustomString objects with 800,000 bytes retained size
                    // Total objects = 1,890,256; total size = 80 MB
                    // Duplicate strings: 625
                }

                if (tse == TestStringEnum.B_LITERAL_PLUS_VARIABLE_OF_VARIABLE_WIDTH)
                {
                    cs.value = "output " + i;

                    // Results in 50,000 CustomString objects with 800,000 bytes retained size
                    // Total objects = 290,149; total size = 29 MB
                    // Duplicate strings: 623
                }

                if (tse == TestStringEnum.C_LITERAL_PLUS_LITERAL_OF_WIDTH_5)
                {
                    cs.value = "output " + "fiver";

                    // Results in 50,000 CustomString objects with 800,000 bytes retained size
                    // Total objects = 89,869; total size = 21 MB
                    // Duplicate strings: 623
                }

                if (tse == TestStringEnum.D_LITERAL_PLUS_VARIABLE_OF_WIDTH_5_WITH_REUSE)
                {
                    cs.value = reuseString("output " + String.format("%5d", i));

                    // Results in 50,000 CustomString objects with 800,000 bytes retained size
                    // Total objects = 1,890,378; total size = 80 MB
                    // Duplicate strings: 625
                }

                if (tse == TestStringEnum.E_LITERAL_PLUS_VARIABLE_OF_VARIABLE_WIDTH_WITH_REUSE)
                {
                    cs.value = reuseString("output " + i);

                    // Results in 50,000 CustomString objects with 800,000 bytes retained size
                    // Total objects = 290,180; total size = 29 MB
                    // Duplicate strings: 623
                }

                if (tse == TestStringEnum.F_LITERAL_PLUS_LITERAL_OF_WIDTH_5_WITH_REUSE)
                {
                    cs.value = reuseString("output " + "fiver");

                    // Results in 50,000 CustomString objects with 800,000 bytes retained size
                    // Total objects = 90,034; total size = 21 MB
                    // Duplicate strings: 623
                }

            }
        }
    }


    private String reuseString(String input)
    {
        if (!reusedStrings.containsKey(input))
        {
            reusedStrings.put(input, input);
        }

        return reusedStrings.get(input);
    }

}
