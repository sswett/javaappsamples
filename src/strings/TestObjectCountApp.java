package strings;

import java.util.Scanner;

public class TestObjectCountApp
{

    final static int OUTER_LOOP_TIMES = 500;
    final static int INNER_LOOP_TIMES = 100;


    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        TestObjectCountActions actions = new TestObjectCountActions();

        while (true)
        {
            System.out.println();
            System.out.println("a = USE temp string object");
            System.out.println("b = DO NOT USE temp string object");
            System.out.println("q = quit");

            String consoleResponse = input.next();

            if (consoleResponse.equalsIgnoreCase("a"))
            {
                actions.testObjectCount(true, OUTER_LOOP_TIMES, INNER_LOOP_TIMES);
                System.out.println("\nFinished\n");
            }

            else if (consoleResponse.equalsIgnoreCase("b"))
            {
                actions.testObjectCount(false, OUTER_LOOP_TIMES, INNER_LOOP_TIMES);
                System.out.println("\nFinished\n");
            }


            else if (consoleResponse.equalsIgnoreCase("q"))
            {
                break;
            }

            else
            {
                System.out.println("\nInvalid input (case sensitive).  Please try again.");
            }
        }
    }


}