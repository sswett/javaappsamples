package strings;
public class TestStringReplacementWithRegexApp
{
  public static void main(String[] args) {

    /*
        Some business logic rule: "The following special characters are not allowed in the New Name: < > \" \\ ;"
     */

      String stringIn1 = "This has <html-enclosed> and \"quote-surrounded\" and \\back-slashed\\ and ;semi-coloned; bad stuff.";
      String stringOut1 = stringIn1.replaceAll("<|>|\"|\\\\|;", " ");

      System.out.println("stringIn1  value is: " + stringIn1);
      System.out.println("stringOut1 value is: " + stringOut1);


  }
}
