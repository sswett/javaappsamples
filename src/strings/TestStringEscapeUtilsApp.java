package strings;
import org.apache.commons.lang3.StringEscapeUtils;

public class TestStringEscapeUtilsApp
{
    public static void main(String[] args)
    {
        String resTitleInDb = "&#22826;&#31062;&#27494;&#30343;&#24093;&#65292;&#27803;&#22283;";
        System.out.println("resTitleInDb = " + resTitleInDb);

        String titleAsDisplayedInHtml = StringEscapeUtils.unescapeHtml4(resTitleInDb);
        System.out.println("titleAsDisplayedInHtml = " + titleAsDisplayedInHtml);

        doAbbreviationTest(titleAsDisplayedInHtml);
        doLousTest();
    }


    public static void doAbbreviationTest(String titleAsDisplayedInHtml)
    {
        String abbreviatedTitleAsDisplayedInHtml = titleAsDisplayedInHtml.substring(0, 5);
        System.out.println("abbreviatedTitleAsDisplayedInHtml = " + abbreviatedTitleAsDisplayedInHtml);

        String abbreviatedTitleWithHtmlEntityReferences =
                StringEscapeUtils.escapeJava(abbreviatedTitleAsDisplayedInHtml) + "...";

        System.out.println("abbreviatedTitleWithHtmlEntityReferences = " +
                abbreviatedTitleWithHtmlEntityReferences + " (uses hex but equivalent to 'resTitleInDb' values)");

        // String[] splits = abbreviatedTitleWithHtmlEntityReferences.split("\\" + "u");

        // System.out.println("splits length = " + splits.length);

        /*
        try
        {
            // String escaped1 = StringEscapeUtils.escapeHtml4(abbreviatedTitleWithHtmlEntityReferences);   // didn't work
            // String escaped1 = new String(abbreviatedTitleWithHtmlEntityReferences.getBytes("UTF-8"), "ISO-8859-1");
            // String escaped1 = new String(abbreviatedTitleWithHtmlEntityReferences.getBytes(), "ISO-8859-1");
            // String escaped1 = new String(abbreviatedTitleWithHtmlEntityReferences.getBytes("ISO-8859-1"));
            String escaped1 = new String(abbreviatedTitleWithHtmlEntityReferences.getBytes("UTF8"));
            System.out.println("escaped1 = " + escaped1);
            String escaped2 = StringEscapeUtils.escapeHtml4(escaped1);
            System.out.println("escaped2 = " + escaped2);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        */
    }


    public static void doLousTest()
    {
        System.out.println("\n\n\nLou's test\n\n");

        String s = "foo bar &#22826; faz baz";
        System.out.println("s = " + s);
        String s2 = StringEscapeUtils.unescapeHtml4(s);
        System.out.println("s2 = " + s2);
        String s3 = StringEscapeUtils.escapeHtml4(s2);
        System.out.println("s3 = " + s3);

        boolean sMatchesS3 = s.equals(s3);
        System.out.println("do s and s3 match: " + sMatchesS3);
    }
}
