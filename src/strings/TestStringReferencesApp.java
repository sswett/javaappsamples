package strings;
import java.util.*;

public class TestStringReferencesApp {
  public static void main(String[] args) {


      String string1 = "Steve";
      String string2 = string1;

      System.out.println("string1 value is: " + string1);
      System.out.println("string2 value is: " + string2);

      string1 = "Fred";

      System.out.println("string1 value is: " + string1);
      System.out.println("string2 value is now: " + string2);

      final String string3 = "Joe";
      String string4 = string3;

      if (string3 == string4)
      {
          System.out.println("string3 and string4 reference the same object.");
      }
      else
      {
          System.out.println("string3 and string4 do NOT reference the same object.");
      }

  }
}