package junk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestItApp {

	public static void main(String[] args) {

		ArrayList<Animal> list = new ArrayList<Animal>();
		
		list.add(new Dog());
		list.add(new Cat());
		
		for (Animal animal : list)
		{
			System.out.println(animal.makeSound());
		}
		
		
		Animal[] array = list.toArray(new Animal[list.size()]);
		
		System.out.println("Array size = " + array.length);
		
		List<Animal> list2 = Arrays.asList(array);
		
		
	}

}
