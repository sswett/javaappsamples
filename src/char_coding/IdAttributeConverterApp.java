package char_coding;
import java.util.*;

public class IdAttributeConverterApp {

    private static final char  UPPERA_CHAR  = (int)'A';
    private static final char  UPPERZ_CHAR  = (int)'Z';
    private static final char  LOWERA_CHAR  = (int)'a';
    private static final char  LOWERZ_CHAR  = (int)'z';
    private static final char  ZERO_CHAR  =   (int)'0';
    private static final char  NINE_CHAR  =   (int)'9';
    private static final char  HYPHEN_CHAR  =   (int)'-';
    private static final char  UNDERSCORE_CHAR  =   (int)'_';

    public static void main(String[] args) {

        String converted = coerceAsHtmlIdAttribute("root-x1689347621234--sep--A Test & Try Folder'<>\"");

        System.out.print(converted);

    }

    public static String coerceAsHtmlIdAttribute(String stringIn)
    {
        char[] chars = stringIn.toCharArray();

        for (int c = 0; c < chars.length; c++)
        {
            char oneChar = chars[c];
            boolean isGood = false;

            if (oneChar == HYPHEN_CHAR || oneChar == UNDERSCORE_CHAR)
            {
                isGood = true;
            }
            else if (oneChar >= UPPERA_CHAR && oneChar <= UPPERZ_CHAR)
            {
                isGood = true;
            }
            else if (oneChar >= LOWERA_CHAR && oneChar <= LOWERZ_CHAR)
            {
                isGood = true;
            }
            else if (oneChar >= ZERO_CHAR && oneChar <= NINE_CHAR)
            {
                isGood = true;
            }

            if (!isGood)
            {
                chars[c] = UNDERSCORE_CHAR;
            }

        }

        return new String(chars);

    }

}