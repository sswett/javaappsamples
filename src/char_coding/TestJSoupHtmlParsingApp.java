package char_coding;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;

import java.util.Date;

public class TestJSoupHtmlParsingApp
{
    public static void main(String[] args)
    {
        Runtime runtime = Runtime.getRuntime();

        String resTitleInDb = "&#22826;&#31062;&#27494;&#30343;&#24093;&#65292;&#27803;&#22283;";
        System.out.println("resTitleInDb = " + resTitleInDb);

        Date start = new Date();
        long startingFreeMemory = runtime.freeMemory();

        // Following is a test of one million runs.  It performs fine.  It will even perform with the max heap size
        // set to only 256 MB.  Here are some stats:

        //  iterations  time (ms)   memory used     PSYoungGen GCs (none full)
        //  1           109ms       3.3 MB          0
        //  100         151ms       4 MB            0
        //  1,000       492ms       13 MB           0
        //  10,000      630ms       19 MB           2
        //  100,000     1,178ms     -208 MB         9
        //  1,000,000   5,388ms     -120 MB         51

        for (int x = 0; x < 1; x ++)
        {
            doParseAndAbbreviationTest(resTitleInDb);
        }

        long endingFreeMemory = runtime.freeMemory();
        Date end = new Date();
        long elapsedMs = end.getTime() - start.getTime();
        System.out.println("\nElapsed time in ms = " + elapsedMs);


        System.out.println("\nUsed memory (bytes) = " + (startingFreeMemory - endingFreeMemory));
    }


    public static void doParseAndAbbreviationTest(String resTitleInDb)
    {
        Document doc = Jsoup.parseBodyFragment(resTitleInDb);
        String titleAsDisplayedInHtml = doc.body().text();
        System.out.println("titleAsDisplayedInHtml = " + titleAsDisplayedInHtml);

        String abbreviatedTitleAsDisplayedInHtml = titleAsDisplayedInHtml.substring(0, 5);
        System.out.println("abbreviatedTitleAsDisplayedInHtml = " + abbreviatedTitleAsDisplayedInHtml);

        doc = Jsoup.parseBodyFragment(abbreviatedTitleAsDisplayedInHtml);
        doc.outputSettings().escapeMode(Entities.EscapeMode.extended);   // base is default
        doc.outputSettings().charset("ISO-8859-1");
        String abbreviatedTitleWithHtmlEntityReferences = doc.body().html() + "...";

        // Following uses hex but equivalent to 'resTitleInDb' values
        System.out.println("abbreviatedTitleWithHtmlEntityReferences = " + abbreviatedTitleWithHtmlEntityReferences);
    }


    // Turns out I didn't need this.  The StringEscapeUtils.escape and unescape did the
    // trick.  I almost added the following methods to HtmlUtils, but ended up not needing them.

    /*
        public static String unescapeHtmlUsingJsoup(String input)
    {
        return Jsoup.parseBodyFragment(input).body().text();
    }


    public static String escapeHtmlUsingJsoup(String input)
    {
        org.jsoup.nodes.Document doc = Jsoup.parseBodyFragment(input);
        doc.outputSettings().escapeMode(Entities.EscapeMode.extended);   // Entities.EscapeMode.base is default
        doc.outputSettings().charset("ISO-8859-1");
        return doc.body().html();
    }

     */
}
