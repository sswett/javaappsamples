package geometry;

public class Rectangle {

	public static int rectangleCount = 0;
	
	private String name;
	private int topSideLength;
	private int rightSideLength;
	
	public Rectangle(String rectName, int rectTopSideLength, int rectRightSideLength)
	{
		name = rectName;
		topSideLength = rectTopSideLength;
		rightSideLength = rectRightSideLength;
		rectangleCount++;
	}
	
	public int getArea()
	{
		return topSideLength * rightSideLength;
	}
	
	public String getName()
	{
		return name;
	}
	
	public static String getNameOfLargerRectangle(Rectangle rect1, Rectangle rect2)
	{
		if (rect1.getArea() > rect2.getArea())
		{
			return rect1.getName();
		}
		else
		{
			return rect2.getName();
		}
	}
}
