package geometry;

public class RectangleTesterApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Rectangle rectA = new Rectangle("our front yard", 70, 40);
		Rectangle rectB = new Rectangle("piece of carpet", 15, 18);
		Rectangle rectC = new Rectangle("our house", 40, 30);

		Rectangle[] rectangles = { rectA, rectB, rectC };
		for (Rectangle rectangle : rectangles)
		{
			System.out.println("Rectangle info");
			System.out.println("   Name: " + rectangle.getName());
			System.out.println("   Area: " + rectangle.getArea());
			System.out.println();
		}
		
		System.out.println("The bigger of '" + rectB.getName() + "' and '" +
				rectC.getName() + "' is: '" +
				Rectangle.getNameOfLargerRectangle(rectB, rectC) + "'");
		
		System.out.println();
		
		System.out.println("The number of Rectangle instances is: " + 
				Rectangle.rectangleCount);
	}

}
