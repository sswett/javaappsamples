package rss;
import java.net.URL;
import java.io.IOException;
import java.util.List;
import java.util.Iterator;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.fetcher.FeedFetcher;
import com.sun.syndication.fetcher.FetcherException;
import com.sun.syndication.fetcher.impl.FeedFetcherCache;
import com.sun.syndication.fetcher.impl.HashMapFeedInfoCache;
import com.sun.syndication.fetcher.impl.HttpURLFeedFetcher;
import com.sun.syndication.io.FeedException;


public class RssFeed {
    
    private String _feedUrlString = "";
    private SyndFeed _feed = null;
    
    
    /** Sets to NULL if there is a problem constructing the feed
     * 
     * @param feedUrlString
     */ 
    public RssFeed (String feedUrlString)
    {
        _feedUrlString = feedUrlString;
        
        FeedFetcherCache feedInfoCache = HashMapFeedInfoCache.getInstance();
        FeedFetcher feedFetcher = new HttpURLFeedFetcher(feedInfoCache);
        
        try
        {
            _feed = feedFetcher.retrieveFeed(new URL(feedUrlString));
        }
        catch(IOException e)
        {
        }
        catch(FeedException e)
        {
        }
        catch(FetcherException e)
        {
        }
        
    }
    
    
    public String getFeedResponseAsString()
    {
        StringBuilder result = new StringBuilder();
        
            result.append(getFeedHeaderString());
            result.append(getFeedEntriesString());
        
        return result.toString();
    }
    
    
    private String getFeedHeaderString()
    {
        StringBuilder result = new StringBuilder();
        
        result.append("Title: " + _feed.getTitle() + "\n");
        result.append("Description: " + _feed.getDescription() + "\n");
        result.append("Published Date: " + _feed.getPublishedDate() + "\n");
        result.append("Copyright: " + _feed.getCopyright() + "\n");
        
        return result.toString();
    }
    
    
    private String getFeedEntriesString()
    {
        StringBuilder result = new StringBuilder();
        
        List feedEntries = _feed.getEntries();
        Iterator iter = feedEntries.iterator();
        
        while (iter.hasNext())
        {
            SyndEntryImpl entry = (SyndEntryImpl) iter.next();
            result.append("\n\n");
            result.append("Title: " + entry.getTitle() + "\n");
            result.append("Link: " + entry.getLink() + "\n");
            result.append("Published Date: " + entry.getPublishedDate() + "\n");
            result.append("Description: " + entry.getDescription().getValue() + "\n");
        }
        
        return result.toString();
        
    }
    
    
    public String getFeedResponseAsHtml()
    {
        StringBuilder result = new StringBuilder();
        
            result.append(getFeedHeaderHtml());
            result.append(getFeedEntriesHtml());
        
        return result.toString();
    }
    
    
    private String getFeedHeaderHtml()
    {
        StringBuilder result = new StringBuilder();
        
        result.append("<div class=\"rssFeedHeader\">\n");
        
        result.append("<span title=\"" + _feed.getDescription() + "\"><b>" + _feed.getTitle() + "</b></span><br/>\n");
        result.append(_feed.getPublishedDate() + "&nbsp;&nbsp;&nbsp;" + _feed.getCopyright() + "<br/>\n");
        
        result.append("</div>\n\n");
        
        return result.toString();
    }
    
    
    private String getFeedEntriesHtml()
    {
        StringBuilder result = new StringBuilder();
        
        result.append("<div class=\"rssFeedEntries\">\n\n");
        
        List feedEntries = _feed.getEntries();
        Iterator iter = feedEntries.iterator();
        
        while (iter.hasNext())
        {
            SyndEntryImpl entry = (SyndEntryImpl) iter.next();
            result.append("<br/>\n");
            result.append("<a href=\"" + entry.getLink() + "\">" + entry.getTitle() + "</a>");
            
            if ( !_feed.getPublishedDate().toString().equals(entry.getPublishedDate().toString())  )
            {
                result.append(" - " + entry.getPublishedDate());
            }
            
            result.append("<br/>\n");

            String descr = entry.getDescription().getValue();
            if ( (descr != null) && (!descr.equals("")) )
            {
                result.append(descr + "<br/>\n");
            }

        }
        
        result.append("\n</div>");
        
        return result.toString();
        
    }
    
    
}
