package rss;

public class TestRssFetchApp {
    
    public static void main(String[] args)
    {
        
        String feedUrl = "http://rss.cnn.com/rss/cnn_topstories.rss"; 
        // String feedUrl = "http://rss.weather.com/weather/rss/local/49306?cm_ven=LWO&cm_cat=rss&par=LWO_rss"; 
        RssFeed myFeed = new RssFeed(feedUrl);
        if (myFeed != null)
        {
            System.out.println(myFeed.getFeedResponseAsString());
            // System.out.println(myFeed.getFeedResponseAsHtml());
        }
        
    }
    
    
}
