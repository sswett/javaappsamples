package serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializeEmployeeApp
{
    public static void main(String [] args)
    {
       Employee e = new Employee();
       e.name = "Steve Swett";
       e.address = "5788 Dix Dr NE";
       e.SSN = 111223333;
       e.number = 49;

       try
       {

           FileOutputStream fileOut = new FileOutputStream("/junk/employee.ser");
           ObjectOutputStream out = new ObjectOutputStream(fileOut);
           out.writeObject(e);
           out.close();
           fileOut.close();
       }
       catch(IOException i)
       {
           i.printStackTrace();
       }
    }
}
