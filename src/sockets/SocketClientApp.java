package sockets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Random;


public class SocketClientApp {
	
    private static final Logger errorLogger = LoggerFactory.getLogger("socket.client.error");
    private static final Logger warnLogger = LoggerFactory.getLogger("socket.client.warn");
    private static final Logger visitURLLogger = LoggerFactory.getLogger("socket.client.visit.url");

	private static final int SOCKET_TIMEOUT_MS = 5000;
	
	private static final String[] WEB_SITES = {
		"http://www.wikipedia.org",
		"http://www.google.com",
		"http://tennis.zbasu.net",
		"http://www.stackoverflow.com",
		"http://cnn.com",
		"http://abcnews.com",
		"http://www.mvpsportsclubs.com/michigan-rockford-mvp-athletic-club",
		"http://www.thinkful.com/",
		"http://guides.rubyonrails.org/",
		"http://maven.apache.org/",
		"http://spring.io/",
		"https://www.youtube.com/",
		"http://compass-style.org/",
		"http://nodejs.org/",
		"https://github.com/"
	};
	

	public static void main(String[] args) {

		// Make 5 random requests to web site via running SocketServerApp.  Logging will show output.
		
		for (int x = 0; x < 5; x++)
		{
			int index = new Random().nextInt(WEB_SITES.length);
			boolean success = makeVisitURLRequest(WEB_SITES[index]);
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

	
    private static boolean makeVisitURLRequest(String urlStr)
    {
        boolean success = false;
        Socket socket = null;

        try
        {
            // We may want to use a protocol for the communication.  Presently, a simple "HI", make request, and
            // "BYE" are used.

            socket = new Socket();

            // Used to be able to pass a socket timeout value here.  Must be another way.
            InetSocketAddress addr = new InetSocketAddress(SocketServerApp.SERVER_HOST_NAME, SocketServerApp.SERVER_PORT);
            socket.connect(addr);

            PrintWriter clientRequestWriter = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader serverResponseReader = new BufferedReader( new InputStreamReader(socket.getInputStream()));

            final String serverResponse = serverResponseReader.readLine();

            if (serverResponse == null || !serverResponse.equals(ServerResponseType.HI.name()))
            {
                errorLogger.error("Did not receive expected '{}' response from server.  " +
                        "Unable to make request.", ServerResponseType.HI.name());

                return false;
            }

            success = sendVisitURLRequest(clientRequestWriter, serverResponseReader, urlStr);

            sendByeRequest(clientRequestWriter);
        }
        catch (UnknownHostException uhe)
        {
            errorLogger.error("UnknownHostException from server {}.  " +
                    "Unable to make request.", SocketServerApp.SERVER_HOST_NAME);
        }
        catch (SocketTimeoutException ioe)
        {
            errorLogger.error("SocketTimeoutException from server {}.  " +
            		"Unable to make request.", SocketServerApp.SERVER_HOST_NAME);
        }
        catch (IOException ioe)
        {
            errorLogger.error("IOException from server {}.  " +
            		"Unable to make request.", SocketServerApp.SERVER_HOST_NAME);
        }
        finally
        {
        	try
            {
                if (socket != null)
                {
                    socket.close();   // this closes the input and output streams, too
                }
            }
            catch (IOException ioe)
            {
                // eat
            }
        }
        
        return success;
    }
    
    
    private static boolean sendVisitURLRequest(PrintWriter clientRequestWriter,
    		BufferedReader serverResponseReader, String urlStr)
    {
    	final String clientRequest = ClientRequestType.VISIT_URL.name() + "=" +
    			urlStr;

    	visitURLLogger.trace("Sending the following request to {}: {}", SocketServerApp.SERVER_HOST_NAME, clientRequest);
    	clientRequestWriter.println(clientRequest);

    	final String serverResponse;

    	try
    	{
    		serverResponse = serverResponseReader.readLine();
    		visitURLLogger.trace("Received the following response from {}: {}", SocketServerApp.SERVER_HOST_NAME, serverResponse);
    	}
    	catch (IOException ioe)
    	{
    		errorLogger.error("IOException from server {}.  " +
    				"Unable to send 'visit URL' request.",
    				SocketServerApp.SERVER_HOST_NAME);

    		return false;
    	}

    	return serverResponse != null && serverResponse.equals(ServerResponseType.OK.name());
    }


    private static void sendByeRequest(PrintWriter clientRequestWriter)
    {
        final String checkerRequest = ClientRequestType.BYE.name();
        clientRequestWriter.println(checkerRequest);

    }
    
    
	
	
}
