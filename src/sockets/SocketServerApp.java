package sockets;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;

import utilities.ExceptionUtils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class SocketServerApp {

	private static final Logger errorLogger = LoggerFactory.getLogger("socket.server.error");
	private static final Logger warnLogger = LoggerFactory.getLogger("socket.server.warn");
	private static final Logger infoLogger = LoggerFactory.getLogger("socket.server.info");


	static final String SERVER_HOST_NAME = "127.0.0.1";
	static final int SERVER_PORT = 64999;


	public static void main(String[] args) 
	{
		ServerSocket serverSocket = null;

		try
		{
			serverSocket = new ServerSocket(SERVER_PORT);

			while (serverSocket != null)
			{
				Socket clientSocket = null;

				try
				{

					// The loop with sleep is to avoid hogging CPU
					try
					{
						Thread.sleep(1000L);
					}
					catch (InterruptedException ie)
					{
						warnLogger.warn("InterruptedException occurred.\nMessage: {}", ExceptionUtils.getCauseMessage(ie));
					}

					infoLogger.trace("Listening for client requests on port {}", SERVER_PORT);
					clientSocket = serverSocket.accept();
					OutputStream responseOutputStream = clientSocket.getOutputStream();
					PrintWriter responseOutput = new PrintWriter(responseOutputStream, true);
					InputStreamReader clientSocketInputStreamReader = new InputStreamReader(clientSocket.getInputStream());
					BufferedReader clientRequestReader = new BufferedReader( clientSocketInputStreamReader );

					responseOutput.println(ServerResponseType.HI.name());

					String clientRequest;

					while ((clientRequest = clientRequestReader.readLine()) != null)
					{
						if (clientRequest.equals(ClientRequestType.BYE.name()))
						{
							break;
						}

						if (clientRequest != null)
						{
							final String[] requestParts = clientRequest.split("=");

							try
							{
								ClientRequestType requestType = ClientRequestType.valueOf(requestParts[0]);

								if (requestType == ClientRequestType.VISIT_URL)
								{
									String urlStr = requestParts[1];
									String docTitle = getDocumentTitleFromSite(urlStr);
									responseOutput.println(ServerResponseType.OK.name() + " - " + docTitle);
									infoLogger.trace("Successfully handled URL: {}; returned doc title: {}", urlStr, docTitle);
								}
							}
							catch (IllegalArgumentException iae)
							{
								errorLogger.error("Unable to perform request: {}.\nMessage: {}",
										clientRequest, ExceptionUtils.getCauseMessage(iae));
							}
						}
					}
				}
				catch (IOException e)
				{
					warnLogger.warn("IOException occurred.\nMessage: {}", ExceptionUtils.getCauseMessage(e));
				}
				finally
				{
					try
					{
						if (clientSocket != null)
						{
							clientSocket.close();   // This closes the input and output streams, too
						}
					}
					catch (Exception e)
					{
						// eat
					}
				}
			}
		}
		catch (IOException ioe)
		{
			errorLogger.error("Could not establish server socket on port {}.\nMessage: {}",
					SERVER_PORT, ExceptionUtils.getCauseMessage(ioe));
		}
		finally
		{
			try
			{
				if (serverSocket != null)
				{
					serverSocket.close();   // this closes the input and output streams, too
				}
			}
			catch (Exception e)
			{
				// eat
			}
		}
	}


    private static String getDocumentTitleFromSite(String urlStr)
    {
        SimpleHttpConnectionManager httpConnMgr = new SimpleHttpConnectionManager();
        HttpClient httpClient = new HttpClient(httpConnMgr);

        /*
        Credentials credentials = new UsernamePasswordCredentials(user, pwd);
        httpClient.getState().setCredentials(AuthScope.ANY, credentials);
        */

        org.apache.commons.httpclient.HttpMethod method = null;

        try
        {
            method = new GetMethod(urlStr);
            httpClient.executeMethod(method);
            
            // String response = method.getResponseBodyAsString();
            
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();   // Javadoc states closing has no effect
            byte[] byteArray = new byte[1024];
            int count = 0;

            while((count = method.getResponseBodyAsStream().read(byteArray, 0, byteArray.length)) > 0)
            {
                outputStream.write(byteArray, 0, count);
            }

            String response = new String(outputStream.toByteArray(), "UTF-8");
            
            return getDocTitleFromResponse(response);
            
            // DOM parsing would actually be better, but I don't have time to deal with malformed XML right now.

            /*
            final Pattern pattern = Pattern.compile("<head>(.+?)</head>");
            final Matcher matcher = pattern.matcher(response);
            matcher.find();
            String headPortion = matcher.group(1);
            */

            /*
            DOMParser parser = new DOMParser();
            parser.parse(new InputSource(new StringReader(response)));
            Document doc = parser.getDocument();
            Element docElement = doc.getDocumentElement();
            docElement.getElementsByTagName("head");
            //String message = doc.getDocumentElement().getTextContent();
            */

            /*
            Reader xml = new StringReader(response);

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document dom = db.parse(new InputSource(xml));
            Element docElement = dom.getDocumentElement();
            docElement.getElementsByTagName("head");
            */
            
        }
        catch(Exception e)
        {
			errorLogger.error("Error while attempting to get response from: {}.\nMessage: {}",
					urlStr, ExceptionUtils.getCauseMessage(e));
        	
            return "no response";
        }
        finally
        {
            if (method != null)
            {
                try
                {
                    method.releaseConnection();
                }
                catch(Exception e2)
                {

                }
            }
        }
    }
    
    
    private static String getDocTitleFromResponse(String response)
    {
    	int headStart = response.indexOf("<head");
    	int headFinish = response.indexOf("</head");
    	
    	if (headStart < 0 || headFinish < 0)
    	{
    		return "no doc title";
    	}
    	
    	String headArea = response.substring(headStart, headFinish);   // rough is fine

    	int titleStart = headArea.indexOf("<title>");
    	int titleFinish = headArea.indexOf("</title>");
    	
    	if (titleStart < 0 || titleFinish < 0)
    	{
    		return "no doc title";
    	}
    	
    	return headArea.substring(titleStart + "<title>".length(), titleFinish);
    }
	


}
