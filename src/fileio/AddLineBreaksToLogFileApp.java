package fileio;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class AddLineBreaksToLogFileApp
{
    public static void main(String[] args)
    {

        try
        {
            // Create new output file:

            FileOutputStream outStream = new FileOutputStream("c:/junk/Transformed/all-out-line-breaks.txt");
            Writer writer = new OutputStreamWriter(outStream);

            // Read records in input file:

            FileInputStream fileInputStream = new FileInputStream("c:/junk/Transformed/all-out.txt");
            DataInputStream dataInputStream = new DataInputStream(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));

            String oneLine;
            int counter = 0;
            String prevEntid = "";

            while ((oneLine = bufferedReader.readLine()) != null)
            {
                String entid = oneLine.substring(0, 53);

                boolean doLineBreak = counter > 0 && !entid.equals(prevEntid);
                counter++;

                if (doLineBreak)
                {
                    writer.write("\r\n");
                }

                writer.write(oneLine + "\r\n");

                prevEntid = entid;
            }

            bufferedReader.close();
            dataInputStream.close();
            fileInputStream.close();

            writer.close();

        }

        catch (IOException e)
        {
            e.printStackTrace();
        }

    }

}
