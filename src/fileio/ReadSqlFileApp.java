package fileio;

// http://stackoverflow.com/questions/1464291/how-to-really-read-text-file-from-classpath-in-java

/*
 *  This app reads an sql file out of the project's "resources" directory (a sibling to "src").  The
 *  "resources" directory must be on the classpath.
 */

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Scanner;


public class ReadSqlFileApp {

	
	public static void main(String[] args) {

		ReadSqlFileApp app = new ReadSqlFileApp();
		app.readSqlFile();
	}

	
	private void readSqlFile()
	{
        ClassLoader cl = ClassLoader.getSystemClassLoader();

        URL[] urls = ((URLClassLoader)cl).getURLs();
        
        System.out.println("Paths on classpath:\n");

        for(URL url: urls){
        	System.out.println(url.getFile());
        }

		InputStream is = getClass().getClassLoader().getResourceAsStream("Create tokens table.sql");
		String fileContents = getStringFromStreamWithClose(is);
		System.out.println("\nContents of sql file:\n");
		System.out.println(fileContents);
	}
	
	
	private String getStringFromStreamWithClose(InputStream is)
	{
		Scanner s = null;
		String result = null;

		try
		{
			s = new Scanner(is).useDelimiter("\\A");
			result = s.hasNext() ? s.next() : "";
			is.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (s != null) s.close();
		}
		
		return result == null ? "" : result;
	}
	
}
