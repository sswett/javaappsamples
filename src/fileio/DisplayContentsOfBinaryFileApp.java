package fileio;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;


/**
 * Can pass in argument "-unzip" to unzip the file
 */
public class DisplayContentsOfBinaryFileApp
{
    private static final int BUFFER_SIZE = 8192;


    public static void main(String[] args)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        File file = new File("C:\\junkblobs\\backup\\254306721720FA94-discussion-00000143D2C4D624.blob");

        try
        {
            copy(new FileInputStream(file), baos, BUFFER_SIZE);
            byte[] bytes = baos.toByteArray();

            if (args.length > 0 && args[0].equals("-unzip"))
            {
                bytes = unzipBytes(bytes);
            }

            String myString = new String(bytes);
            System.out.println(myString);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public static void copy(InputStream input,
                            OutputStream output,
                            int bufferSize)
            throws IOException
    {
        byte[] buf = new byte[bufferSize];
        int bytesRead = input.read(buf);

        while (bytesRead != -1)
        {
            output.write(buf, 0, bytesRead);
            bytesRead = input.read(buf);
        }

        output.flush();
    }


    public static byte[] unzipBytes(byte[] compressedData) throws IOException
    {
        final Inflater inf  = new Inflater();
        final ByteArrayInputStream bais = new ByteArrayInputStream(compressedData);
        final InflaterInputStream iis  = new InflaterInputStream(bais, inf);
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try
        {
            final byte[] buffer = new byte[BUFFER_SIZE];

            try
            {
                while (true)
                {
                    final int bytesRead = iis.read(buffer, 0, buffer.length);

                    if (bytesRead < 0)
                    {
                        // End of file.
                        break;
                    }

                    baos.write(buffer, 0, bytesRead);
                }
            }
            finally
            {
                closeWithCatch(iis);

                inf.end();  // This does not throw an IOException
            }

            return baos.toByteArray();
        }
        catch (IOException ioe)
        {
            throw ioe;
        }
        finally
        {
            // These ByteArray stream closes are not technically needed so they are together in the same
            // try block because it will not hurt if the second one is skipped.
            closeWithCatch(baos);
            closeWithCatch(bais);
        }
    }


    public static void closeWithCatch(OutputStream streamIn)
    {
        try
        {
            if (streamIn != null)
            {
                streamIn.close();
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }


    public static void closeWithCatch(InputStream streamIn)
    {
        try
        {
            if (streamIn != null)
            {
                streamIn.close();
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }


}
