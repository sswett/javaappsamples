package fileio;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class RepositionEntidInLogFileApp
{
    public static void main(String[] args)
    {

        String delim1 = "in parent folder ";
        String delim2 = "/db:";

        try
        {
            // Create new output file:

            FileOutputStream outStream = new FileOutputStream("c:/junk/Transformed/all-out.txt");
            Writer writer = new OutputStreamWriter(outStream);

            // Read records in input file:

            FileInputStream fileInputStream = new FileInputStream("c:/junk/Transformed/all.txt");
            DataInputStream dataInputStream = new DataInputStream(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));

            String oneLine;

            while ((oneLine = bufferedReader.readLine()) != null)
            {
                String entid = "";

                int posDelim1 = oneLine.indexOf(delim1);

                if (posDelim1 > 0)
                {
                    int posDelim2 = oneLine.indexOf(delim2, posDelim1);

                    if (posDelim2 > 0)
                    {
                        entid = oneLine.substring(posDelim1 + delim1.length(), posDelim2);
                    }
                }

                writer.write("http://www.server1.net/resources/u" + entid + " " + oneLine + "\r\n");
            }

            bufferedReader.close();
            dataInputStream.close();
            fileInputStream.close();

            writer.close();

        }

        catch (IOException e)
        {
            e.printStackTrace();
        }

    }

}
