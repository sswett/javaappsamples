package fileio;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ReadFileIntoByteArrayApp {

public static void main(String[] args)
{

    try {
        File myFile = new File("/junk/mytest.zip");
        FileInputStream fis = new FileInputStream (myFile);
        int fileLength = (int) myFile.length();
        byte[] myByteArray = new byte[fileLength];
        fis.read(myByteArray, 0, fileLength);
        fis.close();
    } catch (IOException e) {
        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
    }


    System.out.println("Here we go.");
}


}
