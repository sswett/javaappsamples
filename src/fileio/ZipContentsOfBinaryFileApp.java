package fileio;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;


/**
 * Can pass in argument "-unzip" to unzip the file
 */
public class ZipContentsOfBinaryFileApp
{
    private static final int BUFFER_SIZE = 8192;


    public static void main(String[] args)
    {
        File inputFile = new File("C:\\junkblobs\\tsi750\\vol24.blob\\vol24uncompressed-fixed.xml");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try
        {
            InputStream inputFileStream = new FileInputStream(inputFile);
            copy(inputFileStream, baos, BUFFER_SIZE);
            byte[] bytes = baos.toByteArray();
            closeWithCatch(inputFileStream);

            bytes = zipBytes(bytes);

            // Output to file:

            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            File outputFile = new File("C:\\junkblobs\\tsi750\\vol24.blob\\fixedzipped.blob");
            FileOutputStream outputFileStream = new FileOutputStream(outputFile);
            copy(bais, outputFileStream, BUFFER_SIZE);
            closeWithCatch(outputFileStream);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public static void copy(InputStream input,
                            OutputStream output,
                            int bufferSize)
            throws IOException
    {
        byte[] buf = new byte[bufferSize];
        int bytesRead = input.read(buf);

        while (bytesRead != -1)
        {
            output.write(buf, 0, bytesRead);
            bytesRead = input.read(buf);
        }

        output.flush();
    }




    public static byte[] zipBytes(byte[] uncompressedData) throws IOException
    {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try
        {
            final Deflater def = new Deflater(Deflater.BEST_SPEED);
            final DeflaterOutputStream dos = new DeflaterOutputStream(baos, def);

            try
            {
                dos.write(uncompressedData);
            }
            catch (IOException ioe)
            {
                throw ioe;
            }
            finally
            {
                closeWithCatch(dos);

                def.end();   // This does not throw an IOException
            }

            return baos.toByteArray();
        }
        finally
        {
            closeWithCatch(baos);
        }
    }


    public static void closeWithCatch(OutputStream streamIn)
    {
        try
        {
            if (streamIn != null)
            {
                streamIn.close();
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }


    public static void closeWithCatch(InputStream streamIn)
    {
        try
        {
            if (streamIn != null)
            {
                streamIn.close();
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }


}
