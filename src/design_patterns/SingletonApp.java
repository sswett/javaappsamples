package design_patterns;

public class SingletonApp {
	
    private static SingletonApp singleton;
    
    static
    {
        singleton = new SingletonApp();
    }

    
    private SingletonApp() 
    {
    }
     
    public static SingletonApp getInstance() 
    {
        return singleton;
    }
     
    
    public void testMe() 
    {
        System.out.println("Inside instance method \"testMe\" of singleton instance");
    }

    
    public static void main(String args[]) 
    {
    	SingletonApp theInstance = getInstance();
    	theInstance.testMe();
    }

}
