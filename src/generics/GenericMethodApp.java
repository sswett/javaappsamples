package generics;

public class GenericMethodApp {

	/*
	 * Notes for generic method below:
	 * 
	 * < E > = type parameter (aka type variable) section.  E is an identifier that specifies a generic type name.
	 * 
	 * The type parameter can also be used in the return type (instead of "void" below) and for actual type arguments ("inputArray")
	 * 
	 * When you see E vs. T, I guess it doesn't matter: http://stackoverflow.com/questions/6008241/java-generics-e-and-t-what-is-the-difference
	 * 
	 * If the code doesn't work, I believe you get a compile time error (which is improvement over runtime error).
	 */
	
	public static < E > void printArray( E[] inputArray )
	{
		// Display array elements              
		for ( E element : inputArray ){        
			System.out.printf( "%s ", element );
		}
		
		System.out.println();
	}
	

	public static void main( String args[] )
	{
		// Create arrays of Integer, Double and Character
		Integer[] intArray = { 1, 2, 3, 4, 5 };
		Double[] doubleArray = { 1.1, 2.2, 3.3, 4.4 };
		Character[] charArray = { 'H', 'E', 'L', 'L', 'O' };

		System.out.println( "Array integerArray contains:" );
		printArray( intArray  ); // pass an Integer array

		System.out.println( "\nArray doubleArray contains:" );
		printArray( doubleArray ); // pass a Double array

		System.out.println( "\nArray characterArray contains:" );
		printArray( charArray ); // pass a Character array
		
		// For example, the following gives a compile-time error:
		/*
		int[] integerArray = { 1, 2, 3, 4, 5 };
		printArray ( integerArray );
		*/
	} 
}
