package generics;

import inheritance.Animal;

public class GenericKeyValue<T extends Number, E extends Animal> 
{
	
    private T key;
    private E value;

    
    public GenericKeyValue (T k , E v)
    {
        setKey(k);
        setValue(v);
    }

    
    public void setKey(T key) {
        this.key = key;
    }

    
    public void setValue(E value) {
        this.value = value;
    }

    
    public T getKey() {
        return key;
    }

    
    public E getValue() {
        return value;
    }

}
