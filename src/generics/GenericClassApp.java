package generics;

import inheritance.Animal;
import inheritance.Cat;
import inheritance.Dog;

import java.util.ArrayList;
import java.util.Date;

public class GenericClassApp {

	public static void main(String[] args) {
		
		GenericKeyValue<Number, Animal> keyValue1 = new GenericKeyValue<Number, Animal>(1, new Cat());
		GenericKeyValue<Number, Animal> keyValue2 = new GenericKeyValue<Number, Animal>(2.5, new Dog());
		GenericKeyValue<Number, Animal> keyValue3 = new GenericKeyValue<Number, Animal>(new Date().getTime(), new Animal());
		
		ArrayList<GenericKeyValue<Number, Animal>> kvArrayList = new ArrayList<GenericKeyValue<Number, Animal>>();
		kvArrayList.add(keyValue1);
		kvArrayList.add(keyValue2);
		kvArrayList.add(keyValue3);
		
		for (GenericKeyValue<Number, Animal> keyValue : kvArrayList)
		{
			System.out.println(String.format("Key: %s, Type: %s, Sound: %s",
					keyValue.getKey(), keyValue.getValue().getClass(), keyValue.getValue().getSound()
					));
		}
	}

}
