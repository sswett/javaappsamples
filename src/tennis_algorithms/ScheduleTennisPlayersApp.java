package tennis_algorithms;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/*
    NOTE: this algorithm doesn't work very well.  See tried and true methods online (Wikipedia "round robin tournament")

    TODO: eventually code the round robin tournament format.
 */

public class ScheduleTennisPlayersApp
{

    private static final int noPlayers = 6;
    private static final int noDates = noPlayers - 1;
    private static final int OPPONENT_UNSET = -1;

    private static HashMap<Integer, Integer[]> schedulingData = new HashMap<Integer, Integer[]>();
    private static ArrayList<Integer> playersCompletelyScheduled = new ArrayList<Integer>();

    private static Random random = new Random();


    public static void main(String[] args)
    {
        initSchedulingData();

        /*
        for (int player = 0; player < noPlayers; player ++)
        {
        */


        for ( ; ; )
        {
            int player = getRandomPlayer();

            // If no more players, bail
            if (player == -1)
            {
                break;
            }


            for ( ; ; )
            {
                int unscheduledDate = getRandomUnscheduledDateForPlayer(player);
                boolean success = processUnscheduledPlayerAndDate(player, unscheduledDate);

                if (success)
                {
                    break;
                }
            }

            /*
            // THIS WORKS!!
            for (int d = 0; d < noDates; d++)
            {
                if (isDateUnscheduled(player, d))
                {
                    boolean success = processUnscheduledPlayerAndDate(player, d);

                    if (success)
                    {
                        // break;
                    }
                }
            }
            */
        }

        printSchedules();
    }


    /**
     *
     * @param player
     * @param date
     * @return true if success; false if schedule conflict
     */
    private static boolean processUnscheduledPlayerAndDate(int player, int date)
    {
        // Find most eligible opponent for date
        int mostEligibleOpponent = -1;
        int maxAvailableDatesForOpponent = 0;

        for (int opponent = 0; opponent < noPlayers; opponent++)
        {
            if (opponent == player)
            {
                continue;
            }

            if (isOpponentAlreadyOnPlayersSchedule(player, opponent))
            {
                System.out.println(String.format("Player %02d date %02d: opponent already on schedule: %02d",
                        player, date, opponent));

                continue;
            }

            if (isDateUnscheduled(opponent, date))
            {
                int availableDatesForOpponent = getNumberOfAvailableDatesForPlayer(opponent);

                if (availableDatesForOpponent > maxAvailableDatesForOpponent)
                {
                    maxAvailableDatesForOpponent = availableDatesForOpponent;
                    mostEligibleOpponent = opponent;
                }
            }
            else
            {
                System.out.println(String.format("Player %02d date %02d: opponent has a schedule conflict: %02d",
                        player, date, opponent));
            }
        }

        // Something's wrong
        if (mostEligibleOpponent == -1)
        {
            return false;   // let randomizer pick a different player and date; it will eventually schedule
            // printSchedules();
        }

        schedulePlayerAndDate(player, date, mostEligibleOpponent);
        schedulePlayerAndDate(mostEligibleOpponent, date, player);

        return true;
    }


    private static boolean isOpponentAlreadyOnPlayersSchedule(int player, int opponent)
    {
        Integer[] opponents = schedulingData.get(player);

        for (int o : opponents)
        {
            if (o == opponent)
            {
                return true;
            }
        }

        return false;
    }


    private static void schedulePlayerAndDate(int player, int date, int opponent)
    {
        Integer[] opponents = schedulingData.get(player);
        opponents[date] = opponent;
        // schedulingData.put(player, opponents);

        boolean anyDatesUnset = false;

        for (int o : opponents)
        {
            if (o == OPPONENT_UNSET)
            {
                anyDatesUnset = true;
                break;
            }
        }

        // If all opponents are scheduled, make note of it
        if (!anyDatesUnset)
        {
            playersCompletelyScheduled.add(player);
        }
    }


    private static boolean isDateUnscheduled(int player, int date)
    {
        return schedulingData.get(player)[date] == OPPONENT_UNSET;
    }


    private static int getNumberOfAvailableDatesForPlayer(int player)
    {
        int result = 0;

        for (int opponent : schedulingData.get(player))
        {
            if (opponent == OPPONENT_UNSET)
            {
                result++;
            }
        }

        return result;
    }


    private static void initSchedulingData()
    {
        for (int player = 0; player < noPlayers; player++)
        {
            Integer[] opponents = new Integer[noDates];

            for (int date = 0; date < noDates; date++)
            {
                opponents[date] = OPPONENT_UNSET;
            }

            schedulingData.put(player, opponents);
        }
    }


    private static int getRandomPlayer()
    {
        if (playersCompletelyScheduled.size() == noPlayers)
        {
            return -1;
        }

        for ( ; ; )
        {
            int player = random.nextInt(noPlayers);

            if (!playersCompletelyScheduled.contains(player))
            {
                return player;
            }
        }
    }


    private static int getRandomUnscheduledDateForPlayer(int player)
    {
        Integer[] opponents = schedulingData.get(player);

        for ( ; ; )
        {
            int date = random.nextInt(noDates);

            if (opponents[date] == OPPONENT_UNSET)
            {
                return date;
            }
        }
    }


    private static void printSchedules()
    {
        System.out.println();

        for (int player = 0; player < schedulingData.size(); player++)
        {
            System.out.print(String.format("Player %02d: ", player));

            Integer[] opponents = schedulingData.get(player);

            for (int d = 0; d < opponents.length; d++)
            {
                System.out.print(String.format("%02d ", opponents[d]));
            }

            System.out.println();
        }
    }



}