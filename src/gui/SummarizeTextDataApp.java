package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JFileChooser;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class SummarizeTextDataApp {

	private JFrame frmTestAppWindow;
	private final JButton inputChooseButton = new JButton("Choose");
	private final JPanel panel = new JPanel();
	private final JLabel lblInputFileName = new JLabel("Input file name:");
	private final JTextField inputFileName = new JTextField();
	
	private boolean chooserIsForInput = false;
	private final JPanel panel_1 = new JPanel();
	private final JButton outputChooseButton = new JButton("Choose");
	private final JLabel lblOutputFileName = new JLabel("Output file name:");
	private final JTextField outputFileName = new JTextField();
	private final JPanel panel_2 = new JPanel();
	private final JButton btnRun = new JButton("Run");
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					SummarizeTextDataApp window = new SummarizeTextDataApp();
					window.frmTestAppWindow.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SummarizeTextDataApp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		inputFileName.setBounds(137, 12, 440, 20);
		inputFileName.setColumns(10);
		frmTestAppWindow = new JFrame();
		frmTestAppWindow.setTitle("Summarize Text Data");
		frmTestAppWindow.setBounds(100, 100, 800, 664);
		frmTestAppWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTestAppWindow.getContentPane().setLayout(null);

		
		panel.setBounds(20, 11, 732, 50);
		
		frmTestAppWindow.getContentPane().add(panel);
		panel.setLayout(null);

		inputChooseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				OpenFileDialog dialog = new OpenFileDialog();
				dialog.setVisible(true);
				
				// The above dialog is modal.  This frame is blocked until the dialog is diposed.
				// At that time, execution resumes here.
				
				String selectedFileName = dialog.getSelectedFileName();
				
				if (selectedFileName.length() > 0)
				{
					inputFileName.setText(selectedFileName);
				}
			}
		});

		inputChooseButton.setBounds(629, 11, 93, 23);
		panel.add(inputChooseButton);
		lblInputFileName.setBounds(10, 11, 96, 14);
		
		panel.add(lblInputFileName);
		
		panel.add(inputFileName);
		panel_1.setLayout(null);
		panel_1.setBounds(20, 72, 732, 50);
		
		frmTestAppWindow.getContentPane().add(panel_1);

		outputChooseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				OpenFileDialog dialog = new OpenFileDialog();
				dialog.setVisible(true);
				
				// The above dialog is modal.  This frame is blocked until the dialog is diposed.
				// At that time, execution resumes here.
				
				String selectedFileName = dialog.getSelectedFileName();
				
				if (selectedFileName.length() > 0)
				{
					outputFileName.setText(selectedFileName);
				}
			}
		});

		outputChooseButton.setBounds(629, 7, 93, 23);
		
		panel_1.add(outputChooseButton);
		lblOutputFileName.setBounds(10, 11, 96, 14);
		
		panel_1.add(lblOutputFileName);
		outputFileName.setColumns(10);
		outputFileName.setBounds(137, 10, 440, 20);
		
		panel_1.add(outputFileName);
		panel_2.setBounds(20, 133, 732, 50);
		
		frmTestAppWindow.getContentPane().add(panel_2);

		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				boolean hasError = false;
						
				String filename = inputFileName.getText().trim();
				
				if (filename.length() == 0)
				{
					JOptionPane.showMessageDialog(frmTestAppWindow.getContentPane(), "The input file name is required.", "Input Error", JOptionPane.ERROR_MESSAGE);
					hasError = true;
				}
				else if ( !doesFileExist(filename) )
				{
					JOptionPane.showMessageDialog(frmTestAppWindow.getContentPane(), "The input file name does not exist.", "Input Error", JOptionPane.ERROR_MESSAGE);
					hasError = true;
				}

				filename = outputFileName.getText().trim();
				
				if (filename.length() == 0)
				{
					JOptionPane.showMessageDialog(frmTestAppWindow.getContentPane(), "The output file name is required.", "Input Error", JOptionPane.ERROR_MESSAGE);
					hasError = true;
				}
				else if ( !doesDirectoryForFileExist(filename) )
				{
					JOptionPane.showMessageDialog(frmTestAppWindow.getContentPane(), "The output file directory does not exist.", "Input Error", JOptionPane.ERROR_MESSAGE);
					hasError = true;
				}

				if (!hasError)
				{
					// This is where an actual "run" could take place:
					
					frmTestAppWindow.dispose();
				}
			}
		});
		
		panel_2.add(btnRun);
	}
	
	
	private boolean doesFileExist(String filePath)
	{
		File file = new File(filePath);
		return file.isFile();
	}
	
	
	private boolean doesDirectoryForFileExist(String filePath)
	{
		int lastSlash = filePath.lastIndexOf("\\");
		
		if (lastSlash == -1 )
		{
			return false;
		}
		
		String directoryPath = filePath.substring(0,  lastSlash);
		
		File directory = new File(directoryPath);
		return directory.isDirectory();
	}
}
