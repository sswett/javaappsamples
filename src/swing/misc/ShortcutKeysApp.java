package swing.misc;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class ShortcutKeysApp
{


	public ShortcutKeysApp ()
	{
		JFrame frame = new JFrame("Test Shortcut Keys");
		Container content = frame.getContentPane();
		content.setLayout(new FlowLayout());
		frame.setSize(300,300);

		// Menu setup
		JMenuBar jmb = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem quitItem = new JMenuItem("Quit");
		jmb.add(fileMenu);
		fileMenu.add(quitItem);
		frame.setJMenuBar(jmb);

		quitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae)
			{
				System.exit(0);
			}
		});

		fileMenu.setMnemonic(KeyEvent.VK_F);
		quitItem.setMnemonic(KeyEvent.VK_Q);

		// test field setup
		final JTextField testText = new JTextField("",15);
		content.add(testText);

		// cancel button setup
		JButton button = new JButton("Cancel");
		button.setSize(30,30);
		button.setToolTipText("Click here to cancel the operation");
		content.add(button);

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae)
			{
				testText.setText("Cancel button clicked");
			}
		});

		button.setMnemonic(KeyEvent.VK_C);

		// display the frame
		frame.setVisible(true);
	}


	public static void main(String[] args)
	{
		new ShortcutKeysApp();
	}

}

