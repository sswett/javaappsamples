package swing.course_selections_demo;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * Title:        Course Selections Demo
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      Best Computer Systems, Inc.
 * @author Steve Swett
 * @version 1.0
 */

public class SignInFrame extends JFrame {
	JPanel SignInPanel = new JPanel();
	JLabel jLabel1 = new JLabel();
	JLabel jLabel2 = new JLabel();
	JLabel jLabel3 = new JLabel();
	JPasswordField StudentIDPasswordField = new JPasswordField();
	JPasswordField BirthDatePasswordField = new JPasswordField();
	JLabel jLabel4 = new JLabel();
	JButton OKButton = new JButton();
	JButton ExitButton = new JButton();
	boolean OKPressed = false;
	boolean ExitPressed = false;

	public SignInFrame() {
		try {
			jbInit();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	private void jbInit() throws Exception {
		this.getContentPane().setBackground(new Color(192, 206, 192));
		this.setTitle("Sign-In: Course Selections Demo");
		this.getContentPane().setLayout(null);
		SignInPanel.setBackground(new Color(192, 206, 192));
		SignInPanel.setFont(new java.awt.Font("SansSerif", 0, 11));
		SignInPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		SignInPanel.setBounds(new Rectangle(8, 8, 402, 267));
		SignInPanel.setLayout(null);
		jLabel1.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel1.setText("Sign-in Screen");
		jLabel1.setBounds(new Rectangle(23, 25, 141, 26));
		jLabel2.setBounds(new Rectangle(23, 69, 149, 26));
		jLabel2.setText("CIMS Student ID Number:");
		jLabel2.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel3.setBounds(new Rectangle(23, 107, 141, 26));
		jLabel3.setText("Birth date MMDDYY:");
		jLabel3.setFont(new java.awt.Font("SansSerif", 0, 12));
		StudentIDPasswordField.setBounds(new Rectangle(182, 72, 121, 21));
		BirthDatePasswordField.setBounds(new Rectangle(182, 110, 121, 21));
		jLabel4.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel4.setText("Example: 091486 for Sept. 14, 1986");
		jLabel4.setBounds(new Rectangle(182, 147, 213, 26));
		OKButton.setBackground(new Color(192, 192, 215));
		OKButton.setText("OK");
		OKButton.setBounds(new Rectangle(107, 188, 75, 25));
		OKButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OKButton_actionPerformed(e);
			}
		});
		ExitButton.setBounds(new Rectangle(228, 188, 75, 25));
		ExitButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ExitButton_actionPerformed(e);
			}
		});
		ExitButton.setText("Exit");
		ExitButton.setBackground(new Color(192, 192, 215));
		this.getContentPane().add(SignInPanel, null);
		SignInPanel.add(jLabel1, null);
		SignInPanel.add(jLabel2, null);
		SignInPanel.add(StudentIDPasswordField, null);
		SignInPanel.add(jLabel3, null);
		SignInPanel.add(BirthDatePasswordField, null);
		SignInPanel.add(jLabel4, null);
		SignInPanel.add(ExitButton, null);
		SignInPanel.add(OKButton, null);
	}

	void ExitButton_actionPerformed(ActionEvent e) {
		ExitPressed = true;
		this.hide();
	}

	void OKButton_actionPerformed(ActionEvent e) {
		OKPressed = true;
		this.hide();

	}
}