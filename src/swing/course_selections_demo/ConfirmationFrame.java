package swing.course_selections_demo;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * Title:        Course Selections Demo
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      Best Computer Systems, Inc.
 * @author Steve Swett
 * @version 1.0
 */

public class ConfirmationFrame extends JFrame {
	JPanel ConfirmationPanel = new JPanel();
	JLabel jLabel1 = new JLabel();
	JLabel StudentNameLabel = new JLabel();
	JLabel jLabel2 = new JLabel();
	JLabel jLabel3 = new JLabel();
	JLabel CurrentYearDistrictLabel = new JLabel();
	JLabel CurrentYearDistrictNameLabel = new JLabel();
	JLabel jLabel4 = new JLabel();
	JLabel CurrentYearSchoolLabel = new JLabel();
	JLabel CurrentYearSchoolNameLabel = new JLabel();
	JLabel jLabel5 = new JLabel();
	JLabel CurrentYearClassLabel = new JLabel();
	JLabel jLabel6 = new JLabel();
	JLabel CurrentYearCounselorNameLabel = new JLabel();
	JPanel NextYearPanel = new JPanel();
	JLabel NextYearClassLabel = new JLabel();
	JLabel NextYearDistrictNameLabel = new JLabel();
	JLabel NextYearDistrictLabel = new JLabel();
	JLabel NextYearSchoolLabel = new JLabel();
	JLabel NextYearSchoolNameLabel = new JLabel();
	JLabel NextYearCounselorNameLabel = new JLabel();
	JLabel jLabel7 = new JLabel();
	JLabel jLabel8 = new JLabel();
	JLabel jLabel9 = new JLabel();
	JLabel jLabel10 = new JLabel();
	JLabel jLabel11 = new JLabel();
	JLabel jLabel12 = new JLabel();
	JComboBox AcademyComboBox = new JComboBox();
	JButton OKContinueButton = new JButton();
	JButton CancelButton = new JButton();
	boolean OKPressed = false;
	boolean CancelPressed = false;
	JLabel jLabel13 = new JLabel();


	public ConfirmationFrame() {
		try {
			jbInit();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	private void jbInit() throws Exception {
		this.getContentPane().setBackground(new Color(192, 206, 192));
		this.setTitle("Name Confirmation: Course Selections Demo");
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowActivated(WindowEvent e) {
				this_windowActivated(e);
			}
			public void windowClosed(WindowEvent e) {
				this_windowClosed(e);
			}
			public void windowClosing(WindowEvent e) {
				this_windowClosing(e);
			}
			public void windowDeactivated(WindowEvent e) {
				this_windowDeactivated(e);
			}
			public void windowDeiconified(WindowEvent e) {
				this_windowDeiconified(e);
			}
			public void windowIconified(WindowEvent e) {
				this_windowIconified(e);
			}
			public void windowOpened(WindowEvent e) {
				this_windowOpened(e);
			}
		});
		this.getContentPane().setLayout(null);
		ConfirmationPanel.setBackground(new Color(192, 206, 192));
		ConfirmationPanel.setFont(new java.awt.Font("SansSerif", 0, 12));
		ConfirmationPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		ConfirmationPanel.setBounds(new Rectangle(9, 9, 554, 437));
		ConfirmationPanel.setLayout(null);
		jLabel1.setFont(new java.awt.Font("SansSerif", 1, 12));
		jLabel1.setText("Student Name:");
		jLabel1.setBounds(new Rectangle(30, 27, 91, 19));
		StudentNameLabel.setBounds(new Rectangle(142, 27, 417, 19));
		StudentNameLabel.setText("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		StudentNameLabel.setFont(new java.awt.Font("SansSerif", 1, 12));
		jLabel2.setBounds(new Rectangle(30, 63, 131, 19));
		jLabel2.setText("Currently assigned to");
		jLabel2.setFont(new java.awt.Font("SansSerif", 1, 12));
		jLabel3.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel3.setText("District:");
		jLabel3.setBounds(new Rectangle(30, 81, 47, 19));
		CurrentYearDistrictLabel.setBounds(new Rectangle(106, 81, 32, 19));
		CurrentYearDistrictLabel.setText("XXX");
		CurrentYearDistrictLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		CurrentYearDistrictNameLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		CurrentYearDistrictNameLabel.setText("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		CurrentYearDistrictNameLabel.setBounds(new Rectangle(142, 81, 305, 19));
		jLabel4.setBounds(new Rectangle(30, 100, 47, 19));
		jLabel4.setText("School:");
		jLabel4.setFont(new java.awt.Font("SansSerif", 0, 12));
		CurrentYearSchoolLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		CurrentYearSchoolLabel.setText("XXX");
		CurrentYearSchoolLabel.setBounds(new Rectangle(106, 100, 32, 19));
		CurrentYearSchoolNameLabel.setBounds(new Rectangle(142, 100, 305, 19));
		CurrentYearSchoolNameLabel.setText("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		CurrentYearSchoolNameLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel5.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel5.setText("Class:");
		jLabel5.setBounds(new Rectangle(30, 118, 47, 19));
		CurrentYearClassLabel.setBounds(new Rectangle(106, 118, 25, 19));
		CurrentYearClassLabel.setText("XX");
		CurrentYearClassLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel6.setBounds(new Rectangle(30, 137, 69, 19));
		jLabel6.setText("Counselor:");
		jLabel6.setFont(new java.awt.Font("SansSerif", 0, 12));
		CurrentYearCounselorNameLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		CurrentYearCounselorNameLabel.setText("XXXXXXXXXXXXXXX");
		CurrentYearCounselorNameLabel.setBounds(new Rectangle(142, 137, 123, 19));
		NextYearPanel.setBackground(new Color(192, 206, 192));
		NextYearPanel.setBounds(new Rectangle(18, 157, 475, 111));
		NextYearPanel.setLayout(null);
		NextYearClassLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		NextYearClassLabel.setText("XX");
		NextYearClassLabel.setBounds(new Rectangle(88, 67, 25, 19));
		NextYearDistrictNameLabel.setBounds(new Rectangle(124, 30, 305, 19));
		NextYearDistrictNameLabel.setText("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		NextYearDistrictNameLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		NextYearDistrictLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		NextYearDistrictLabel.setText("XXX");
		NextYearDistrictLabel.setBounds(new Rectangle(88, 30, 32, 19));
		NextYearSchoolLabel.setBounds(new Rectangle(88, 49, 32, 19));
		NextYearSchoolLabel.setText("XXX");
		NextYearSchoolLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		NextYearSchoolNameLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		NextYearSchoolNameLabel.setText("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		NextYearSchoolNameLabel.setBounds(new Rectangle(124, 49, 305, 19));
		NextYearCounselorNameLabel.setBounds(new Rectangle(124, 86, 123, 19));
		NextYearCounselorNameLabel.setText("XXXXXXXXXXXXXXX");
		NextYearCounselorNameLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel7.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel7.setText("Counselor:");
		jLabel7.setBounds(new Rectangle(12, 86, 69, 19));
		jLabel8.setBounds(new Rectangle(12, 67, 47, 19));
		jLabel8.setText("Class:");
		jLabel8.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel9.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel9.setText("School:");
		jLabel9.setBounds(new Rectangle(12, 49, 47, 19));
		jLabel10.setBounds(new Rectangle(12, 30, 47, 19));
		jLabel10.setText("District:");
		jLabel10.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel11.setFont(new java.awt.Font("SansSerif", 1, 12));
		jLabel11.setText("Next year placement");
		jLabel11.setBounds(new Rectangle(12, 12, 131, 19));
		jLabel12.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel12.setText("Academy:");
		jLabel12.setBounds(new Rectangle(30, 288, 69, 19));
		AcademyComboBox.setBounds(new Rectangle(142, 287, 168, 21));
		OKContinueButton.setBackground(new Color(192, 192, 215));
		OKContinueButton.setFont(new java.awt.Font("SansSerif", 0, 12));
		OKContinueButton.setText("OK / Continue");
		OKContinueButton.setBounds(new Rectangle(141, 334, 113, 28));
		OKContinueButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OKContinueButton_actionPerformed(e);
			}
		});
		CancelButton.setBounds(new Rectangle(309, 334, 113, 28));
		CancelButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CancelButton_actionPerformed(e);
			}
		});
		CancelButton.setText("Cancel");
		CancelButton.setBackground(new Color(192, 192, 215));
		CancelButton.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel13.setBounds(new Rectangle(69, 388, 417, 19));
		jLabel13.setText("If the correct student is shown, choose an academy and then click " +
				"OK.");
		jLabel13.setFont(new java.awt.Font("SansSerif", 1, 12));
		this.getContentPane().add(ConfirmationPanel, null);
		ConfirmationPanel.add(jLabel2, null);
		ConfirmationPanel.add(jLabel1, null);
		ConfirmationPanel.add(CurrentYearDistrictNameLabel, null);
		ConfirmationPanel.add(CurrentYearSchoolNameLabel, null);
		ConfirmationPanel.add(CurrentYearDistrictLabel, null);
		ConfirmationPanel.add(CurrentYearSchoolLabel, null);
		ConfirmationPanel.add(jLabel3, null);
		ConfirmationPanel.add(jLabel4, null);
		ConfirmationPanel.add(jLabel5, null);
		ConfirmationPanel.add(jLabel6, null);
		ConfirmationPanel.add(CurrentYearClassLabel, null);
		ConfirmationPanel.add(CurrentYearCounselorNameLabel, null);
		ConfirmationPanel.add(NextYearPanel, null);
		NextYearPanel.add(jLabel11, null);
		NextYearPanel.add(jLabel10, null);
		NextYearPanel.add(NextYearDistrictLabel, null);
		NextYearPanel.add(NextYearDistrictNameLabel, null);
		NextYearPanel.add(jLabel9, null);
		NextYearPanel.add(NextYearSchoolLabel, null);
		NextYearPanel.add(jLabel8, null);
		NextYearPanel.add(NextYearClassLabel, null);
		NextYearPanel.add(jLabel7, null);
		NextYearPanel.add(NextYearCounselorNameLabel, null);
		NextYearPanel.add(NextYearSchoolNameLabel, null);
		ConfirmationPanel.add(jLabel12, null);
		ConfirmationPanel.add(AcademyComboBox, null);
		ConfirmationPanel.add(OKContinueButton, null);
		ConfirmationPanel.add(jLabel13, null);
		ConfirmationPanel.add(StudentNameLabel, null);
		ConfirmationPanel.add(CancelButton, null);
	}

	void OKContinueButton_actionPerformed(ActionEvent e) {
		OKPressed = true;
		this.hide();
	}

	void CancelButton_actionPerformed(ActionEvent e) {
		CancelPressed = true;
		this.hide();
	}

	void this_windowActivated(WindowEvent e) {
		OKPressed = OKPressed;
	}

	void this_windowClosed(WindowEvent e) {
		OKPressed = OKPressed;

	}

	void this_windowClosing(WindowEvent e) {
		OKPressed = OKPressed;

		// Programatically click the "Cancel" button:
		/*      CancelButton_actionPerformed(
        new ActionEvent(CancelButton,
          java.awt.event.ActionEvent.ACTION_PERFORMED,
          null));
		 */
	}

	void this_windowDeactivated(WindowEvent e) {
		OKPressed = OKPressed;

	}

	void this_windowDeiconified(WindowEvent e) {
		OKPressed = OKPressed;

	}

	void this_windowIconified(WindowEvent e) {
		OKPressed = OKPressed;

	}

	void this_windowOpened(WindowEvent e) {
		OKPressed = OKPressed;

	}

}