package swing.course_selections_demo;

/*  CourseSelectionsDemo - Written 12/24/02 - 12/27/02 by S Swett - JDK 1.3.0c
demo of web-based student course selections.


Reminder of how to construct a table:

To define a table ...

1.  Define a table model class by extending AbstractTableModel and implementing
these methods: setResultSet, getColumnCount, getRowCount, getValueAt, and
getColumnName.  If the table is editable and updateable, the following
methods must also be implemented: isCellEditable, getColumnClass, and
setValueAt.  Example: CourseTableModel

2.  Define a table model object based on the table model class.  Example: MyCourseTableModel

3.  Define a table based on the table model object.  Example: CourseTable

To load/refresh the contents of a table ...

1.  Call the model's setResultSet method.  Example: MyCourseTableModel.setResultSet();

2.  Set/reset table column widths.  Example: SetCourseTableColumnWidths();

 */


import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.util.*;
import javax.swing.event.*;
import java.awt.print.*;
import java.awt.geom.*;
import java.awt.Dimension;
import java.net.*;


public class CourseSelectionsDemo extends JApplet implements Printable{
	boolean isStandalone = false;

	SignInFrame mySignInFrame = new SignInFrame();
	ConfirmationFrame myConfirmationFrame = new ConfirmationFrame();

	String GlobalStudentID = "";
	String GlobalBirthDate = "";
	boolean GlobalSignInOKPressed;
	boolean GlobalSignInExitPressed;
	String GlobalAcademy = "";
	boolean GlobalConfirmationOKPressed;
	boolean GlobalConfirmationCancelPressed;
	String GlobalCurrentCourse = "";   // Course# in selected row of SelectedCoursesTable

	boolean InitiallyLoading = true;

	Vector MasterCourseList;
	Vector BriefMasterCourseList;
	Vector LinkCoursesList;
	Vector DependentCoursesList;
	Vector GroupDeptList;
	Vector CoreLevelsList;
	Vector SelectedCoursesList;
	Vector SelectedCoursesAltList;
	Vector SequencedSelectionsList;
	Vector SplitLinksList;

	boolean IsTeamDept;

	CourseTableModel MyCourseTableModel = new CourseTableModel();
	JTable CourseTable = new JTable(MyCourseTableModel);
	JComboBox SelectorComboBox = new JComboBox();

	SelectedCoursesTableModel MySelectedCoursesTableModel = new SelectedCoursesTableModel();
	JTable SelectedCoursesTable = new JTable(MySelectedCoursesTableModel);

	CreditsTableModel MyCreditsTableModel = new CreditsTableModel();
	JTable CreditsTable = new JTable(MyCreditsTableModel);

	JTabbedPane MainTabbedPane = new JTabbedPane();
	JPanel SelectionsPanel = new JPanel();
	JPanel CreditsPanel = new JPanel();
	JPanel HeaderPanel = new JPanel();
	JLabel jLabel1 = new JLabel();
	JLabel StudentNameLabel = new JLabel();
	JLabel jLabel2 = new JLabel();
	JLabel CurrentYearDistrictLabel = new JLabel();
	JLabel jLabel3 = new JLabel();
	JLabel CurrentYearSchoolLabel = new JLabel();
	JLabel jLabel4 = new JLabel();
	JLabel CurrentYearClassLabel = new JLabel();
	JLabel jLabel5 = new JLabel();
	JLabel CurrentYearCounselorNameLabel = new JLabel();
	JLabel jLabel6 = new JLabel();
	JLabel NextYearDistrictLabel = new JLabel();
	JLabel jLabel7 = new JLabel();
	JLabel NextYearSchoolLabel = new JLabel();
	JLabel jLabel8 = new JLabel();
	JLabel NextYearClassLabel = new JLabel();
	JLabel jLabel9 = new JLabel();
	JLabel NextYearCounselorNameLabel = new JLabel();
	JLabel jLabel10 = new JLabel();
	JLabel AcademyLabel = new JLabel();
	JPanel SemesterNotePanel = new JPanel();
	JLabel jLabel11 = new JLabel();
	JLabel jLabel12 = new JLabel();
	JPanel SelectionsTopPanel = new JPanel();
	JScrollPane CourseTableScrollPane = new JScrollPane();
	JComboBox DeptComboBox = new JComboBox();
	JLabel jLabel13 = new JLabel();
	JButton PreviousButton = new JButton();
	JButton FirstButton = new JButton();
	JButton NextButton = new JButton();
	JButton LastButton = new JButton();
	JPanel LimitToGroupPanel = new JPanel();
	TitledBorder titledBorder1;
	JComboBox EngComboBox = new JComboBox();
	JComboBox MathComboBox = new JComboBox();
	JComboBox SocComboBox = new JComboBox();
	JComboBox SciComboBox = new JComboBox();
	JLabel jLabel14 = new JLabel();
	JLabel jLabel15 = new JLabel();
	JLabel jLabel16 = new JLabel();
	JLabel jLabel17 = new JLabel();
	JLabel jLabel18 = new JLabel();
	JPanel SelectionsBottomPanel = new JPanel();
	JScrollPane SelectedCoursesTableScrollPane = new JScrollPane();
	JPanel jPanel1 = new JPanel();
	JLabel jLabel19 = new JLabel();
	JPanel SelectionsBottomRightPanel = new JPanel();
	JButton SaveAndExitButton = new JButton();
	JButton CancelButton = new JButton();
	JButton PrintButton = new JButton();
	JLabel jLabel20 = new JLabel();
	JLabel jLabel21 = new JLabel();
	JLabel jLabel22 = new JLabel();
	JLabel TotalSelectionsCountLabel = new JLabel();
	JLabel TotalSelectionsAltCountLabel = new JLabel();
	JLabel TotalTranscriptCreditsLabel = new JLabel();
	JScrollPane CreditsTableScrollPane = new JScrollPane();
	JPanel CreditsBottomPanel = new JPanel();
	JLabel jLabel23 = new JLabel();
	JLabel TotalCreditsLabel = new JLabel();
	JButton MoveUpButton = new JButton();
	JButton MoveDownButton = new JButton();
	/**Get a parameter value*/
	public String getParameter(String key, String def) {
		return isStandalone ? System.getProperty(key, def) :
			(getParameter(key) != null ? getParameter(key) : def);
	}

	/**Construct the applet*/
	public CourseSelectionsDemo() {
	}
	/**Initialize the applet*/
	public void init() {
		try {
			jbInit();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**Component initialization*/
	private void jbInit() throws Exception {

		mySignInFrame.setBounds(210,135,426,310);
		mySignInFrame.setLocation(210,135);

		myConfirmationFrame.setBounds(100,80,578,489);
		myConfirmationFrame.setLocation(100,80);

		SelectorComboBox.setFont(new java.awt.Font("SansSerif", 0, 11));


		// Load master course list:
		MasterCourseList = new Vector();
		MasterCourseList.clear();

		// MasterCourseString format: DDDCCCCCCTTTTTTTTTTTTCC.CCC
		// where:
		// DDD = Dept/Group - 1,3
		// CCCCCC = Course# - 4,6
		// TTTTTTTTTTTT = Course title - 10,12
		// CC.CCC = Transcript credit, example: 00.500 - 22,6

		// Dept 1. -- Math courses
		MasterCourseList.addElement("001410   General Math00.500");
		MasterCourseList.addElement("001420   Algebra I   00.000");
		MasterCourseList.addElement("001430   Algebra II  00.500");
		MasterCourseList.addElement("001435   Geometry    00.500");
		MasterCourseList.addElement("001440   Trigonometry00.500");
		MasterCourseList.addElement("001450   Calculus    00.500");
		MasterCourseList.addElement("001460   Adv Calculus00.500");
		MasterCourseList.addElement("001470   Statistics  00.500");

		// Dept 2. -- English courses
		MasterCourseList.addElement("002310   English 9   00.500");
		MasterCourseList.addElement("002320   Literature  00.500");
		MasterCourseList.addElement("002330   Bus Writing 00.500");
		MasterCourseList.addElement("002340   Creat Writ. 00.500");
		MasterCourseList.addElement("002350   Speech      00.500");
		MasterCourseList.addElement("002360   Composition 00.500");

		// Dept 3. -- Science courses
		MasterCourseList.addElement("003510   Earth Sci   00.500");
		MasterCourseList.addElement("003520   Biology     00.500");
		MasterCourseList.addElement("003530   Chemistry   00.500");
		MasterCourseList.addElement("003540   Phys Science00.500");
		MasterCourseList.addElement("003550   Anatomy     00.500");
		MasterCourseList.addElement("003560   Gen Science 00.500");

		// Dept 4. -- History courses
		MasterCourseList.addElement("004210   U.S. History00.500");
		MasterCourseList.addElement("004220   World Hist  00.500");
		MasterCourseList.addElement("004230   Ancient Rome00.500");
		MasterCourseList.addElement("004240   Early AmHist00.500");
		MasterCourseList.addElement("004250   Anc. Greece 00.500");
		MasterCourseList.addElement("004260   Hist & Myth 00.500");

		// Dept 5. -- Elective courses
		MasterCourseList.addElement("005110   Art I       00.250");
		MasterCourseList.addElement("005120   Art II      00.250");
		MasterCourseList.addElement("005130   Painting    00.250");
		MasterCourseList.addElement("005140   Photography 00.250");
		MasterCourseList.addElement("005150   Pottery     00.250");
		MasterCourseList.addElement("005160   Woodworking 00.250");
		MasterCourseList.addElement("005170   Welding     00.250");
		MasterCourseList.addElement("005180   Mechanics   00.250");

		// Dept 6. -- Team courses
		MasterCourseList.addElement("006T01   ER MR SOR SR00.000");
		MasterCourseList.addElement("006T02   ER MR SOR SP00.000");
		MasterCourseList.addElement("006T03   ER MR SOP SR00.000");
		MasterCourseList.addElement("006T04   ER MR SOP SP00.000");
		MasterCourseList.addElement("006T05   ER MP SOR SR00.000");
		MasterCourseList.addElement("006T06   ER MP SOR SP00.000");
		MasterCourseList.addElement("006T07   ER MP SOP SR00.000");
		MasterCourseList.addElement("006T08   ER MP SOP SP00.000");
		MasterCourseList.addElement("006T09   EP MR SOR SR00.000");
		MasterCourseList.addElement("006T10   EP MR SOR SP00.000");
		MasterCourseList.addElement("006T11   EP MR SOP SR00.000");
		MasterCourseList.addElement("006T12   EP MR SOP SP00.000");
		MasterCourseList.addElement("006T13   EP MP SOR SR00.000");
		MasterCourseList.addElement("006T14   EP MP SOR SP00.000");
		MasterCourseList.addElement("006T15   EP MP SOP SR00.000");
		MasterCourseList.addElement("006T16   EP MP SOP SP00.000");


		// Load brief master course list:
		BriefMasterCourseList = new Vector();
		BriefMasterCourseList.clear();

		// Load in same order as MasterCourseList:

		// Dept 1. -- Math courses
		BriefMasterCourseList.addElement("410   ");
		BriefMasterCourseList.addElement("420   ");
		BriefMasterCourseList.addElement("430   ");
		BriefMasterCourseList.addElement("435   ");
		BriefMasterCourseList.addElement("440   ");
		BriefMasterCourseList.addElement("450   ");
		BriefMasterCourseList.addElement("460   ");
		BriefMasterCourseList.addElement("470   ");

		// Dept 2. -- English courses
		BriefMasterCourseList.addElement("310   ");
		BriefMasterCourseList.addElement("320   ");
		BriefMasterCourseList.addElement("330   ");
		BriefMasterCourseList.addElement("340   ");
		BriefMasterCourseList.addElement("350   ");
		BriefMasterCourseList.addElement("360   ");

		// Dept 3. -- Science courses
		BriefMasterCourseList.addElement("510   ");
		BriefMasterCourseList.addElement("520   ");
		BriefMasterCourseList.addElement("530   ");
		BriefMasterCourseList.addElement("540   ");
		BriefMasterCourseList.addElement("550   ");
		BriefMasterCourseList.addElement("560   ");

		// Dept 4. -- History courses
		BriefMasterCourseList.addElement("210   ");
		BriefMasterCourseList.addElement("220   ");
		BriefMasterCourseList.addElement("230   ");
		BriefMasterCourseList.addElement("240   ");
		BriefMasterCourseList.addElement("250   ");
		BriefMasterCourseList.addElement("260   ");

		// Dept 5. -- Elective courses
		BriefMasterCourseList.addElement("110   ");
		BriefMasterCourseList.addElement("120   ");
		BriefMasterCourseList.addElement("130   ");
		BriefMasterCourseList.addElement("140   ");
		BriefMasterCourseList.addElement("150   ");
		BriefMasterCourseList.addElement("160   ");
		BriefMasterCourseList.addElement("170   ");
		BriefMasterCourseList.addElement("180   ");

		// Dept 6. -- Team courses
		BriefMasterCourseList.addElement("T01   ");
		BriefMasterCourseList.addElement("T02   ");
		BriefMasterCourseList.addElement("T03   ");
		BriefMasterCourseList.addElement("T04   ");
		BriefMasterCourseList.addElement("T05   ");
		BriefMasterCourseList.addElement("T06   ");
		BriefMasterCourseList.addElement("T07   ");
		BriefMasterCourseList.addElement("T08   ");
		BriefMasterCourseList.addElement("T09   ");
		BriefMasterCourseList.addElement("T10   ");
		BriefMasterCourseList.addElement("T11   ");
		BriefMasterCourseList.addElement("T12   ");
		BriefMasterCourseList.addElement("T13   ");
		BriefMasterCourseList.addElement("T14   ");
		BriefMasterCourseList.addElement("T15   ");
		BriefMasterCourseList.addElement("T16   ");

		// Load link course list:
		LinkCoursesList = new Vector();
		LinkCoursesList.clear();

		LinkCoursesList.addElement("T01   ");
		LinkCoursesList.addElement("T02   ");
		LinkCoursesList.addElement("T03   ");
		LinkCoursesList.addElement("T04   ");
		LinkCoursesList.addElement("T05   ");
		LinkCoursesList.addElement("T06   ");
		LinkCoursesList.addElement("T07   ");
		LinkCoursesList.addElement("T08   ");
		LinkCoursesList.addElement("T09   ");
		LinkCoursesList.addElement("T10   ");
		LinkCoursesList.addElement("T11   ");
		LinkCoursesList.addElement("T12   ");
		LinkCoursesList.addElement("T13   ");
		LinkCoursesList.addElement("T14   ");
		LinkCoursesList.addElement("T15   ");
		LinkCoursesList.addElement("T16   ");
		LinkCoursesList.addElement("410   ");
		LinkCoursesList.addElement("420   ");
		LinkCoursesList.addElement("430   ");
		LinkCoursesList.addElement("310   ");
		LinkCoursesList.addElement("320   ");
		LinkCoursesList.addElement("330   ");
		LinkCoursesList.addElement("510   ");
		LinkCoursesList.addElement("520   ");
		LinkCoursesList.addElement("530   ");
		LinkCoursesList.addElement("210   ");
		LinkCoursesList.addElement("220   ");
		LinkCoursesList.addElement("230   ");


		// Load dependent courses list:
		DependentCoursesList = new Vector();
		DependentCoursesList.clear();

		// DependentCoursesList format: LLLLLLCCCCCCTTTTTTTTTTTTCC.CCC
		// where:
		// LLLLLL = link course# - 1,6
		// CCCCCC = exploded course# - 7,6
		// TTTTTTTTTTTT = exploded course title - 13,12
		// CC.CCC = transcript credits - 25,6

		DependentCoursesList.addElement("T01   6ERA  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T01   6ERB  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T01   6MRA  6 Math Reg  00.500");
		DependentCoursesList.addElement("T01   6MRB  6 Math Reg  00.500");
		DependentCoursesList.addElement("T01   6SORA 6 SocStudReg00.500");
		DependentCoursesList.addElement("T01   6SORB 6 SocStudReg00.500");
		DependentCoursesList.addElement("T01   6SRA  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T01   6SRB  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T02   6ERA  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T02   6ERB  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T02   6MRA  6 Math Reg  00.500");
		DependentCoursesList.addElement("T02   6MRB  6 Math Reg  00.500");
		DependentCoursesList.addElement("T02   6SORA 6 SocStudReg00.500");
		DependentCoursesList.addElement("T02   6SORB 6 SocStudReg00.500");
		DependentCoursesList.addElement("T02   6SPA  6 Sci PreA/P00.500");
		DependentCoursesList.addElement("T02   6SPB  6 Sci PreA/P00.500");
		DependentCoursesList.addElement("T03   6ERA  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T03   6ERB  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T03   6MRA  6 Math Reg  00.500");
		DependentCoursesList.addElement("T03   6MRB  6 Math Reg  00.500");
		DependentCoursesList.addElement("T03   6SOPA 6 SocStudA/P00.500");
		DependentCoursesList.addElement("T03   6SOPB 6 SocStudA/P00.500");
		DependentCoursesList.addElement("T03   6SRA  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T03   6SRB  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T04   6ERA  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T04   6ERB  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T04   6MRA  6 Math Reg  00.500");
		DependentCoursesList.addElement("T04   6MRB  6 Math Reg  00.500");
		DependentCoursesList.addElement("T04   6SOPA 6 SocStudA/P00.500");
		DependentCoursesList.addElement("T04   6SOPB 6 SocStudA/P00.500");
		DependentCoursesList.addElement("T04   6SRA  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T04   6SRB  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T05   6ERA  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T05   6ERB  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T05   6MPA  6 Math PreAP00.500");
		DependentCoursesList.addElement("T05   6MPB  6 Math PreAP00.500");
		DependentCoursesList.addElement("T05   6SORA 6 SocStudReg00.500");
		DependentCoursesList.addElement("T05   6SORB 6 SocStudReg00.500");
		DependentCoursesList.addElement("T05   6SRA  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T05   6SRB  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T06   6ERA  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T06   6ERB  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T06   6MPA  6 Math PreAP00.500");
		DependentCoursesList.addElement("T06   6MPB  6 Math PreAP00.500");
		DependentCoursesList.addElement("T06   6SORA 6 SocStudReg00.500");
		DependentCoursesList.addElement("T06   6SORB 6 SocStudReg00.500");
		DependentCoursesList.addElement("T06   6SPA  6 Sci PreAP 00.500");
		DependentCoursesList.addElement("T06   6SPB  6 Sci PreAP 00.500");
		DependentCoursesList.addElement("T07   6ERA  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T07   6ERB  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T07   6MPA  6 Math PreAP00.500");
		DependentCoursesList.addElement("T07   6MPB  6 Math PreAP00.500");
		DependentCoursesList.addElement("T07   6SOPA 6 SocStud AP00.500");
		DependentCoursesList.addElement("T07   6SOPB 6 SocStud AP00.500");
		DependentCoursesList.addElement("T07   6SRA  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T07   6SRB  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T08   6ERA  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T08   6ERB  6 Eng Reg   00.500");
		DependentCoursesList.addElement("T08   6MPA  6 Math PreAP00.500");
		DependentCoursesList.addElement("T08   6MPB  6 Math PreAP00.500");
		DependentCoursesList.addElement("T08   6SOPA 6 SocStud AP00.500");
		DependentCoursesList.addElement("T08   6SOPB 6 SocStud AP00.500");
		DependentCoursesList.addElement("T08   6SPA  6 Sci Pre AP00.500");
		DependentCoursesList.addElement("T08   6SPB  6 Sci Pre AP00.500");
		DependentCoursesList.addElement("T09   6EPA  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T09   6EPB  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T09   6MRA  6 Math Reg  00.500");
		DependentCoursesList.addElement("T09   6MRB  6 Math Reg  00.500");
		DependentCoursesList.addElement("T09   6SORA 6 SocStudReg00.500");
		DependentCoursesList.addElement("T09   6SORB 6 SocStudReg00.500");
		DependentCoursesList.addElement("T09   6SRA  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T09   6SRB  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T10   6EPA  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T10   6EPB  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T10   6MRA  6 Math Reg  00.500");
		DependentCoursesList.addElement("T10   6MRB  6 Math Reg  00.500");
		DependentCoursesList.addElement("T10   6SORA 6 SocStudReg00.500");
		DependentCoursesList.addElement("T10   6SORB 6 SocStudReg00.500");
		DependentCoursesList.addElement("T10   6SPA  6 Sci Pre AP00.500");
		DependentCoursesList.addElement("T10   6SPB  6 Sci Pre AP00.500");
		DependentCoursesList.addElement("T11   6EPA  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T11   6EPB  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T11   6MRA  6 Math Reg  00.500");
		DependentCoursesList.addElement("T11   6MRB  6 Math Reg  00.500");
		DependentCoursesList.addElement("T11   6SOPA 6 SocStud AP00.500");
		DependentCoursesList.addElement("T11   6SOPB 6 SocStud AP00.500");
		DependentCoursesList.addElement("T11   6SRA  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T11   6SRB  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T12   6EPA  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T12   6EPB  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T12   6MRA  6 Math Reg  00.500");
		DependentCoursesList.addElement("T12   6MRB  6 Math Reg  00.500");
		DependentCoursesList.addElement("T12   6SOPA 6 SocStud AP00.500");
		DependentCoursesList.addElement("T12   6SOPB 6 SocStud AP00.500");
		DependentCoursesList.addElement("T12   6SPA  6 Sci Pre AP00.500");
		DependentCoursesList.addElement("T12   6SPB  6 Sci Pre AP00.500");
		DependentCoursesList.addElement("T13   6EPA  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T13   6EPB  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T13   6MPA  6 Math PreAP00.500");
		DependentCoursesList.addElement("T13   6MPB  6 Math PreAP00.500");
		DependentCoursesList.addElement("T13   6SORA 6 SocStudReg00.500");
		DependentCoursesList.addElement("T13   6SORB 6 SocStudReg00.500");
		DependentCoursesList.addElement("T13   6SRA  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T13   6SRB  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T14   6EPA  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T14   6EPB  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T14   6MPA  6 Math PreAP00.500");
		DependentCoursesList.addElement("T14   6MPB  6 Math PreAP00.500");
		DependentCoursesList.addElement("T14   6SORA 6 SocStudReg00.500");
		DependentCoursesList.addElement("T14   6SORB 6 SocStudReg00.500");
		DependentCoursesList.addElement("T14   6SPA  6 Sci Pre AP00.500");
		DependentCoursesList.addElement("T14   6SPB  6 Sci Pre AP00.500");
		DependentCoursesList.addElement("T15   6EPA  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T15   6EPB  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T15   6MPA  6 Math PreAP00.500");
		DependentCoursesList.addElement("T15   6MPB  6 Math PreAP00.500");
		DependentCoursesList.addElement("T15   6SOPA 6 SocStud AP00.500");
		DependentCoursesList.addElement("T15   6SOPB 6 SocStud AP00.500");
		DependentCoursesList.addElement("T15   6SRA  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T15   6SRB  6 Sci Reg   00.500");
		DependentCoursesList.addElement("T16   6EPA  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T16   6EPB  6 Eng Pre AP00.500");
		DependentCoursesList.addElement("T16   6MPA  6 Math PreAP00.500");
		DependentCoursesList.addElement("T16   6MPB  6 Math PreAP00.500");
		DependentCoursesList.addElement("T16   6SOPA 6 SocStud AP00.500");
		DependentCoursesList.addElement("T16   6SOPB 6 SocStud AP00.500");
		DependentCoursesList.addElement("T16   6SPA  6 Sci Pre AP00.500");
		DependentCoursesList.addElement("T16   6SPB  6 Sci Pre AP00.500");
		DependentCoursesList.addElement("410   410A  Gen Math S1 00.500");
		DependentCoursesList.addElement("410   410B  Gen Math S2 00.500");
		DependentCoursesList.addElement("420   420A  Algebra I S100.500");
		DependentCoursesList.addElement("420   420B  Algebra I S200.500");
		DependentCoursesList.addElement("430   430A  Alg II S1   00.500");
		DependentCoursesList.addElement("430   430B  Alg II S2   00.500");
		DependentCoursesList.addElement("310   310A  English 9 S100.500");
		DependentCoursesList.addElement("310   310B  English 9 S200.500");
		DependentCoursesList.addElement("320   320A  LiteratureS100.500");
		DependentCoursesList.addElement("320   320B  LiteratureS200.500");
		DependentCoursesList.addElement("330   330A  Bus Writing100.500");
		DependentCoursesList.addElement("330   330B  Bus Writing200.500");
		DependentCoursesList.addElement("510   510A  Earth Sci S100.500");
		DependentCoursesList.addElement("510   510B  Earth Sci S200.500");
		DependentCoursesList.addElement("520   520A  Biology   S100.500");
		DependentCoursesList.addElement("520   520B  Biology   S200.500");
		DependentCoursesList.addElement("530   530A  Chemistry S100.500");
		DependentCoursesList.addElement("530   530B  Chemistry S200.500");
		DependentCoursesList.addElement("210   210A  U.S. Hist S100.500");
		DependentCoursesList.addElement("210   210B  U.S. Hist S200.500");
		DependentCoursesList.addElement("220   220A  World Hist 100.500");
		DependentCoursesList.addElement("220   220B  World Hist 200.500");
		DependentCoursesList.addElement("230   230A  AncientRome100.500");
		DependentCoursesList.addElement("230   230B  AncientRome200.500");

		// Load group/dept list:
		GroupDeptList = new Vector();
		GroupDeptList.clear();

		GroupDeptList.addElement("001. Math");
		GroupDeptList.addElement("002. English");
		GroupDeptList.addElement("003. Science");
		GroupDeptList.addElement("004. History");
		GroupDeptList.addElement("005. Electives");
		GroupDeptList.addElement("006. Team");

		// Load core levels list:
		CoreLevelsList = new Vector();
		CoreLevelsList.clear();

		CoreLevelsList.addElement("(any)");
		CoreLevelsList.addElement("Regular");
		CoreLevelsList.addElement("Pre-A/P");
		CoreLevelsList.addElement("Gifted");



		titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(134, 134, 150)),"Limit list at right to");
		this.setFont(new java.awt.Font("SansSerif", 0, 8));
		this.getContentPane().setBackground(new Color(233, 228, 205));
		this.setSize(new Dimension(790,580));
		this.getContentPane().setLayout(null);
		MainTabbedPane.setBackground(new Color(192, 192, 215));
		MainTabbedPane.setFont(new java.awt.Font("SansSerif", 0, 11));
		MainTabbedPane.setBounds(new Rectangle(8, 88, 774, 482));
		MainTabbedPane.addChangeListener(new javax.swing.event.ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				MainTabbedPane_stateChanged(e);
			}
		});


		SelectionsPanel.setBackground(new Color(192, 192, 215));
		SelectionsPanel.setLayout(null);
		CreditsPanel.setBackground(new Color(192, 206, 192));
		CreditsPanel.setLayout(null);
		HeaderPanel.setBackground(new Color(233, 228, 205));
		HeaderPanel.setBounds(new Rectangle(4, 9, 782, 75));
		HeaderPanel.setLayout(null);
		jLabel1.setFont(new java.awt.Font("SansSerif", 1, 11));
		jLabel1.setText("Student name:");
		jLabel1.setBounds(new Rectangle(9, 9, 89, 17));
		StudentNameLabel.setFont(new java.awt.Font("SansSerif", 1, 11));
		StudentNameLabel.setText("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		StudentNameLabel.setBounds(new Rectangle(101, 9, 366, 17));
		jLabel2.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel2.setText("Current Yr:  Dist:");
		jLabel2.setBounds(new Rectangle(9, 27, 86, 17));
		CurrentYearDistrictLabel.setBounds(new Rectangle(99, 27, 28, 17));
		CurrentYearDistrictLabel.setText("XXX");
		CurrentYearDistrictLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel3.setBounds(new Rectangle(126, 27, 32, 17));
		jLabel3.setText("Schl:");
		jLabel3.setFont(new java.awt.Font("SansSerif", 0, 11));
		CurrentYearSchoolLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		CurrentYearSchoolLabel.setText("XXX");
		CurrentYearSchoolLabel.setBounds(new Rectangle(161, 27, 30, 17));
		jLabel4.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel4.setText("Class:");
		jLabel4.setBounds(new Rectangle(190, 27, 36, 17));
		CurrentYearClassLabel.setBounds(new Rectangle(227, 27, 28, 17));
		CurrentYearClassLabel.setText("XXX");
		CurrentYearClassLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel5.setBounds(new Rectangle(256, 27, 58, 17));
		jLabel5.setText("Counselor:");
		jLabel5.setFont(new java.awt.Font("SansSerif", 0, 11));
		CurrentYearCounselorNameLabel.setBounds(new Rectangle(310, 27, 108, 17));
		CurrentYearCounselorNameLabel.setText("XXXXXXXXXXXXXXX");
		CurrentYearCounselorNameLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel6.setBounds(new Rectangle(9, 45, 86, 17));
		jLabel6.setText("Next Yr:  Dist:");
		jLabel6.setFont(new java.awt.Font("SansSerif", 0, 11));
		NextYearDistrictLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		NextYearDistrictLabel.setText("XXX");
		NextYearDistrictLabel.setBounds(new Rectangle(99, 45, 28, 17));
		jLabel7.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel7.setText("Schl:");
		jLabel7.setBounds(new Rectangle(126, 45, 32, 17));
		NextYearSchoolLabel.setBounds(new Rectangle(161, 45, 30, 17));
		NextYearSchoolLabel.setText("XXX");
		NextYearSchoolLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel8.setBounds(new Rectangle(190, 45, 36, 17));
		jLabel8.setText("Class:");
		jLabel8.setFont(new java.awt.Font("SansSerif", 0, 11));
		NextYearClassLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		NextYearClassLabel.setText("XXX");
		NextYearClassLabel.setBounds(new Rectangle(227, 45, 28, 17));
		jLabel9.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel9.setText("Counselor:");
		jLabel9.setBounds(new Rectangle(256, 45, 58, 17));
		NextYearCounselorNameLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		NextYearCounselorNameLabel.setText("XXXXXXXXXXXXXXX");
		NextYearCounselorNameLabel.setBounds(new Rectangle(310, 45, 108, 17));
		jLabel10.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel10.setText("Academy:");
		jLabel10.setBounds(new Rectangle(517, 9, 55, 17));
		AcademyLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		AcademyLabel.setText("XXXXXXXXXXXXXXXXXXXX");
		AcademyLabel.setBounds(new Rectangle(579, 9, 175, 17));
		SemesterNotePanel.setBackground(new Color(215, 192, 192));
		SemesterNotePanel.setFont(new java.awt.Font("SansSerif", 0, 11));
		SemesterNotePanel.setBorder(BorderFactory.createLineBorder(Color.black));
		SemesterNotePanel.setBounds(new Rectangle(429, 29, 347, 40));
		SemesterNotePanel.setLayout(null);
		jLabel11.setFont(new java.awt.Font("SansSerif", 1, 11));
		jLabel11.setText("Note:  \"Split from\" courses ending with 1 indicate first");
		jLabel11.setBounds(new Rectangle(4, 4, 335, 16));
		jLabel12.setFont(new java.awt.Font("SansSerif", 1, 11));
		jLabel12.setText("semester;  those ending with 3 indicate second semester.");
		jLabel12.setBounds(new Rectangle(4, 18, 337, 17));
		SelectionsTopPanel.setBackground(new Color(192, 192, 215));
		SelectionsTopPanel.setFont(new java.awt.Font("SansSerif", 0, 11));
		SelectionsTopPanel.setBorder(BorderFactory.createRaisedBevelBorder());
		SelectionsTopPanel.setBounds(new Rectangle(3, 7, 764, 209));
		SelectionsTopPanel.setLayout(null);
		CourseTableScrollPane.setBorder(BorderFactory.createLoweredBevelBorder());
		CourseTableScrollPane.setBounds(new Rectangle(309, 7, 449, 193));
		DeptComboBox.setFont(new java.awt.Font("SansSerif", 0, 11));
		DeptComboBox.setBounds(new Rectangle(79, 7, 217, 21));
		DeptComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				DeptComboBox_itemStateChanged(e);
			}
		});

		// Add elements to Group/Department combo box:
		DeptComboBox.removeAllItems();
		for (int gdl = 0; gdl <= GroupDeptList.size() - 1 ; gdl++)
			DeptComboBox.addItem(GroupDeptList.elementAt(gdl));


		jLabel13.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel13.setText("Group/dept:");
		jLabel13.setBounds(new Rectangle(9, 7, 64, 21));
		PreviousButton.setBackground(new Color(192, 206, 192));
		PreviousButton.setFont(new java.awt.Font("SansSerif", 0, 11));
		PreviousButton.setText("Previous");
		PreviousButton.setBounds(new Rectangle(86, 32, 80, 25));
		PreviousButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PreviousButton_actionPerformed(e);
			}
		});
		FirstButton.setBounds(new Rectangle(9, 32, 70, 25));
		FirstButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FirstButton_actionPerformed(e);
			}
		});
		FirstButton.setText("First");
		FirstButton.setFont(new java.awt.Font("SansSerif", 0, 11));
		FirstButton.setBackground(new Color(192, 206, 192));
		NextButton.setBounds(new Rectangle(174, 32, 60, 25));
		NextButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NextButton_actionPerformed(e);
			}
		});
		NextButton.setText("Next");
		NextButton.setFont(new java.awt.Font("SansSerif", 0, 11));
		NextButton.setBackground(new Color(192, 206, 192));
		LastButton.setBounds(new Rectangle(241, 32, 62, 25));
		LastButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LastButton_actionPerformed(e);
			}
		});
		LastButton.setText("Last");
		LastButton.setFont(new java.awt.Font("SansSerif", 0, 11));
		LastButton.setBackground(new Color(192, 206, 192));
		LimitToGroupPanel.setBackground(new Color(192, 192, 215));
		LimitToGroupPanel.setBorder(titledBorder1);
		LimitToGroupPanel.setBounds(new Rectangle(8, 71, 287, 89));
		LimitToGroupPanel.setLayout(null);
		EngComboBox.setBounds(new Rectangle(79, 22, 70, 21));
		EngComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				EngComboBox_itemStateChanged(e);
			}
		});
		EngComboBox.setFont(new java.awt.Font("SansSerif", 0, 11));
		MathComboBox.setFont(new java.awt.Font("SansSerif", 0, 11));
		MathComboBox.setBounds(new Rectangle(207, 22, 70, 21));
		MathComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				MathComboBox_itemStateChanged(e);
			}
		});
		SocComboBox.setFont(new java.awt.Font("SansSerif", 0, 11));
		SocComboBox.setBounds(new Rectangle(79, 55, 70, 21));
		SocComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				SocComboBox_itemStateChanged(e);
			}
		});
		SciComboBox.setFont(new java.awt.Font("SansSerif", 0, 11));
		SciComboBox.setBounds(new Rectangle(207, 55, 70, 21));
		SciComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				SciComboBox_itemStateChanged(e);
			}
		});

		// Add elements to core level combo boxes:
		EngComboBox.removeAllItems();
		MathComboBox.removeAllItems();
		SocComboBox.removeAllItems();
		SciComboBox.removeAllItems();
		for (int cll = 0; cll <= CoreLevelsList.size() - 2 ; cll++) {   // change " - 2 " to " - 1 " in future
			EngComboBox.addItem(CoreLevelsList.elementAt(cll));
			MathComboBox.addItem(CoreLevelsList.elementAt(cll));
			SocComboBox.addItem(CoreLevelsList.elementAt(cll));
			SciComboBox.addItem(CoreLevelsList.elementAt(cll));
		}   // for (int cll = 0; cll <= CoreLevelsList.size(); cll++)



		jLabel14.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel14.setText("Science:");
		jLabel14.setBounds(new Rectangle(157, 56, 49, 18));
		jLabel15.setBounds(new Rectangle(157, 23, 49, 18));
		jLabel15.setText("Math:");
		jLabel15.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel16.setBounds(new Rectangle(9, 56, 64, 18));
		jLabel16.setText("Soc Studies:");
		jLabel16.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel17.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel17.setText("English:");
		jLabel17.setBounds(new Rectangle(9, 23, 64, 18));
		jLabel18.setFont(new java.awt.Font("SansSerif", 1, 11));
		jLabel18.setText("To make selections use the Selector column at right.");
		jLabel18.setBounds(new Rectangle(9, 164, 292, 14));
		SelectionsBottomPanel.setBackground(new Color(192, 192, 215));
		SelectionsBottomPanel.setBorder(BorderFactory.createRaisedBevelBorder());
		SelectionsBottomPanel.setBounds(new Rectangle(3, 220, 764, 233));
		SelectionsBottomPanel.setLayout(null);
		SelectedCoursesTableScrollPane.setBorder(BorderFactory.createLoweredBevelBorder());
		SelectedCoursesTableScrollPane.setBounds(new Rectangle(3, 34, 607, 191));
		jPanel1.setBackground(new Color(192, 192, 215));
		jPanel1.setBounds(new Rectangle(3, 3, 607, 28));
		jPanel1.setLayout(null);
		jLabel19.setFont(new java.awt.Font("SansSerif", 1, 11));
		jLabel19.setText("Selected Courses");
		jLabel19.setBounds(new Rectangle(251, 7, 105, 14));
		SelectionsBottomRightPanel.setBackground(new Color(192, 192, 215));
		SelectionsBottomRightPanel.setBounds(new Rectangle(614, 4, 145, 224));
		SelectionsBottomRightPanel.setLayout(null);
		SaveAndExitButton.setBackground(new Color(192, 206, 192));
		SaveAndExitButton.setFont(new java.awt.Font("SansSerif", 0, 11));
		SaveAndExitButton.setText("Save and Exit");
		SaveAndExitButton.setBounds(new Rectangle(19, 160, 107, 25));
		SaveAndExitButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SaveAndExitButton_actionPerformed(e);
			}
		});
		CancelButton.setBounds(new Rectangle(19, 190, 107, 25));
		CancelButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CancelButton_actionPerformed(e);
			}
		});
		CancelButton.setText("Cancel");
		CancelButton.setFont(new java.awt.Font("SansSerif", 0, 11));
		CancelButton.setBackground(new Color(192, 206, 192));
		PrintButton.setBounds(new Rectangle(19, 130, 107, 25));
		PrintButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PrintButton_actionPerformed(e);
			}
		});
		PrintButton.setText("Print");
		PrintButton.setFont(new java.awt.Font("SansSerif", 0, 11));
		PrintButton.setBackground(new Color(192, 206, 192));
		jLabel20.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel20.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel20.setHorizontalTextPosition(SwingConstants.LEADING);
		jLabel20.setText("Selections Count");
		jLabel20.setBounds(new Rectangle(14, 2, 116, 20));
		jLabel21.setBounds(new Rectangle(4, 38, 137, 20));
		jLabel21.setText("Alternate Selections Count");
		jLabel21.setHorizontalTextPosition(SwingConstants.LEADING);
		jLabel21.setFont(new java.awt.Font("SansSerif", 0, 11));
		jLabel21.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel22.setBounds(new Rectangle(5, 73, 134, 20));
		jLabel22.setText("Transcript Credits Selected");
		jLabel22.setHorizontalTextPosition(SwingConstants.LEADING);
		jLabel22.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel22.setFont(new java.awt.Font("SansSerif", 0, 11));
		TotalSelectionsCountLabel.setBounds(new Rectangle(14, 17, 116, 20));
		TotalSelectionsCountLabel.setText("XX");
		TotalSelectionsCountLabel.setHorizontalTextPosition(SwingConstants.LEADING);
		TotalSelectionsCountLabel.setHorizontalAlignment(SwingConstants.CENTER);
		TotalSelectionsCountLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		TotalSelectionsAltCountLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		TotalSelectionsAltCountLabel.setHorizontalAlignment(SwingConstants.CENTER);
		TotalSelectionsAltCountLabel.setHorizontalTextPosition(SwingConstants.LEADING);
		TotalSelectionsAltCountLabel.setText("XX");
		TotalSelectionsAltCountLabel.setBounds(new Rectangle(14, 54, 116, 20));
		TotalTranscriptCreditsLabel.setFont(new java.awt.Font("SansSerif", 0, 11));
		TotalTranscriptCreditsLabel.setHorizontalAlignment(SwingConstants.CENTER);
		TotalTranscriptCreditsLabel.setHorizontalTextPosition(SwingConstants.LEADING);
		TotalTranscriptCreditsLabel.setText("XX.XXX");
		TotalTranscriptCreditsLabel.setBounds(new Rectangle(14, 90, 116, 20));
		CreditsTableScrollPane.setBorder(BorderFactory.createLoweredBevelBorder());
		CreditsTableScrollPane.setBounds(new Rectangle(4, 30, 761, 379));
		CreditsBottomPanel.setBackground(new Color(192, 206, 192));
		CreditsBottomPanel.setBounds(new Rectangle(4, 415, 761, 35));
		CreditsBottomPanel.setLayout(null);
		jLabel23.setFont(new java.awt.Font("SansSerif", 0, 12));
		jLabel23.setText("Total transcript credits:");
		jLabel23.setBounds(new Rectangle(256, 8, 135, 18));
		TotalCreditsLabel.setBounds(new Rectangle(395, 8, 59, 18));
		TotalCreditsLabel.setText("XXX.XXX");
		TotalCreditsLabel.setFont(new java.awt.Font("SansSerif", 0, 12));
		CourseTable.setFont(new java.awt.Font("SansSerif", 0, 11));
		CourseTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		CourseTable.setColumnSelectionAllowed(true);
		CourseTable.setRowHeight(18);

		// Set course table so it cannot be edited:
		//CourseTable.setDefaultEditor(Object.class,null);

		SelectedCoursesTable.setFont(new java.awt.Font("SansSerif", 0, 11));
		SelectedCoursesTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		SelectedCoursesTable.setRowHeight(18);
		SelectedCoursesTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				SelectedCoursesTable_mouseReleased(e);
			}
		});
		CreditsTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		CreditsTable.setRowHeight(18);
		MoveUpButton.setBackground(new Color(192, 206, 192));
		MoveUpButton.setFont(new java.awt.Font("SansSerif", 0, 10));
		MoveUpButton.setText("Move Up");
		MoveUpButton.setBounds(new Rectangle(0, 4, 92, 20));
		MoveUpButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MoveUpButton_actionPerformed(e);
			}
		});
		MoveUpButton.setVisible(false);
		MoveDownButton.setBounds(new Rectangle(95, 4, 92, 20));
		MoveDownButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MoveDownButton_actionPerformed(e);
			}
		});
		MoveDownButton.setText("Move Down");
		MoveDownButton.setFont(new java.awt.Font("SansSerif", 0, 10));
		MoveDownButton.setBackground(new Color(192, 206, 192));
		MoveDownButton.setVisible(false);
		this.getContentPane().add(MainTabbedPane, null);
		MainTabbedPane.add(SelectionsPanel, "Selections");
		SelectionsPanel.add(SelectionsBottomPanel, null);
		SelectionsBottomPanel.add(jPanel1, null);
		jPanel1.add(jLabel19, null);
		jPanel1.add(MoveUpButton, null);
		jPanel1.add(MoveDownButton, null);
		SelectionsBottomPanel.add(SelectedCoursesTableScrollPane, null);
		SelectedCoursesTableScrollPane.getViewport().add(SelectedCoursesTable, null);
		SelectionsBottomPanel.add(SelectionsBottomRightPanel, null);
		SelectionsBottomRightPanel.add(CancelButton, null);
		SelectionsBottomRightPanel.add(SaveAndExitButton, null);
		SelectionsBottomRightPanel.add(PrintButton, null);
		SelectionsBottomRightPanel.add(jLabel21, null);
		SelectionsBottomRightPanel.add(jLabel22, null);
		SelectionsBottomRightPanel.add(jLabel20, null);
		SelectionsBottomRightPanel.add(TotalSelectionsCountLabel, null);
		SelectionsBottomRightPanel.add(TotalSelectionsAltCountLabel, null);
		SelectionsBottomRightPanel.add(TotalTranscriptCreditsLabel, null);
		SelectionsPanel.add(SelectionsTopPanel, null);
		SelectionsTopPanel.add(CourseTableScrollPane, null);
		CourseTableScrollPane.getViewport().add(CourseTable, null);
		SelectionsTopPanel.add(DeptComboBox, null);
		SelectionsTopPanel.add(PreviousButton, null);
		SelectionsTopPanel.add(FirstButton, null);
		SelectionsTopPanel.add(NextButton, null);
		SelectionsTopPanel.add(LastButton, null);
		SelectionsTopPanel.add(LimitToGroupPanel, null);
		LimitToGroupPanel.add(MathComboBox, null);
		LimitToGroupPanel.add(SciComboBox, null);
		LimitToGroupPanel.add(EngComboBox, null);
		LimitToGroupPanel.add(jLabel15, null);
		LimitToGroupPanel.add(jLabel14, null);
		LimitToGroupPanel.add(SocComboBox, null);
		LimitToGroupPanel.add(jLabel16, null);
		LimitToGroupPanel.add(jLabel17, null);
		SelectionsTopPanel.add(jLabel13, null);
		SelectionsTopPanel.add(jLabel18, null);
		MainTabbedPane.add(CreditsPanel, "Transcript Credits");

		MainTabbedPane.setBackgroundAt(0,new Color(192, 192, 215));  // make tab match - Selections
		MainTabbedPane.setBackgroundAt(1,new Color(192, 206, 192));  // make tab match - Transcript Credits


		CreditsPanel.add(CreditsTableScrollPane, null);
		CreditsTableScrollPane.getViewport().add(CreditsTable, null);
		CreditsPanel.add(CreditsBottomPanel, null);
		CreditsBottomPanel.add(TotalCreditsLabel, null);
		CreditsBottomPanel.add(jLabel23, null);
		this.getContentPane().add(HeaderPanel, null);
		HeaderPanel.add(jLabel1, null);
		HeaderPanel.add(StudentNameLabel, null);
		HeaderPanel.add(jLabel2, null);
		HeaderPanel.add(CurrentYearCounselorNameLabel, null);
		HeaderPanel.add(jLabel5, null);
		HeaderPanel.add(CurrentYearClassLabel, null);
		HeaderPanel.add(jLabel4, null);
		HeaderPanel.add(CurrentYearSchoolLabel, null);
		HeaderPanel.add(jLabel3, null);
		HeaderPanel.add(CurrentYearDistrictLabel, null);
		HeaderPanel.add(jLabel6, null);
		HeaderPanel.add(NextYearDistrictLabel, null);
		HeaderPanel.add(jLabel7, null);
		HeaderPanel.add(NextYearSchoolLabel, null);
		HeaderPanel.add(jLabel8, null);
		HeaderPanel.add(NextYearClassLabel, null);
		HeaderPanel.add(jLabel9, null);
		HeaderPanel.add(NextYearCounselorNameLabel, null);
		HeaderPanel.add(AcademyLabel, null);
		HeaderPanel.add(jLabel10, null);
		HeaderPanel.add(SemesterNotePanel, null);
		SemesterNotePanel.add(jLabel11, null);
		SemesterNotePanel.add(jLabel12, null);


	}   // end jbInit()


	/**Start the applet*/
	public void start() {

		// Hide the main application window until entry screens are gone through:
		HeaderPanel.setVisible(false);
		MainTabbedPane.setVisible(false);
		this.setEnabled(false);

		// Show the sign-in frame:
		ShowSignInFrame();

	}   // end of start()


	void ShowSignInFrame() {

		GlobalStudentID = "";
		GlobalBirthDate = "";
		mySignInFrame.OKPressed = false;
		mySignInFrame.ExitPressed = false;

		mySignInFrame.setVisible(true);

		mySignInFrame.addWindowListener(new java.awt.event.WindowListener() {
			public void windowClosed(WindowEvent e) {
				SignInFrame_windowClosed(e);
			}
			public void windowDeactivated(WindowEvent e) {
				SignInFrame_windowDeactivated(e);
			}
			public void windowActivated(WindowEvent e) {
				SignInFrame_windowActivated(e);
			}
			public void windowDeiconified(WindowEvent e) {
				SignInFrame_windowDeiconified(e);
			}
			public void windowIconified(WindowEvent e) {
				SignInFrame_windowIconified(e);
			}
			public void windowClosing(WindowEvent e) {
				SignInFrame_windowClosing(e);
			}
			public void windowOpened(WindowEvent e) {
				SignInFrame_windowOpened(e);
			}
		} );   // mySignInFrame.addWindowListener(new java.awt.event.WindowListener()

	}   // end of ShowSignInFrame()



	// Following are "listen to" events for the SignInFrame window:

	void SignInFrame_windowClosed(WindowEvent e) {
	}   // SignInFrame_windowClosed


	void SignInFrame_windowDeactivated(WindowEvent e) {

		// This method is called when the Sign-In Frame is hidden -- which is
		// when the user clicks the X in the upper right corner or if they click the
		// OK or Exit button.

		// Fetch values from the SignInFrame window:
		GlobalStudentID = new String(mySignInFrame.StudentIDPasswordField.getPassword() );
		GlobalBirthDate = new String(mySignInFrame.BirthDatePasswordField.getPassword() );
		GlobalSignInOKPressed = mySignInFrame.OKPressed;
		GlobalSignInExitPressed = mySignInFrame.ExitPressed;

		// If OK pressed on SignInFrame window, show the ConfirmationFrame window:
		if (GlobalSignInOKPressed)
			ShowConfirmationFrame();
		else
			// Shut down the applet:
			this.destroy();
	}

	void SignInFrame_windowActivated(WindowEvent e) {
	}

	void SignInFrame_windowDeiconified(WindowEvent e) {
	}

	void SignInFrame_windowIconified(WindowEvent e) {
	}

	void SignInFrame_windowClosing(WindowEvent e) {
	}

	void SignInFrame_windowOpened(WindowEvent e) {
	}


	void ShowConfirmationFrame() {

		GlobalAcademy = "";
		myConfirmationFrame.OKPressed = false;
		myConfirmationFrame.CancelPressed = false;

		// Fill field values on screen before displaying:
		myConfirmationFrame.StudentNameLabel.setText("DAIN, DIANA LYNN");

		myConfirmationFrame.CurrentYearDistrictLabel.setText("HKS");
		myConfirmationFrame.CurrentYearDistrictNameLabel.setText("Hard Knock Public Schools");
		myConfirmationFrame.CurrentYearSchoolLabel.setText("HKH");
		myConfirmationFrame.CurrentYearSchoolNameLabel.setText("Hard Knock High School");
		myConfirmationFrame.CurrentYearClassLabel.setText("11");
		myConfirmationFrame.CurrentYearCounselorNameLabel.setText("Mrs. Jones");

		myConfirmationFrame.NextYearDistrictLabel.setText("HKS");
		myConfirmationFrame.NextYearDistrictNameLabel.setText("Hard Knock Public Schools");
		myConfirmationFrame.NextYearSchoolLabel.setText("HKH");
		myConfirmationFrame.NextYearSchoolNameLabel.setText("Hard Knock High School");
		myConfirmationFrame.NextYearClassLabel.setText("12");
		myConfirmationFrame.NextYearCounselorNameLabel.setText("Mr. Williams");

		myConfirmationFrame.AcademyComboBox.removeAllItems();
		myConfirmationFrame.AcademyComboBox.addItem("(none)");
		myConfirmationFrame.AcademyComboBox.addItem("01M - Medical");
		myConfirmationFrame.AcademyComboBox.addItem("01E - Engineering");
		myConfirmationFrame.AcademyComboBox.addItem("01T - Technical");

		myConfirmationFrame.setVisible(true);

		myConfirmationFrame.addWindowListener(new java.awt.event.WindowListener() {
			public void windowClosed(WindowEvent e) {
				ConfirmationFrame_windowClosed(e);
			}
			public void windowDeactivated(WindowEvent e) {
				ConfirmationFrame_windowDeactivated(e);
			}
			public void windowActivated(WindowEvent e) {
				ConfirmationFrame_windowActivated(e);
			}
			public void windowDeiconified(WindowEvent e) {
				ConfirmationFrame_windowDeiconified(e);
			}
			public void windowIconified(WindowEvent e) {
				ConfirmationFrame_windowIconified(e);
			}
			public void windowClosing(WindowEvent e) {
				ConfirmationFrame_windowClosing(e);
			}
			public void windowOpened(WindowEvent e) {
				ConfirmationFrame_windowOpened(e);
			}
		} );   // myConfirmationFrame.addWindowListener(new java.awt.event.WindowListener()

	}   // end of ConfirmationFrame()


	// Following are "listen to" events for the ConfirmationFrame Window:

	void ConfirmationFrame_windowClosed(WindowEvent e) {
	}

	void ConfirmationFrame_windowDeactivated(WindowEvent e) {

		// This method is called when the Confirmation Frame is hidden -- which is
		// when the user clicks the X in the upper right corner or if they click the
		// OK or Cancel button.

		// Fetch values from the ConfirmationFrame window:
		GlobalConfirmationOKPressed = myConfirmationFrame.OKPressed;
		GlobalConfirmationCancelPressed = myConfirmationFrame.CancelPressed;

		GlobalAcademy = "";
		if ( myConfirmationFrame.AcademyComboBox.getSelectedIndex() >= 0 )
			GlobalAcademy = myConfirmationFrame.AcademyComboBox.getItemAt(
					myConfirmationFrame.AcademyComboBox.getSelectedIndex() ).toString();

		// If OK pressed on ConfirmationFrame window, show the main selection window:
		if (GlobalConfirmationOKPressed) {

			// Populate header panel before showing:
			StudentNameLabel.setText(myConfirmationFrame.StudentNameLabel.getText());
			AcademyLabel.setText(myConfirmationFrame.AcademyComboBox.getSelectedItem().toString());

			CurrentYearDistrictLabel.setText(myConfirmationFrame.CurrentYearDistrictLabel.getText());
			CurrentYearSchoolLabel.setText(myConfirmationFrame.CurrentYearSchoolLabel.getText());
			CurrentYearClassLabel.setText(myConfirmationFrame.CurrentYearClassLabel.getText());
			CurrentYearCounselorNameLabel.setText(myConfirmationFrame.CurrentYearCounselorNameLabel.getText());

			NextYearDistrictLabel.setText(myConfirmationFrame.NextYearDistrictLabel.getText());
			NextYearSchoolLabel.setText(myConfirmationFrame.NextYearSchoolLabel.getText());
			NextYearClassLabel.setText(myConfirmationFrame.NextYearClassLabel.getText());
			NextYearCounselorNameLabel.setText(myConfirmationFrame.NextYearCounselorNameLabel.getText());

			SemesterNotePanel.setVisible(false);

			HeaderPanel.setVisible(true);

			// Populate the "Selections" tabbed pane:
			SelectedCoursesList = new Vector();
			SelectedCoursesList.removeAllElements();
			SelectedCoursesAltList = new Vector();
			SelectedCoursesAltList.removeAllElements();
			SequencedSelectionsList = new Vector();
			SequencedSelectionsList.removeAllElements();
			SplitLinksList = new Vector();
			SplitLinksList.removeAllElements();

			// Populate the "Transcript Credits" tabbed pane:
			MyCreditsTableModel.setResultSet();
			SetCreditsTableColumnWidths();

			MainTabbedPane.setVisible(true);
			this.setEnabled(true);

			InitiallyLoading = true;

			// Programatically click the "First" button:
			FirstButton_actionPerformed(
					new ActionEvent(FirstButton,
							java.awt.event.ActionEvent.ACTION_PERFORMED,
							null));

			SetSelectedCoursesGridValues();

			InitiallyLoading = false;

		}   // if (GlobalConfirmationOKPressed)
		else
			this.start();
	}

	void ConfirmationFrame_windowActivated(WindowEvent e) {
	}

	void ConfirmationFrame_windowDeiconified(WindowEvent e) {
	}

	void ConfirmationFrame_windowIconified(WindowEvent e) {
	}

	void ConfirmationFrame_windowClosing(WindowEvent e) {
		// This event occurs when the user presses "X" in the upper corner of screen.
		// Re-show sign-in frame when that happens.
		// GlobalAcademy="";

	}

	void ConfirmationFrame_windowOpened(WindowEvent e) {
	}



	/**Stop the applet*/
	public void stop() {
	}

	/**Destroy the applet*/
	public void destroy() {
		try {
			URL endedURL = new URL("http://www.bcswebsite.com/demos/CourseSelectionsDemoEnded.html");
			//
			// 2nd parameter to showDocument is optional.  If blank, defaults to
			// _self = current page.  Other choices:
			// _blank = new window; _top = outer frame; _parent = current frame
			this.getAppletContext().showDocument(endedURL);
		}   // try
		catch (MalformedURLException urlexc) {
			urlexc.printStackTrace();
		}   // catch

	}

	/**Get Applet information*/
	public String getAppletInfo() {
		return "Applet Information";
	}
	/**Get parameter info*/
	public String[][] getParameterInfo() {
		return null;
	}

	//static initializer for setting look & feel
	static {
		try {
			//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			//UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		}
		catch(Exception e) {
		}
	}


	void SetCourseGridValues() {

		// Note:  The order of items in this method are important in order for the
		// combobox drop-down to be shown.

		MyCourseTableModel.setResultSet();
		SetCourseTableColumnWidths();

		// Set the editor for column 0 ("Selector") to be a combobox:
		CourseTable.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(SelectorComboBox));

	}   // end of SetCourseGridValues()

	void SetSelectedCoursesGridValues() {
		MySelectedCoursesTableModel.setResultSet();
		SetSelectedCoursesTableColumnWidths();
	}   // end of SetSelectedCoursesGridValues()


	void SaveAndExitButton_actionPerformed(ActionEvent e) {
		// For now, just have this loop back to beginning of program:
		this.start();
	}

	void CancelButton_actionPerformed(ActionEvent e) {
		// For now, just have this loop back to beginning of program:
		this.start();

	}

	void FirstButton_actionPerformed(ActionEvent e) {
		DeptComboBox.setSelectedIndex(0);
		SetCourseGridValues();

	}

	void LastButton_actionPerformed(ActionEvent e) {
		DeptComboBox.setSelectedIndex(DeptComboBox.getItemCount() - 1 );
		SetCourseGridValues();

	}

	void NextButton_actionPerformed(ActionEvent e) {
		if ( DeptComboBox.getSelectedIndex() < DeptComboBox.getItemCount() - 1 )
			DeptComboBox.setSelectedIndex(DeptComboBox.getSelectedIndex() + 1 );
		SetCourseGridValues();

	}

	void PreviousButton_actionPerformed(ActionEvent e) {
		if ( DeptComboBox.getSelectedIndex() > 0 )
			DeptComboBox.setSelectedIndex(DeptComboBox.getSelectedIndex() - 1 );
		SetCourseGridValues();

	}

	//--------------------------------------------------------------------------

	//Definition of Course Table Model (table in upper right of window):

	class CourseTableModel extends AbstractTableModel
	{
		int columns = 8;                      // Get number of columns
		String[] columnNames = new String[columns];  // Array to hold names
		Vector dataRows = new Vector();
		String PreviousCellValue = "";


		//----- setResultSet() method:

		public void setResultSet()
		{
			columnNames[0] = "Selector";
			columnNames[1] = "Crs#";
			columnNames[2] = "Title";
			columnNames[3] = "Link";

			// load rows one after another
			dataRows.removeAllElements();                // stores all rows
			Object[] CourseRowData = new Object[columns];     // Stores one row


			// Define work fields:
			int x;
			int y;
			int z;
			String MasterCourseString = "";
			String MCLDept = "";
			String MCLCourse = "";
			String MCLCourseTitle = "";
			String MCLCredit = "";
			//myGridRect: TGridRect;
			String Course = "";
			String CourseTitle = "";
			int ListIndex;
			String Eng = "";
			String Math = "";
			String Soc = "";
			String Sci = "";
			boolean Qualifies;
			boolean LinkIsSplit;
			String ExplodedCourse = "";
			String ExplodedCourseTitle = "";
			String DependentCoursesString = "";
			String DCLLinkCourse = "";
			String DCLExpCourse = "";
			String DCLExpCourseTitle = "";
			String DCLCredit = "";

			// Determine whether current Group/dept is a "team" one:
			IsTeamDept = false;

			if (DeptComboBox.getSelectedIndex()==5)
				IsTeamDept = true;

			if (IsTeamDept) {
				LimitToGroupPanel.setVisible(true);
				LimitToGroupPanel.setEnabled(true);
				columnNames[4] = "Eng";
				columnNames[5] = "Math";
				columnNames[6] = "Soc";
				columnNames[7] = "Sci";
			}
			else {
				LimitToGroupPanel.setVisible(false);
				LimitToGroupPanel.setEnabled(false);
				columnNames[4] = "";
				columnNames[5] = "";
				columnNames[6] = "";
				columnNames[7] = "";
			}

			// Loop through MasterCourseList, processing only those for the
			// department indicated in DeptComboBox.getSelectedIndex() + 1
			y = 0;
			for (x = 1; x <= MasterCourseList.size(); x++) {
				MasterCourseString = MasterCourseList.get(x-1).toString();
				// Reminder: substring (a,b) means a=start position, b=end position + 1
				MCLDept = MasterCourseString.substring(0,3);
				MCLCourse = MasterCourseString.substring(3,9);
				MCLCourseTitle = MasterCourseString.substring(9,21);
				MCLCredit = MasterCourseString.substring(21,27);

				if ( Integer.parseInt(MCLDept) == DeptComboBox.getSelectedIndex() + 1) {
					Qualifies = true;
					CourseTitle = MCLCourseTitle;

					// If "team" Group/dept, check "limit to" criteria:
					if ( IsTeamDept) {
						Eng = CourseTitle.substring(1,2);
						Math = CourseTitle.substring(4,5);
						Soc = CourseTitle.substring(8,9);
						Sci = CourseTitle.substring(11,12);

						// Check to see if English qualifies:
						if ( (EngComboBox.getSelectedItem().toString().equals("Regular")) &&
								( !(Eng.equals("R")) ) )
							Qualifies = false;
						if ( (EngComboBox.getSelectedItem().toString().equals("Pre-A/P")) &&
								( !(Eng.equals("P")) ) )
							Qualifies = false;

						// Check to see if Math qualifies:
						if ( (MathComboBox.getSelectedItem().toString().equals("Regular")) &&
								( !(Math.equals("R")) ) )
							Qualifies = false;
						if ( (MathComboBox.getSelectedItem().toString().equals("Pre-A/P")) &&
								( !(Math.equals("P")) ) )
							Qualifies = false;

						// Check to see if Social Studies qualifies:
						if ( (SocComboBox.getSelectedItem().toString().equals("Regular")) &&
								( !(Soc.equals("R")) ) )
							Qualifies = false;
						if ( (SocComboBox.getSelectedItem().toString().equals("Pre-A/P")) &&
								( !(Soc.equals("P")) ) )
							Qualifies = false;

						// Check to see if Science qualifies:
						if ( (SciComboBox.getSelectedItem().toString().equals("Regular")) &&
								( !(Sci.equals("R")) ) )
							Qualifies = false;
						if ( (SciComboBox.getSelectedItem().toString().equals("Pre-A/P")) &&
								( !(Sci.equals("P")) ) )
							Qualifies = false;

					}   // if IsTeamDept


					if (Qualifies) {
						LinkIsSplit = false;
						y = y+1;
						Course = MCLCourse;
						CourseRowData[0] = "";
						if (SelectedCoursesList.indexOf(Course) >=0 )
							CourseRowData[0] = "Select";
						if (SelectedCoursesAltList.indexOf(Course) >= 0)
							CourseRowData[0] = "Select Alt";
						CourseRowData[1] = Course;
						if (IsTeamDept)
							CourseRowData[2] = "TEAM CHOICE";
						else
							CourseRowData[2] = CourseTitle;

						// See if a linked course:
						if (LinkCoursesList.indexOf(Course) >= 0) {
							// See if a split link:
							if ( (SplitLinksList.size() > 0) &&
									(SplitLinksList.indexOf(Course) >= 0) ) {
								LinkIsSplit = true;
								CourseRowData[3] = "Yes - Split";
							}   // if (SplitLinksList.indexOf(Course) >= 0)
							else
								CourseRowData[3] = "Yes";
						}   // if (LinkCoursesList.indexOf(Course) >= 0)
						else
							CourseRowData[3] = "No ";


						// Fill English, Math, Social Studies, and Science flags:
						CourseRowData[4] = "";
						CourseRowData[5] = "";
						CourseRowData[6] = "";
						CourseRowData[7] = "";
						if (IsTeamDept) {
							if (Eng.equals("R"))
								CourseRowData[4] = "Reg";
							else
								CourseRowData[4] = "A/P";
							if (Math.equals("R"))
								CourseRowData[5] = "Reg";
							else
								CourseRowData[5] = "A/P";
							if (Soc.equals("R"))
								CourseRowData[6] = "Reg";
							else
								CourseRowData[6] = "A/P";
							if (Sci.equals("R"))
								CourseRowData[7] = "Reg";
							else
								CourseRowData[7] = "A/P";
						}   // if IsTeamDept

						dataRows.addElement(CourseRowData.clone()); // store the rows in the vector

						// If linked course is split, display its dependent courses
						// immediately after the link course:
						if (LinkIsSplit) {
							for (z=1; z <= DependentCoursesList.size(); z++) {
								DependentCoursesString = DependentCoursesList.get(z-1).toString();
								// Reminder: substring (a,b) means a=start position, b=end position + 1
								DCLLinkCourse = DependentCoursesString.substring(0,6);
								DCLExpCourse = DependentCoursesString.substring(6,12);
								DCLExpCourseTitle = DependentCoursesString.substring(12,24);
								DCLCredit = DependentCoursesString.substring(24,30);

								if ( Course.equals(DCLLinkCourse) ) {
									y = y + 1;
									ExplodedCourse = DCLExpCourse;
									CourseRowData[0] = "";
									if (SelectedCoursesList.indexOf(ExplodedCourse) >= 0 )
										CourseRowData[0] = "Select";
									if (SelectedCoursesAltList.indexOf(ExplodedCourse) >= 0)
										CourseRowData[0] = "Select Alt";
									CourseRowData[1] = ExplodedCourse;
									ExplodedCourseTitle = DCLExpCourseTitle;
									CourseRowData[2] = ExplodedCourseTitle;
									CourseRowData[3] = "Split from "+Course;
									CourseRowData[4] ="";
									CourseRowData[5] ="";
									CourseRowData[6] ="";
									CourseRowData[7] ="";

									dataRows.addElement(CourseRowData.clone()); // store the rows in the vector
								}   // if ( Course.equals(DCLLinkCourse) )
							}   // for (z=1; DependentCoursesList.size(); z++)
						}   // if LinkIsSplit then



					}   // if Qualifies

				}   // if ( Integer.parseInt(MCLDept) = DeptComboBox.getSelectedIndex() + 1)

			}   // for (x = 1; x <= MasterCourseList.size(); x++)


			fireTableChanged(null);       // Signal the table there is new model data

		} // setResultSet


		//----- getColumnCount() method:

		public int getColumnCount()
		{
			return columnNames.length;
		}


		//----- getRowCount() method:

		public int getRowCount()
		{
			if (dataRows == null)
				return 0;
			else
				return dataRows.size();
		}


		//----- getValueAt(row,column) method:

		public Object getValueAt(int row, int column)
		{
			//return ( (String[]) (dataRows.elementAt(row)) )[column];
			return ( (Object[]) (dataRows.elementAt(row)) )[column];
		}


		//----- getColumnName(column) method:

		public String getColumnName(int column)
		{
			return columnNames[column] == null ? "No Name" : columnNames[column];
		}


		//----- isCellEditable(row,col) method:

		public boolean isCellEditable(int row, int col)
		{
			Object[] CourseRowData = new Object[columns];     // Stores one row
			CourseRowData = ( (Object[]) (dataRows.elementAt(row)) );

			// Set aside previous cell value for later use:
			PreviousCellValue = CourseRowData[0].toString();


			// Determine whether or not this row is a split link component:
			boolean IsSplitLinkComponent = false;
			String LinkDescription = CourseRowData[3].toString();
			String LinkDescriptionCourse = "";
			if (LinkDescription.length() >= 17) {
				String LinkDescriptionPrefix = LinkDescription.substring(0,10);
				if ( LinkDescriptionPrefix.equals("Split from") ) {
					IsSplitLinkComponent = true;
					LinkDescriptionCourse = LinkDescription.substring(11,17);
				}   // if ( LinkDescriptionPrefix.equals("Split from") )
			}   // if (LinkDescription.length() >= 17)

			// If split link component, determine whether or not parent was selected:
			boolean SplitLinkParentSelected = false;
			if (IsSplitLinkComponent) {
				if ( SelectedCoursesList.indexOf(LinkDescriptionCourse) >= 0 )
					SplitLinkParentSelected = true;
				if ( SelectedCoursesAltList.indexOf(LinkDescriptionCourse) >= 0 )
					SplitLinkParentSelected = true;
			}   // if (IsSplitLinkComponent)

			// Don't allow selection if this row is a split link component and
			// its parent was selected:
			boolean SelectionAllowed = true;
			if ( (IsSplitLinkComponent) && (SplitLinkParentSelected) )
				SelectionAllowed = false;


			// Don't allow selection if this row is a split link parent and its
			// child was selected:
			if (LinkDescription.equals("Yes - Split") ) {
				String ParentCourse = CourseRowData[1].toString();

				// Count how many components of this parent link course there are
				// that are still in the SelectedCoursesList or the SelectedCoursesAltList:
				int StillSelectedCount = 0;
				String DependentCoursesString = "";
				String DCLLinkCourse = "";
				String DCLExpCourse = "";

				for (int z=0; z < DependentCoursesList.size(); z++) {
					DependentCoursesString=DependentCoursesList.get(z).toString();
					// Reminder: substring(a,b) -- a= start position, b= end position +1
					DCLLinkCourse=DependentCoursesString.substring(0,6);
					DCLExpCourse=DependentCoursesString.substring(6,12);

					if ( ParentCourse.equals(DCLLinkCourse) ) {
						if ( SelectedCoursesList.indexOf(DCLExpCourse) >= 0 )
							StillSelectedCount = StillSelectedCount + 1;
						if ( SelectedCoursesAltList.indexOf(DCLExpCourse) >= 0 )
							StillSelectedCount = StillSelectedCount + 1;
					}   // if ( ParentCourse.equals(DCLLinkCourse) )
				}   // for (z=0; z < DependentCoursesList.size(); z++)

				if ( StillSelectedCount > 0 )
					SelectionAllowed=false;


			}   // if (LinkDescription.equals("Yes - Split")


			SelectorComboBox.removeAllItems();

			if (SelectionAllowed) {
				SelectorComboBox.addItem("Select");
				SelectorComboBox.addItem("Select Alt");
				if ( CourseRowData[3].toString().equals("Yes") )
					SelectorComboBox.addItem("Split Link");
				SelectorComboBox.addItem("(not selected)");
			}   // if (SelectionAllowed)
			else
				SelectorComboBox.addItem("(not allowed)");




			if (
					( col == 0 ) &&
					( !(DeptComboBox.isPopupVisible() ) )
					)
				return true;
			else
				return false;

		}


		//----- getColumnClass(col) method:

		/* Note from Java Tutorial:
		 *
		 * JTable uses this method to determine the default renderer/
		 * editor for each cell.  If we didn't implement this method,
		 * then the last column would contain text ("true"/"false"),
		 * rather than a check box.
		 */

		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}


		//----- setValueAt(value,row,col) method:

		public void setValueAt(Object value, int row, int col) {

			// Update table cell with cell editor's value:
			Object[] CourseRowData = new Object[columns];     // Stores one row
			CourseRowData = ( (Object[]) (dataRows.elementAt(row)) );
			String SelectorValue = "";
			if ( !(value == null) )
				SelectorValue = value.toString();
			String Course = CourseRowData[1].toString();
			//

			boolean IsSplitLinkComponent = false;
			String LinkDescription = CourseRowData[3].toString();
			String LinkDescriptionCourse = "";
			if (LinkDescription.length() >= 17) {
				String LinkDescriptionPrefix = LinkDescription.substring(0,10);
				if ( LinkDescriptionPrefix.equals("Split from") ) {
					LinkDescriptionCourse = LinkDescription.substring(11,17);
					IsSplitLinkComponent = true;
				}   // if ( LinkDescriptionPrefix.equals("Split from") )
			}   // if (LinkDescription.length() >= 17)

			// Whenever a split link component is chosen, store the parent link
			// in the SequencedSelectionsList.  Set aside SequencedCourse here.
			String SequencedCourse = Course;
			if (IsSplitLinkComponent)
				SequencedCourse = LinkDescriptionCourse;

			//

			if ( SelectorValue.equals("(not selected)") )
				value = "";

			if ( SelectorValue.equals("(not allowed)") )
				value = "";

			// Perform the action selected in the table cell:

			// Process a "Split Link" action:
			if ( SelectorValue.equals("Split Link") ) {
				if ( SplitLinksList.indexOf(Course) < 0 )
					SplitLinksList.addElement(Course);
				CourseRowData[3] = "Yes - Split";
				SelectorComboBox.removeAllItems();
				SelectorComboBox.addItem("Select");
				SelectorComboBox.addItem("Select Alt");
				SelectorComboBox.addItem("(not selected)");
				value = PreviousCellValue;
				SetCourseGridValues();
				SemesterNotePanel.setVisible(true);
			}   // if ( SelectorValue.equals("Split Link")

			else

			{


				// Update the "selected" string lists:

				// First delete entries from wherever found in "selected" lists:
				if ( SelectedCoursesList.indexOf(Course) >=0 )
					SelectedCoursesList.removeElementAt(SelectedCoursesList.indexOf(Course));

				if ( SelectedCoursesAltList.indexOf(Course) >=0 )
					SelectedCoursesAltList.removeElementAt(SelectedCoursesAltList.indexOf(Course));


				// If de-selecting the course, remove it from the SequencedSelectionsList, if it exists:
				if ( SelectorValue.equals("(not selected)") )
					if ( SequencedSelectionsList.indexOf(SequencedCourse) >= 0 )

						// If dealing with a split link component, only remove from SequencedSelectionsList
						// if it is the only link component still selected:

						if (IsSplitLinkComponent) {

							// Count how many components of this parent link course there are
							// that are still in the SelectedCoursesList or the SelectedCoursesAltList:
							int StillSelectedCount = 0;
							String DependentCoursesString = "";
							String DCLLinkCourse = "";
							String DCLExpCourse = "";

							for (int z=0; z < DependentCoursesList.size(); z++) {
								DependentCoursesString=DependentCoursesList.get(z).toString();
								// Reminder: substring(a,b) -- a= start position, b= end position +1
								DCLLinkCourse=DependentCoursesString.substring(0,6);
								DCLExpCourse=DependentCoursesString.substring(6,12);

								if ( SequencedCourse.equals(DCLLinkCourse) ) {
									if ( SelectedCoursesList.indexOf(DCLExpCourse) >= 0 )
										StillSelectedCount = StillSelectedCount + 1;
									if ( SelectedCoursesAltList.indexOf(DCLExpCourse) >= 0 )
										StillSelectedCount = StillSelectedCount + 1;
								}   // if ( SequencedCourse.equals(DCLLinkCourse) )

							}   // for (z=0; z < DependentCoursesList.size(); z++)

							if ( StillSelectedCount == 0 )
								SequencedSelectionsList.removeElementAt(SequencedSelectionsList.indexOf(SequencedCourse));

						}   // if (IsSplitLinkComponent)

						else

							SequencedSelectionsList.removeElementAt(SequencedSelectionsList.indexOf(SequencedCourse));


				// Then add entry to the appropriate list:
				if ( SelectorValue.equals("Select") )
					SelectedCoursesList.addElement(Course);

				if ( SelectorValue.equals("Select Alt") )
					SelectedCoursesAltList.addElement(Course);


				// If the course isn't already in the SequencedSelectionsList, then add it:
				if (
						( SelectorValue.equals("Select")     )
						||
						( SelectorValue.equals("Select Alt") )
						)
					if ( SequencedSelectionsList.indexOf(SequencedCourse) < 0 )
						SequencedSelectionsList.addElement(SequencedCourse);


				// Now re-write the "Selected Courses" grid:
				SetSelectedCoursesGridValues();

			}   // if SelectorValue.equals("Split Link")

			CourseRowData[col] = value;
			dataRows.setElementAt(CourseRowData,row);
			fireTableCellUpdated(row, col);

		}   // end of setValueAt method


	} // class CourseTableModel

	//--------------------------------------------------------------------------

	//Definition of Selected Courses Table Model (table in lower left of window):

	class SelectedCoursesTableModel extends AbstractTableModel
	{
		int columns = 8;                      // Get number of columns
		String[] columnNames = { "Group/dept" ,
				"Course#" ,
				"Title" ,
				"Type" ,
				"Trans. Credit" ,
				"Link" ,
				"Exp Course#" ,
		"Exploded Title" };
		Vector dataRows = new Vector();


		//----- setResultSet() method:

		public void setResultSet()
		{

			// Hide the Move Up/Down buttons:
			MoveUpButton.setVisible(false);
			MoveDownButton.setVisible(false);

			// Also reset selected course:
			GlobalCurrentCourse = "";

			// load rows one after another
			dataRows.removeAllElements();                // stores all rows
			Object[] CourseRowData = new Object[columns];     // Stores one row


			// Define work fields:
			String MasterCourseString = "";
			String MCLDept = "";
			String MCLCourse = "";
			String MCLCourseTitle = "";
			String MCLCredit = "";
			String DependentCoursesString = "";
			String DCLLinkCourse = "";
			String DCLExpCourse = "";
			String DCLExpCourseTitle = "";
			String DCLCredit = "";
			String GroupDept = "";
			String Course = "";
			String CourseTitle = "";
			String RequestType = "";
			String Credit = "";
			String Link = "";
			String ExplodedCourse = "";
			String ExplodedCourseTitle = "";
			String DependentCourse = "";
			String DependentCourseTitle = "";
			int x;
			int y;
			int z;
			String SelectionFound = "";
			String DependentSelectionFound = "";
			int TotalSelectionsCount;
			int TotalSelectionsAltCount;
			float TotalTranscriptCreditsCount;
			boolean LinkIsSplit;


			TotalSelectionsCount = 0;
			TotalSelectionsAltCount = 0;
			TotalTranscriptCreditsCount = 0;

			y = 0;

			// Loop through SequencedSelectionsList
			for ( x = 0; x < SequencedSelectionsList.size(); x++ )
				if ( ( BriefMasterCourseList.indexOf(SequencedSelectionsList.get(x).toString()) ) >= 0 ) {
					MasterCourseString = MasterCourseList.get(( BriefMasterCourseList.indexOf(SequencedSelectionsList.get(x).toString()) )).toString();

					/*
// Loop through MasterCourseList
for (x=1; x <= MasterCourseList.size(); x++) {
MasterCourseString=MasterCourseList.get(x-1).toString();
					 */
					// Reminder: substring(a,b) -- a= start position, b= end position +1
					MCLDept=MasterCourseString.substring(0,3);
					MCLCourse=MasterCourseString.substring(3,9);
					MCLCourseTitle=MasterCourseString.substring(9,21);
					MCLCredit=MasterCourseString.substring(21,27);
					GroupDept=MCLDept;
					if ( GroupDept.equals("001") )
						GroupDept = "001. Math";
					if ( GroupDept.equals("002") )
						GroupDept = "002. English";
					if ( GroupDept.equals("003") )
						GroupDept = "003. Science";
					if ( GroupDept.equals("004") )
						GroupDept = "004. History";
					if ( GroupDept.equals("005") )
						GroupDept = "005. Electives";
					if ( GroupDept.equals("006") )
						GroupDept = "006. Team";

					Course=MCLCourse;
					CourseTitle=MCLCourseTitle;

					// See if course found in "selected courses" lists:
					SelectionFound="";

					if ( SelectedCoursesList.indexOf(Course) >=0)
						SelectionFound="Select";

					if ( SelectedCoursesAltList.indexOf(Course) >=0)
						SelectionFound="Select Alt";


					LinkIsSplit=false;

					if ( LinkCoursesList.indexOf(Course) >=0) {
						Link="Yes";
						if ( SplitLinksList.indexOf(Course) >=0 )
							LinkIsSplit=true;
					}   // if ( LinkCoursesList.indexOf(Course) >=0)
					else
						Link="No ";


					if ( (!(SelectionFound.equals(""))) ) {

						// Determine Request Type:
						if (SelectionFound.indexOf("Alt") >= 0)
							RequestType="Alternate";
						else
							if (GroupDept.equals("5. Electives"))
								RequestType="Elective";
							else
								RequestType="Required";

						// Add to grid when Link = "Yes":
						if (Link.equals("Yes")) {

							for (z=1; z <= DependentCoursesList.size(); z++) {
								DependentCoursesString=DependentCoursesList.get(z-1).toString();
								// Reminder: substring(a,b) -- a= start position, b= end position +1
								DCLLinkCourse=DependentCoursesString.substring(0,6);
								DCLExpCourse=DependentCoursesString.substring(6,12);
								DCLExpCourseTitle=DependentCoursesString.substring(12,24);
								DCLCredit=DependentCoursesString.substring(24,30);

								if ( Course.equals(DCLLinkCourse) ) {
									ExplodedCourse=DCLExpCourse;
									ExplodedCourseTitle=DCLExpCourseTitle;
									y=y+1;
									CourseRowData[0]=GroupDept;
									CourseRowData[1]=Course;
									if (GroupDept.equals("006. Team"))
										CourseRowData[2]="TEAM CHOICE";
									else
										CourseRowData[2]=CourseTitle;
									CourseRowData[3]=RequestType;
									Credit=DCLCredit;
									CourseRowData[4]=Credit;
									CourseRowData[5]=Link;
									CourseRowData[6]=ExplodedCourse;
									CourseRowData[7]=ExplodedCourseTitle;

									dataRows.addElement(CourseRowData.clone()); // store the rows in the vector


									// Accumulate counts and credit for "link = yes":
									if ( SelectionFound.equals("Select") ) {
										TotalSelectionsCount=TotalSelectionsCount+1;
										TotalTranscriptCreditsCount=TotalTranscriptCreditsCount +
												Float.parseFloat(Credit);
									}   // if ( SelectionFound.equals("Select") )
									else
										TotalSelectionsAltCount=TotalSelectionsAltCount+1;

								}   // if Course.equals(DCLLinkCourse)
							}   // for (z=1; z <= DependentCoursesList.size(); ++ )

						}   // if (Link.equals("Yes"))

						else   // if (Link.equals("Yes"))

						{
							y=y+1;
							CourseRowData[0]=GroupDept;
							CourseRowData[1]=Course;
							CourseRowData[2]=CourseTitle;
							CourseRowData[3]=RequestType;
							Credit=MCLCredit;
							CourseRowData[4]=Credit;
							CourseRowData[5]=Link;
							CourseRowData[6]="";   // exploded course
							CourseRowData[7]="";   // exploded crs title

							dataRows.addElement(CourseRowData.clone()); // store the rows in the vector

							// Accumulate counts and credit for "link = no":
							if ( SelectionFound.equals("Select") ) {
								TotalSelectionsCount=TotalSelectionsCount+1;
								TotalTranscriptCreditsCount=TotalTranscriptCreditsCount +
										Float.parseFloat(Credit);
							}   // if ( SelectionFound.equals("Select") )
							else
								TotalSelectionsAltCount=TotalSelectionsAltCount+1;

						}   // if (Link.equals("Yes"))

					}   // if !(SelectionFound.equals("") )



					// If a linked course is split, whether selected or not, read
					// through dependent courses to see if any are selected:
					if (LinkIsSplit) {
						for (z=1; z <= DependentCoursesList.size(); z++) {
							DependentCoursesString=DependentCoursesList.get(z-1).toString();
							DCLLinkCourse=DependentCoursesString.substring(0,6);
							DCLExpCourse=DependentCoursesString.substring(6,12);
							DCLExpCourseTitle=DependentCoursesString.substring(12,24);
							DCLCredit=DependentCoursesString.substring(24,30);
							if ( Course.equals(DCLLinkCourse) ) {
								DependentCourse=DCLExpCourse;
								DependentCourseTitle=DCLExpCourseTitle;

								// See if course found in "selected courses" lists:
								DependentSelectionFound="";

								if ( SelectedCoursesList.indexOf(DependentCourse) >= 0)
									DependentSelectionFound="Select";

								if ( SelectedCoursesAltList.indexOf(DependentCourse) >= 0)
									DependentSelectionFound="Select Alt";


								if ( !(DependentSelectionFound.equals("")) ) {

									// Determine Request Type:
									if ( DependentSelectionFound.indexOf("Alt")  >= 0)
										RequestType="Alternate";
									else
										if ( GroupDept.equals("5. Electives") )
											RequestType="Elective";
										else
											RequestType="Required";

									y=y+1;
									CourseRowData[0]=GroupDept;
									CourseRowData[1]=DependentCourse;
									CourseRowData[2]=DependentCourseTitle;
									CourseRowData[3]=RequestType;
									Credit=DCLCredit;
									CourseRowData[4]=Credit;
									CourseRowData[5]="No";
									CourseRowData[6]="";   // exploded course
									CourseRowData[7]="";   // exploded crs title
									dataRows.addElement(CourseRowData.clone()); // store the rows in the vector

									// Accumulate counts and credit for dependents of split link:
									if ( DependentSelectionFound.equals("Select") ) {
										TotalSelectionsCount=TotalSelectionsCount+1;
										TotalTranscriptCreditsCount=TotalTranscriptCreditsCount +
												Float.parseFloat(Credit);
									}   // if ( DependentSelectionFound.equals("Select") )
									else
										TotalSelectionsAltCount=TotalSelectionsAltCount+1;

								}   // if !(DependentSelectionFound.equals("") )
							}   // if Course.equals(DCLLinkCourse)
						}   // for (z=1; z <= DependentCoursesList.size(); z++ )

					}   // if LinkIsSplit

				}   // for ( y = 0; y < SequencedSelectionsList.size(); y++ ) {

			// Update "totals" labels at bottom of panel:
			TotalSelectionsCountLabel.setText("" + TotalSelectionsCount);
			TotalSelectionsAltCountLabel.setText("" + TotalSelectionsAltCount);
			TotalTranscriptCreditsLabel.setText("" + TotalTranscriptCreditsCount);

			fireTableChanged(null);       // Signal the table there is new model data

		} // setResultSet


		//----- getColumnCount() method:

		public int getColumnCount()
		{
			return columnNames.length;
		}


		//----- getRowCount() method:

		public int getRowCount()
		{
			if (dataRows == null)
				return 0;
			else
				return dataRows.size();
		}


		//----- getValueAt(row,column) method:

		public Object getValueAt(int row, int column)
		{
			//return ( (String[]) (dataRows.elementAt(row)) )[column];
			return ( (Object[]) (dataRows.elementAt(row)) )[column];
		}


		//----- getColumnName(column) method:

		public String getColumnName(int column)
		{
			return columnNames[column] == null ? "No Name" : columnNames[column];
		}


		//----- isCellEditable(row,col) method:

		public boolean isCellEditable(int row, int col)
		{
			return false;
		}


	} // class SelectedCoursesTableModel

	//--------------------------------------------------------------------------

	//Definition of Transcript Credits Table Model:

	class CreditsTableModel extends AbstractTableModel
	{
		int columns = 12;                      // Get number of columns
		String[] columnNames = { "Year" ,
				"Grade" ,
				"Term" ,
				"Course#" ,
				"Course Title" ,
				"Credits Earned " ,
				"Year" ,
				"Grade" ,
				"Term" ,
				"Course#" ,
				"Course Title" ,
		"Credits Earned " };
		Vector dataRows = new Vector();


		//----- setResultSet() method:

		public void setResultSet()
		{
			// load rows one after another
			dataRows.removeAllElements();                // stores all rows
			Object[] CourseRowData = new Object[columns];     // Stores one row

			// Set sample row values for credits grid:
			for (int x=1; x <=15; x++) {

				CourseRowData[6] = "";
				CourseRowData[7] = "";
				CourseRowData[8] = "";
				CourseRowData[9] = "";
				CourseRowData[10] = "";
				CourseRowData[11] = "";

				switch(x)
				{

				case 1:
					CourseRowData[0] = "2000-01";
					CourseRowData[1] = "09";
					CourseRowData[2] = "1";
					CourseRowData[3] = "ENG10A";
					CourseRowData[4] = "Freshmen Eng 1";
					CourseRowData[5] = "0.50";

					CourseRowData[6] = "2002-03";
					CourseRowData[7] = "11";
					CourseRowData[8] = "1";
					CourseRowData[9] = "ART110";
					CourseRowData[10] = "Art I";
					CourseRowData[11] = "0.25";
					break;

				case 2:
					CourseRowData[0] = "2000-01";
					CourseRowData[1] = "09";
					CourseRowData[2] = "1";
					CourseRowData[3] = "MTH10A";
					CourseRowData[4] = "Freshmen Math 1";
					CourseRowData[5] = "0.50";

					CourseRowData[6] = "2002-03";
					CourseRowData[7] = "11";
					CourseRowData[8] = "1";
					CourseRowData[9] = "PHO115";
					CourseRowData[10] = "Photography";
					CourseRowData[11] = "0.25";
					break;

				case 3:
					CourseRowData[0] = "2000-01";
					CourseRowData[1] = "09";
					CourseRowData[2] = "1";
					CourseRowData[3] = "SCI10A";
					CourseRowData[4] = "Freshmen Sci 1";
					CourseRowData[5] = "0.50";
					break;

				case 4:
					CourseRowData[0] = "2000-01";
					CourseRowData[1] = "09";
					CourseRowData[2] = "2";
					CourseRowData[3] = "ENG10B";
					CourseRowData[4] = "Freshmen Eng 2";
					CourseRowData[5] = "0.50";
					break;

				case 5:
					CourseRowData[0] = "2000-01";
					CourseRowData[1] = "09";
					CourseRowData[2] = "2";
					CourseRowData[3] = "MTH10B";
					CourseRowData[4] = "Freshmen Math 2";
					CourseRowData[5] = "0.50";
					break;

				case 6:
					CourseRowData[0] = "2000-01";
					CourseRowData[1] = "09";
					CourseRowData[2] = "2";
					CourseRowData[3] = "SCI10B";
					CourseRowData[4] = "Freshmen Sci 2";
					CourseRowData[5] = "0.50";
					break;

				case 7:
					CourseRowData[0] = "2001-02";
					CourseRowData[1] = "10";
					CourseRowData[2] = "1";
					CourseRowData[3] = "ENG20A";
					CourseRowData[4] = "Soph Eng 1";
					CourseRowData[5] = "0.50";
					break;

				case 8:
					CourseRowData[0] = "2001-02";
					CourseRowData[1] = "10";
					CourseRowData[2] = "1";
					CourseRowData[3] = "MTH20A";
					CourseRowData[4] = "Soph Math 1";
					CourseRowData[5] = "0.50";
					break;

				case 9:
					CourseRowData[0] = "2001-02";
					CourseRowData[1] = "10";
					CourseRowData[2] = "1";
					CourseRowData[3] = "SCI20A";
					CourseRowData[4] = "Soph Sci 1";
					CourseRowData[5] = "0.50";
					break;

				case 10:
					CourseRowData[0] = "2001-02";
					CourseRowData[1] = "10";
					CourseRowData[2] = "2";
					CourseRowData[3] = "ENG20B";
					CourseRowData[4] = "Soph Eng 2";
					CourseRowData[5] = "0.50";
					break;

				case 11:
					CourseRowData[0] = "2001-02";
					CourseRowData[1] = "10";
					CourseRowData[2] = "2";
					CourseRowData[3] = "MTH20B";
					CourseRowData[4] = "Soph Math 2";
					CourseRowData[5] = "0.50";
					break;

				case 12:
					CourseRowData[0] = "2001-02";
					CourseRowData[1] = "10";
					CourseRowData[2] = "2";
					CourseRowData[3] = "SCI20B";
					CourseRowData[4] = "Soph Sci 2";
					CourseRowData[5] = "0.50";
					break;

				case 13:
					CourseRowData[0] = "2002-03";
					CourseRowData[1] = "11";
					CourseRowData[2] = "1";
					CourseRowData[3] = "ENG30A";
					CourseRowData[4] = "Junior Eng 1";
					CourseRowData[5] = "0.50";
					break;

				case 14:
					CourseRowData[0] = "2002-03";
					CourseRowData[1] = "11";
					CourseRowData[2] = "1";
					CourseRowData[3] = "MTH30A";
					CourseRowData[4] = "Junior Math 1";
					CourseRowData[5] = "0.50";
					break;

				case 15:
					CourseRowData[0] = "2002-03";
					CourseRowData[1] = "11";
					CourseRowData[2] = "1";
					CourseRowData[3] = "SCI30A";
					CourseRowData[4] = "Junior Sci 1";
					CourseRowData[5] = "0.50";
					break;

				}   // switch(x)

				dataRows.addElement(CourseRowData.clone()); // store the rows in the vector

			}   // for (x=1; x <=15; x++)

			// Update "totals" labels at bottom of panel:
			TotalCreditsLabel.setText("8.00");


			fireTableChanged(null);       // Signal the table there is new model data

		} // setResultSet


		//----- getColumnCount() method:

		public int getColumnCount()
		{
			return columnNames.length;
		}


		//----- getRowCount() method:

		public int getRowCount()
		{
			if (dataRows == null)
				return 0;
			else
				return dataRows.size();
		}


		//----- getValueAt(row,column) method:

		public Object getValueAt(int row, int column)
		{
			//return ( (String[]) (dataRows.elementAt(row)) )[column];
			return ( (Object[]) (dataRows.elementAt(row)) )[column];
		}


		//----- getColumnName(column) method:

		public String getColumnName(int column)
		{
			return columnNames[column] == null ? "No Name" : columnNames[column];
		}


		//----- isCellEditable(row,col) method:

		public boolean isCellEditable(int row, int col)
		{
			return false;
		}


	} // class SelectedCoursesTableModel


	//--------------------------------------------------------------------------

	public void SetCourseTableColumnWidths() {

		CourseTable.getColumnModel().getColumn(0).setMinWidth(80);
		CourseTable.getColumnModel().getColumn(1).setMinWidth(50);
		CourseTable.getColumnModel().getColumn(2).setMinWidth(96);
		CourseTable.getColumnModel().getColumn(3).setMinWidth(90);
		CourseTable.getColumnModel().getColumn(4).setMinWidth(26);
		CourseTable.getColumnModel().getColumn(5).setMinWidth(31);
		CourseTable.getColumnModel().getColumn(6).setMinWidth(26);
		CourseTable.getColumnModel().getColumn(7).setMinWidth(26);

		CourseTable.getColumnModel().getColumn(0).setMaxWidth(80);
		CourseTable.getColumnModel().getColumn(1).setMaxWidth(50);
		CourseTable.getColumnModel().getColumn(2).setMaxWidth(96);
		CourseTable.getColumnModel().getColumn(3).setMaxWidth(90);
		CourseTable.getColumnModel().getColumn(4).setMaxWidth(26);
		CourseTable.getColumnModel().getColumn(5).setMaxWidth(31);
		CourseTable.getColumnModel().getColumn(6).setMaxWidth(26);
		CourseTable.getColumnModel().getColumn(7).setMaxWidth(26);

		CourseTable.getColumnModel().getColumn(0).setPreferredWidth(80);
		CourseTable.getColumnModel().getColumn(1).setPreferredWidth(50);
		CourseTable.getColumnModel().getColumn(2).setPreferredWidth(96);
		CourseTable.getColumnModel().getColumn(3).setPreferredWidth(90);
		CourseTable.getColumnModel().getColumn(4).setPreferredWidth(26);
		CourseTable.getColumnModel().getColumn(5).setPreferredWidth(31);
		CourseTable.getColumnModel().getColumn(6).setPreferredWidth(26);
		CourseTable.getColumnModel().getColumn(7).setPreferredWidth(26);

	}

	//--------------------------------------------------------------------------

	public void SetSelectedCoursesTableColumnWidths() {

		SelectedCoursesTable.getColumnModel().getColumn(0).setMinWidth(102);   // group/dept
		SelectedCoursesTable.getColumnModel().getColumn(1).setMinWidth(49);   // course#
		SelectedCoursesTable.getColumnModel().getColumn(2).setMinWidth(96);   // course title
		SelectedCoursesTable.getColumnModel().getColumn(3).setMinWidth(60);   // type of request
		SelectedCoursesTable.getColumnModel().getColumn(4).setMinWidth(75);   // transcript credit
		SelectedCoursesTable.getColumnModel().getColumn(5).setMinWidth(30);   // link
		SelectedCoursesTable.getColumnModel().getColumn(6).setMinWidth(76);   // exploded course#
		SelectedCoursesTable.getColumnModel().getColumn(7).setMinWidth(96);   // exploded coure title

		SelectedCoursesTable.getColumnModel().getColumn(0).setMaxWidth(102);
		SelectedCoursesTable.getColumnModel().getColumn(1).setMaxWidth(49);
		SelectedCoursesTable.getColumnModel().getColumn(2).setMaxWidth(96);
		SelectedCoursesTable.getColumnModel().getColumn(3).setMaxWidth(60);
		SelectedCoursesTable.getColumnModel().getColumn(4).setMaxWidth(75);
		SelectedCoursesTable.getColumnModel().getColumn(5).setMaxWidth(30);
		SelectedCoursesTable.getColumnModel().getColumn(6).setMaxWidth(76);
		SelectedCoursesTable.getColumnModel().getColumn(7).setMaxWidth(96);

		SelectedCoursesTable.getColumnModel().getColumn(0).setPreferredWidth(102);
		SelectedCoursesTable.getColumnModel().getColumn(1).setPreferredWidth(49);
		SelectedCoursesTable.getColumnModel().getColumn(2).setPreferredWidth(96);
		SelectedCoursesTable.getColumnModel().getColumn(3).setPreferredWidth(60);
		SelectedCoursesTable.getColumnModel().getColumn(4).setPreferredWidth(75);
		SelectedCoursesTable.getColumnModel().getColumn(5).setPreferredWidth(30);
		SelectedCoursesTable.getColumnModel().getColumn(6).setPreferredWidth(76);
		SelectedCoursesTable.getColumnModel().getColumn(7).setPreferredWidth(96);

	}

	//--------------------------------------------------------------------------

	public void SetCreditsTableColumnWidths() {

		CreditsTable.getColumnModel().getColumn(0).setMinWidth(50);   // Year
		CreditsTable.getColumnModel().getColumn(1).setMinWidth(40);   // Grade
		CreditsTable.getColumnModel().getColumn(2).setMinWidth(35);   // Term
		CreditsTable.getColumnModel().getColumn(3).setMinWidth(60);   // Course#
		CreditsTable.getColumnModel().getColumn(4).setMinWidth(100);   // Course Title
		CreditsTable.getColumnModel().getColumn(5).setMinWidth(86);   // Credits Earned
		CreditsTable.getColumnModel().getColumn(6).setMinWidth(50);   // Year
		CreditsTable.getColumnModel().getColumn(7).setMinWidth(40);   // Grade
		CreditsTable.getColumnModel().getColumn(8).setMinWidth(35);   // Term
		CreditsTable.getColumnModel().getColumn(9).setMinWidth(60);   // Course#
		CreditsTable.getColumnModel().getColumn(10).setMinWidth(100);   // Course Title
		CreditsTable.getColumnModel().getColumn(11).setMinWidth(86);   // Credits Earned

		CreditsTable.getColumnModel().getColumn(0).setMaxWidth(50);   // Year
		CreditsTable.getColumnModel().getColumn(1).setMaxWidth(40);   // Grade
		CreditsTable.getColumnModel().getColumn(2).setMaxWidth(35);   // Term
		CreditsTable.getColumnModel().getColumn(3).setMaxWidth(60);   // Course#
		CreditsTable.getColumnModel().getColumn(4).setMaxWidth(100);   // Course Title
		CreditsTable.getColumnModel().getColumn(5).setMaxWidth(86);   // Credits Earned
		CreditsTable.getColumnModel().getColumn(6).setMaxWidth(50);   // Year
		CreditsTable.getColumnModel().getColumn(7).setMaxWidth(40);   // Grade
		CreditsTable.getColumnModel().getColumn(8).setMaxWidth(35);   // Term
		CreditsTable.getColumnModel().getColumn(9).setMaxWidth(60);   // Course#
		CreditsTable.getColumnModel().getColumn(10).setMaxWidth(100);   // Course Title
		CreditsTable.getColumnModel().getColumn(11).setMaxWidth(86);   // Credits Earned

		CreditsTable.getColumnModel().getColumn(0).setPreferredWidth(50);   // Year
		CreditsTable.getColumnModel().getColumn(1).setPreferredWidth(40);   // Grade
		CreditsTable.getColumnModel().getColumn(2).setPreferredWidth(35);   // Term
		CreditsTable.getColumnModel().getColumn(3).setPreferredWidth(60);   // Course#
		CreditsTable.getColumnModel().getColumn(4).setPreferredWidth(100);   // Course Title
		CreditsTable.getColumnModel().getColumn(5).setPreferredWidth(86);   // Credits Earned
		CreditsTable.getColumnModel().getColumn(6).setPreferredWidth(50);   // Year
		CreditsTable.getColumnModel().getColumn(7).setPreferredWidth(40);   // Grade
		CreditsTable.getColumnModel().getColumn(8).setPreferredWidth(35);   // Term
		CreditsTable.getColumnModel().getColumn(9).setPreferredWidth(60);   // Course#
		CreditsTable.getColumnModel().getColumn(10).setPreferredWidth(100);   // Course Title
		CreditsTable.getColumnModel().getColumn(11).setPreferredWidth(86);   // Credits Earned

	}


	void DeptComboBox_itemStateChanged(ItemEvent e) {
		// Don't load course grid when initially loading component; only when user changes:
		if (!(InitiallyLoading))
			SetCourseGridValues();
	}

	void EngComboBox_itemStateChanged(ItemEvent e) {
		// Don't load course grid when initially loading component; only when user changes:
		if (!(InitiallyLoading))
			SetCourseGridValues();

	}

	void MathComboBox_itemStateChanged(ItemEvent e) {
		// Don't load course grid when initially loading component; only when user changes:
		if (!(InitiallyLoading))
			SetCourseGridValues();

	}

	void SocComboBox_itemStateChanged(ItemEvent e) {
		// Don't load course grid when initially loading component; only when user changes:
		if (!(InitiallyLoading))
			SetCourseGridValues();

	}

	void SciComboBox_itemStateChanged(ItemEvent e) {
		// Don't load course grid when initially loading component; only when user changes:
		if (!(InitiallyLoading))
			SetCourseGridValues();

	}

	//------------------------------------------------------------



	void MainTabbedPane_stateChanged(ChangeEvent e) {

		// Following is an attempt at preserving the original "little tab" color
		// after it has been selected.  Without this code, the little tab turns
		// gray.  This tip came from "camickr" at Java Developer Forums.
		int selected = MainTabbedPane.getSelectedIndex();
		UIManager.put("TabbedPane.selected", MainTabbedPane.getBackgroundAt(selected) );
		MainTabbedPane.setUI( MainTabbedPane.getUI() );

	}

	//------------------------------------------------------

	void SetMoveButtonVisibility(int CurrentRow) {
		int LastRow = SelectedCoursesTable.getRowCount() - 1;
		String SelectedCourseInTable = MySelectedCoursesTableModel.getValueAt(CurrentRow,1).toString();
		String FirstCourseInTable = MySelectedCoursesTableModel.getValueAt(0,1).toString();
		String LastCourseInTable = MySelectedCoursesTableModel.getValueAt(LastRow,1).toString();

		// Show "Move Down" button (only if not on last row and if last row is a different course):
		if (
				( CurrentRow < LastRow )
				&&
				( !( SelectedCourseInTable.equals(LastCourseInTable) ) )
				)
			MoveDownButton.setVisible(true);
		else
			MoveDownButton.setVisible(false);

		// Show "Move Up" button (only if not on first row and if first row is a different course):
		if (
				( CurrentRow > 0 )
				&&
				( !( SelectedCourseInTable.equals(FirstCourseInTable) ) )
				)
			MoveUpButton.setVisible(true);
		else
			MoveUpButton.setVisible(false);

	}   // void SetMoveButtonVisibility

	//------------------------------------------------------

	void SelectedCoursesTable_mouseReleased(MouseEvent e) {
		Point mousePoint = e.getPoint();
		int CurrentRow = SelectedCoursesTable.rowAtPoint(mousePoint);

		// Save the course# on this selected row (used in "Move Down" / "Move Up" actions):
		GlobalCurrentCourse = MySelectedCoursesTableModel.getValueAt(CurrentRow,1).toString();

		SetMoveButtonVisibility(CurrentRow);

		// find out whether table cell value at column 0 is in dept/group list:
		for (int x=0; x <= DeptComboBox.getItemCount() - 1; x++ ) {
			if ( MySelectedCoursesTableModel.getValueAt(CurrentRow,0).toString().equals(
					DeptComboBox.getItemAt(x).toString()) )  {
				DeptComboBox.setSelectedIndex(x);
				SetCourseGridValues();
				break;
			}   // if ( MySelectedCoursesTableModel.getValueAt(CurrentRow,0).toString().equals( ...
		}  // for (int x=0; int x <= DeptComboBox.getItemCount() - 1; x++ )

	}   // end of SelectedCoursesTable_mouseReleased


	//-------------------------------------------------------------------------

	//Print the selected courses table:

	public int print(Graphics g, PageFormat pageFormat,
			int pageIndex) throws PrinterException {
		Graphics2D  g2 = (Graphics2D) g;
		g2.setColor(Color.black);
		int fontHeight=g2.getFontMetrics().getHeight();
		int myFontDescent=g2.getFontMetrics().getDescent();

		// leave room for footings (2 rows -- fontHeight*2)
		double pageHeight = pageFormat.getImageableHeight()-fontHeight*2;
		double pageWidth = pageFormat.getImageableWidth();
		double tableWidth = (double) SelectedCoursesTable.getColumnModel().getTotalColumnWidth();
		double scale = 1;
		if (tableWidth >= pageWidth) {
			scale =  pageWidth / tableWidth;
		}

		double headerHeightOnPage=
				SelectedCoursesTable.getTableHeader().getHeight()*scale;
		double tableWidthOnPage=tableWidth*scale;

		double oneRowHeight=(SelectedCoursesTable.getRowHeight()+
				SelectedCoursesTable.getRowMargin())*scale;
		int numRowsOnAPage=
				(int)((pageHeight-headerHeightOnPage)/oneRowHeight);
		double pageHeightForTable=oneRowHeight*numRowsOnAPage;
		int totalNumPages= (int)Math.ceil((
				(double)SelectedCoursesTable.getRowCount())/numRowsOnAPage);
		if(pageIndex>=totalNumPages) {
			return NO_SUCH_PAGE;
		}

		g2.translate(pageFormat.getImageableX(),
				pageFormat.getImageableY());
		g2.drawString("Student: " +StudentNameLabel.getText() +
				"    Academy: " + AcademyLabel.getText() +
				"    " + Calendar.getInstance().getTime().toString() ,
				0,
				(int)(pageHeight+fontHeight-myFontDescent));//bottom center
		g2.drawString("Next Yr:  Dist: " + NextYearDistrictLabel.getText() +
				"  Schl: " + NextYearSchoolLabel.getText() +
				"  Class: " + NextYearClassLabel.getText() +
				"  Counselor: " + NextYearCounselorNameLabel.getText() ,
				0,
				(int)(pageHeight+fontHeight*2-myFontDescent));//bottom center

		//This would print page number at bottom center:
		//g2.drawString("Page "+(pageIndex+1),(int)pageWidth/2-35,
		//(int)(pageHeight+fontHeight-myFontDescent));//bottom center

		g2.translate(0f,headerHeightOnPage);
		g2.translate(0f,-pageIndex*pageHeightForTable);

		//If this piece of the table is smaller than the size available,
		//clip to the appropriate bounds.
		if (pageIndex + 1 == totalNumPages) {
			int lastRowPrinted = numRowsOnAPage * pageIndex;
			int numRowsLeft = SelectedCoursesTable.getRowCount() - lastRowPrinted;
			g2.setClip(0, (int)(pageHeightForTable * pageIndex),
					(int) Math.ceil(tableWidthOnPage),
					(int) Math.ceil(oneRowHeight * numRowsLeft));
		}
		//else clip to the entire area available.
		else{
			g2.setClip(0, (int)(pageHeightForTable*pageIndex),
					(int) Math.ceil(tableWidthOnPage),
					(int) Math.ceil(pageHeightForTable));
		}

		g2.scale(scale,scale);
		SelectedCoursesTable.paint(g2);
		g2.scale(1/scale,1/scale);
		g2.translate(0f,pageIndex*pageHeightForTable);
		g2.translate(0f, -headerHeightOnPage);
		g2.setClip(0, 0,(int) Math.ceil(tableWidthOnPage),
				(int)Math.ceil(headerHeightOnPage));
		g2.scale(scale,scale);
		SelectedCoursesTable.getTableHeader().paint(g2);//paint header at top

		return Printable.PAGE_EXISTS;
	}

	void PrintButton_actionPerformed(ActionEvent e) {

		if ( SelectedCoursesTable.getRowCount() == 0 )
			ShowPopupMessage("Print: Course Selections Demo",
					"Nothing to print.  There are no selected courses.");
		else {

			RepaintManager.currentManager(this).setDoubleBufferingEnabled(false);
			PrinterJob pj=PrinterJob.getPrinterJob();
			pj.setPrintable(this);
			pj.printDialog();
			try {
				pj.print();
			}
			catch (Exception PrintException) {
				PrintException.printStackTrace();
			}
			RepaintManager.currentManager(this).setDoubleBufferingEnabled(true);

		}   // else if ( SelectedCoursesTable.getRowCount() == 0 )

	}

	//---------------------------------------------------------------------------

	public void ShowPopupMessage(String frameTitle, String messageText) {

		// Frame for displaying dialog messages:
		JFrame messageFrame = new JFrame(frameTitle);

		messageFrame.pack();
		messageFrame.setSize(new Dimension(20,20));

		// Set location of messageFrame
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		messageFrame.setLocation( (screenSize.width - 200) / 2 ,
				(screenSize.height - 100) / 2);
		messageFrame.setVisible(false);

		// Note: Use "\n" in messageText to do a line break):
		messageFrame.setVisible(true);
		JOptionPane.showMessageDialog(messageFrame,messageText,frameTitle,
				JOptionPane.ERROR_MESSAGE);

		messageFrame.setVisible(false);

	}

	void MoveUpButton_actionPerformed(ActionEvent e) {
		int CurrentCourseIndex = SequencedSelectionsList.indexOf(GlobalCurrentCourse);
		if ( CurrentCourseIndex > 0 ) {
			String CourseToDisplace = SequencedSelectionsList.get(CurrentCourseIndex-1).toString();
			SequencedSelectionsList.set(CurrentCourseIndex-1,GlobalCurrentCourse);
			SequencedSelectionsList.set(CurrentCourseIndex,CourseToDisplace);

			String SaveCourse = GlobalCurrentCourse;
			SetSelectedCoursesGridValues();   // this blanks out GlobalCurrentCourse
			GlobalCurrentCourse = SaveCourse;

			// Re-select the row to which the GlobalCurrentCourse was moved:
			for (int x = 0; x < SelectedCoursesTable.getRowCount(); x++ )
				if ( GlobalCurrentCourse.equals(MySelectedCoursesTableModel.getValueAt(x,1).toString() ) ) {
					SelectedCoursesTable.setRowSelectionInterval(x,x);
					ensureRowVisible(SelectedCoursesTable,x);
					SetMoveButtonVisibility(x);
					break;
				}   // if ( GlobalCurrentCourse.equals(MySelectedCoursesTableModel.getValueAt(x,1).toString() ) )

		}   // if ( CurrentCourseIndex > 0 ) {
	}

	void MoveDownButton_actionPerformed(ActionEvent e) {
		int CurrentCourseIndex = SequencedSelectionsList.indexOf(GlobalCurrentCourse);
		if ( CurrentCourseIndex < SequencedSelectionsList.size() - 1 ) {
			String CourseToDisplace = SequencedSelectionsList.get(CurrentCourseIndex+1).toString();
			SequencedSelectionsList.set(CurrentCourseIndex+1,GlobalCurrentCourse);
			SequencedSelectionsList.set(CurrentCourseIndex,CourseToDisplace);

			String SaveCourse = GlobalCurrentCourse;
			SetSelectedCoursesGridValues();   // this blanks out GlobalCurrentCourse
			GlobalCurrentCourse = SaveCourse;

			// Re-select the row to which the GlobalCurrentCourse was moved:
			for (int x = 0; x < SelectedCoursesTable.getRowCount(); x++ )
				if ( GlobalCurrentCourse.equals(MySelectedCoursesTableModel.getValueAt(x,1).toString() ) ) {
					SelectedCoursesTable.setRowSelectionInterval(x,x);
					ensureRowVisible(SelectedCoursesTable,x);
					SetMoveButtonVisibility(x);
					break;
				}   // if ( GlobalCurrentCourse.equals(MySelectedCoursesTableModel.getValueAt(x,1).toString() ) )

		}   // if ( CurrentCourseIndex > 0 ) {

	}   // end MoveDownButton_actionPerformed method

	//-------------------------------------------------------

	//Following is method from guy named Tiom from Java forum:

	public static void ensureRowVisible(JTable table, int row) {
		if (row == -1 || table == null) {
			return;
		}
		JScrollPane jsp = (JScrollPane) SwingUtilities.getAncestorOfClass(JScrollPane.class, table.getParent());
		JViewport jvp = jsp.getViewport();
		int portHeight = jvp.getSize().height;
		int height = table.getRowHeight();
		int celltop = table.getCellRect(row, 0, true).y;
		int position = celltop - portHeight + table.getRowHeight()+ table.getRowMargin();
		if (position >= 0) {
			jvp.setViewPosition(new Point(0, position));

		}

	} // ensureRowVisible method

	//-------------------------------------------------------


}   // end of CourseSelectionsDemo class