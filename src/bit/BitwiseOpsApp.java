package bit;

import java.util.Random;

public class BitwiseOpsApp {

	public static void main(String[] args) {

		int bitmask_all_ones = Integer.parseInt("11111111", 2);   // 255
		int bitmask_all_zeros = Integer.parseInt("00000000", 2);   // 0
		int bitmask_low_order_ones = Integer.parseInt("00001111", 2);   // 15
		
		
		for (int x = 0; x < 4; x++)
		{
			int number = new Random().nextInt(255) + 1;
			showBitMaskOps(number, bitmask_all_ones);
			showBitMaskOps(number, bitmask_all_zeros);
			showBitMaskOps(number, bitmask_low_order_ones);
		}
	}
	
	
	private static void showBitMaskOps(int numberIn, int bitmask)
	{
		int shift3Var = numberIn >> 3;
		int compInt = ~numberIn;   // Simply inverts 1s and 0s
		
		// Integers in Java are all signed 32-bit.  At this point, compInt will have 11111111 11111111 11111111 xxxxxxxx
		
		// Drop the 3 high order bytes (for display purposes):
		String compStr = Integer.toBinaryString(compInt).substring(24);
		
		/*
		// Or do it mathematically using a long:
		long compLong = (long) compInt & 0x00000000ffffffffL;
		
		// I'm only interested in the low-order byte, so get rid of the high order bytes 11111111 11111111 11111111 00000000:
		
		long highOrderByteValue = 0xFFFFFF00;   // 4294967040
		compLong -= highOrderByteValue;
		*/
		
		
		System.out.println(String.format("%8s (%s) <- numberIn >> 3 (shift)", Integer.toBinaryString(shift3Var), shift3Var));
		System.out.println(String.format("%8s (%s) <- ~ numberIn (complement flip 0s & 1s)",	compStr, Integer.parseInt(compStr, 2)));
		System.out.println(String.format("%8s (%s) <- numberIn",	Integer.toBinaryString(numberIn), numberIn));
		System.out.println(String.format("%8s (%s) <- bitmask",	Integer.toBinaryString(bitmask), bitmask));
		
		System.out.println();
		
		int andVar = numberIn & bitmask;
		int orVar = numberIn | bitmask;
		int xOrVar = numberIn ^ bitmask;
		
		System.out.println(String.format("%8s (%s) <- & (1 if both 1)", Integer.toBinaryString(andVar), andVar));
		System.out.println(String.format("%8s (%s) <- | (1 except if both 0)", Integer.toBinaryString(orVar), orVar));
		System.out.println(String.format("%8s (%s) <- ^ (1 if both different)", Integer.toBinaryString(xOrVar), xOrVar));
		
		System.out.println("\n----------------------------------------\n");
		
	}

}
