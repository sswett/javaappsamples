package sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlayerSortApp {


	public static void main(String[] args) {

		List<Player> list = new ArrayList<Player>();

		list.add(new Player("Steve", 3.44F));
		list.add(new Player("Paul", 3.48F));
		list.add(new Player("Carl", 3.44F));
		list.add(new Player("Jim S", 3.1F));
		list.add(new Player("James", 3.0F));
		list.add(new Player("Mike", 3.3F));

		// Sort without using Comparator; uses Player.compareTo to compare each Player with one another
		Collections.sort(list);
		printList(list);

		// Sort using comparator: second arg is Comparator -- which implements compare(Player,Player) in this case
		Collections.sort(list, new Player());
		printList(list);
		
		// Flip-flop ascending/descending (could have done Collections.reverseOrder with a comparator)
		Collections.reverse(list);
		printList(list);

	}
	
	
	private static void printList(List<Player> list)
	{
		for (Player player: list)
		{
			System.out.print(
					String.format("%s - %02.2f, ", player.getName(), player.getRating() )
					);
		}

		System.out.println(" ");
	}

	
}
