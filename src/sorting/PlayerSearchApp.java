package sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlayerSearchApp {

	public static void main(String[] args) {

		List<Player> list = new ArrayList<Player>();
		
		Player steve = new Player("Steve", 3.44F);
		Player paul = new Player("Paul", 3.48F);
		Player carl = new Player("Carl", 3.44F);
		Player jim = new Player("Jim S", 3.1F);
		Player james = new Player("James", 3.0F);
		Player mike = new Player("Mike", 3.3F);

		list.add(steve);
		list.add(paul);
		list.add(carl);
		list.add(jim);
		list.add(james);
		list.add(mike);

		Collections.sort(list);
		printList(list);
		int foundAt = Collections.binarySearch(list, carl);
		printSearchResults("binary search without comparator", list, foundAt);
		
		// Now search using an explicit comparator (e.g., sort by rating)

		Player comparator = new Player();
		Collections.sort(list, comparator);
		printList(list);
		foundAt = Collections.binarySearch(list, carl, comparator);
		printSearchResults("binary search with comparator (rating)", list, foundAt);
	}
	
	
	private static void printList(List<Player> list)
	{
		for (Player player: list)
		{
			System.out.print(
					String.format("%s - %02.2f, ", player.getName(), player.getRating() )
					);
		}

		System.out.println(" ");
	}
	
	
	private static void printSearchResults(String searchDescription, List<Player> list, int foundAt)
	{
		System.out.println("\nResults for " + searchDescription);
		
		if (foundAt < 0)
		{
			System.out.println("Object \"carl\" was not found.");
		}
		else
		{
			System.out.println("Object \"carl\" was found at position " + foundAt + " of " + (list.size() - 1) );
		}

		System.out.println(" ");
	}
	

}
