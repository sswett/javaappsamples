package sorting;

import java.util.Comparator;

// Note: Comparable: must implement compareTo; Comparator: must implement compare

public class Player implements Comparable<Player>, Comparator<Player>  {

	private String name;
	private float rating;


	Player()
	{

	}


	Player(String name, float rating)
	{
		this.name = name;
		this.rating = rating;
	}


	String getName()
	{
		return name;
	}


	float getRating()
	{
		return rating;
	}


	@Override 
	public int compareTo(Player player) 
	{
		return (this.name).compareTo(player.name);   // by name
	}

	
	@Override 
	public int compare(Player player, Player player2)   // by rating
	{
		if (player.rating == player2.rating)
		{
			return player.name.compareTo(player2.name);
		}
		
		return player.rating < player2.rating ? -1 : 1;
	}	

}
