package images;

import javax.imageio.ImageIO;

public class ShowImageIOFormatNamesApp {

public static void main(String[] args)
{

    try {
        String[] formatNames = ImageIO.getWriterFormatNames();
        for (String formatName : formatNames)
        {
            System.out.println(formatName);
        }
    } catch (Exception e) {
        System.out.println("Exception occurred.");
    }
}


}