package images;

import javax.imageio.ImageIO;
import java.io.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

public class GifCreatorTestApp {

	private static final Color TRANSPARENT_COLOR = new Color(0xC0FFEE);
	// Important: This means that if we use the color 0xC0FFEE or a similar color anywhere, it will be made transparent.


	public static void main(String[] args)
	{

		buildIt2();
	}

	public static void buildIt2()
	{
		// GIF is definitely the file type that we need.

		final int widthInt = 200;
		final int heightInt = 60;

		try
		{
			BufferedImage bufferedImage = new BufferedImage(widthInt, heightInt, BufferedImage.TYPE_INT_ARGB);
			Graphics2D graphic = bufferedImage.createGraphics();

			// Color transparent = new Color(0, 0, 0, 0);
			// graphic.setBackground(transparent);

			// Attempt to set everything initially to transparent
			graphic.setComposite(AlphaComposite.SrcOver);
			int bg = TRANSPARENT_COLOR.getRGB();
			int[] data = ((DataBufferInt) bufferedImage.getRaster().getDataBuffer()).getData();
			for (int j = 0; j < data.length; j++) {
				data[j] = bg;
			}

			// Draw left and right vertical columns, alternating colors:
			int count = 0;
			for (int y = 0; y < heightInt; y++)
			{
				count++;

				if (count < 3)
				{
					graphic.setPaint(Color.black);
				}
				else
				{
					graphic.setPaint(Color.white);
				}
				graphic.fillRect(0, y, 2, 1);
				graphic.fillRect(widthInt - 2, y, 2, 1);

				if (count == 4)
				{
					count = 0;
				}
			}

			// Draw top and bottom horizontal rows, alternating colors:
				count = 0;
			for (int x = 0; x < widthInt; x++)
			{
				count++;

				if (count < 3)
				{
					graphic.setPaint(Color.white);
				}
				else
				{
					graphic.setPaint(Color.black);
				}
				graphic.fillRect(x, 0, 1, 2);
				graphic.fillRect(x, heightInt - 2, 1, 2);

				if (count == 4)
				{
					count = 0;
				}
			}

			// Set main portion of the image to be transparent:
				if (widthInt > 4 && heightInt > 4)
				{
					//                graphic.setComposite(AlphaComposite.Src);
					//                graphic.setPaint(TRANSPARENT_COLOR);
					//                graphic.fillRect(2, 2, widthInt - 4, heightInt - 4);

					// Color transparent = new Color(0, 0, 0, 0);
					// graphic.setColor(transparent);
					// graphic.setComposite(AlphaComposite.Src);
					// graphic.fillRect(2, 2, widthInt - 4, heightInt - 4);
					// graphic.clearRect(2, 2, widthInt - 4, heightInt - 4);
				}

				// int rgb = 0xFF00FF00; // green;
				// int rgb = 0x00000000; // hopefully transparent -- nope
				// Color transparent = new Color(0, 0, 0, 0);   // nope -- black
				// Color transparent = new Color(255, 255, 255, 255);   // nope -- white
				// int rgb = 0x00000000;  // perhaps transparent -- nope

				/*
            Color transparent = new Color(255, 255, 255, 0);   // -- nope, not transparent either

            // graphic.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER));

            for (int x = 50; x < 150; x++)
            {
                for (int y = 15; y < 50; y++)
                {
                    // try to take any color and make it transparent
                    // int transparentRGB = 0x00FFFFFF & Color.RED.getRGB();
                    bufferedImage.setRGB(x, y, transparent.getRGB());
                }
            }
				 */


				AnimatedGifEncoder e = new AnimatedGifEncoder();
				e.start("C:\\Users\\steve\\Documents\\GifCreatorTestAGE.gif");
				// e.start(put an OutputStream here);
				e.setDelay(0xC0FFEE); // still image; delay between frames shouldn't matter
				e.setTransparent(TRANSPARENT_COLOR);
				e.addFrame(bufferedImage);
				e.finish();

				// The first line below is obsolete; I don't know about the second.
				ImageIO.write(bufferedImage, "gif", new File("C:\\Users\\steve\\Documents\\GifCreatorTest.gif"));
				bufferedImage.flush();


		}
		catch (IOException e)
		{
		}
	}


	public static void buildIt()
	{
		// GIF is definitely the file type that we need.

		final int widthInt = 200;
		final int heightInt = 60;

		try
		{
			BufferedImage bufferedImage = new BufferedImage(widthInt, heightInt, BufferedImage.TYPE_INT_ARGB);
			Graphics2D graphic = bufferedImage.createGraphics();

			// Color transparent = new Color(0, 0, 0, 0);
			// graphic.setBackground(transparent);

			// Attempt to set everything initially to transparent
			graphic.setComposite(AlphaComposite.SrcOver);
			int bg = TRANSPARENT_COLOR.getRGB();
			int[] data = ((DataBufferInt) bufferedImage.getRaster().getDataBuffer()).getData();
			for (int j = 0; j < data.length; j++) {
				data[j] = bg;
			}

			// Draw left and right vertical columns, alternating colors:
			int count = 0;
			for (int y = 0; y < heightInt; y++)
			{
				count++;

				if (count < 3)
				{
					graphic.setPaint(Color.black);
				}
				else
				{
					graphic.setPaint(Color.white);
				}
				graphic.fillRect(0, y, 2, 1);
				graphic.fillRect(widthInt - 2, y, 2, 1);

				if (count == 4)
				{
					count = 0;
				}
			}

			// Draw top and bottom horizontal rows, alternating colors:
				count = 0;
			for (int x = 0; x < widthInt; x++)
			{
				count++;

				if (count < 3)
				{
					graphic.setPaint(Color.white);
				}
				else
				{
					graphic.setPaint(Color.black);
				}
				graphic.fillRect(x, 0, 1, 2);
				graphic.fillRect(x, heightInt - 2, 1, 2);

				if (count == 4)
				{
					count = 0;
				}
			}

			// Set main portion of the image to be transparent:
				if (widthInt > 4 && heightInt > 4)
				{
					//                graphic.setComposite(AlphaComposite.Src);
					//                graphic.setPaint(TRANSPARENT_COLOR);
					//                graphic.fillRect(2, 2, widthInt - 4, heightInt - 4);

					// Color transparent = new Color(0, 0, 0, 0);
					// graphic.setColor(transparent);
					// graphic.setComposite(AlphaComposite.Src);
					// graphic.fillRect(2, 2, widthInt - 4, heightInt - 4);
					// graphic.clearRect(2, 2, widthInt - 4, heightInt - 4);
				}

				// int rgb = 0xFF00FF00; // green;
				// int rgb = 0x00000000; // hopefully transparent -- nope
				// Color transparent = new Color(0, 0, 0, 0);   // nope -- black
				// Color transparent = new Color(255, 255, 255, 255);   // nope -- white
				// int rgb = 0x00000000;  // perhaps transparent -- nope

				/*
            Color transparent = new Color(255, 255, 255, 0);   // -- nope, not transparent either

            // graphic.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER));

            for (int x = 50; x < 150; x++)
            {
                for (int y = 15; y < 50; y++)
                {
                    // try to take any color and make it transparent
                    // int transparentRGB = 0x00FFFFFF & Color.RED.getRGB();
                    bufferedImage.setRGB(x, y, transparent.getRGB());
                }
            }
				 */



				ByteArrayOutputStream baos = new ByteArrayOutputStream();

				ImageIO.write(bufferedImage, "gif", baos);
				bufferedImage.flush();
				baos.flush();

				byte[] imageData;
				imageData = baos.toByteArray();


		}
		catch (IOException e)
		{
		}

	}


}