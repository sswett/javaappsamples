package email;
// Note: this requires downloading the  JavaMail API from: https://java.net/projects/javamail/pages/Home

// This code didn't work.


import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

public class MailWithPasswordAuthentication
{
	public static void main(String[] args) throws MessagingException
    {
		new MailWithPasswordAuthentication().run();
	}

	private void run() throws MessagingException
    {
		Message message = new MimeMessage(getSession());

		message.addRecipient(RecipientType.TO, new InternetAddress("steve.swett@comcast.net"));
		message.addFrom(new InternetAddress[] { new InternetAddress("steve.swett@comcast.net") });

		message.setSubject("the subject");
		message.setContent("the body", "text/plain");

		Transport.send(message);
	}

	private Session getSession()
    {
		Authenticator authenticator = new Authenticator();

		Properties properties = new Properties();

        // Added the following 4 lines:
        properties.setProperty("mail.smtp.starttls.enable", "true");
        // properties.setProperty("mail.smtp.socketFactory.port", d_port);
        properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.smtp.socketFactory.fallback", "false");

		properties.setProperty("mail.smtp.submitter", authenticator.getPasswordAuthentication().getUserName());
		properties.setProperty("mail.smtp.auth", "true");

		properties.setProperty("mail.smtp.host", "smtp.comcast.net");
		properties.setProperty("mail.smtp.port", "587");

		return Session.getInstance(properties, authenticator);
	}

	private class Authenticator extends javax.mail.Authenticator
    {
		private PasswordAuthentication authentication;

		public Authenticator()
        {
			String username = "steve.swett";
			String password = "com3cast4";
			authentication = new PasswordAuthentication(username, password);
		}

		protected PasswordAuthentication getPasswordAuthentication()
        {
			return authentication;
		}
	}
}
