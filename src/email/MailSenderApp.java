package email;
// Note: this requires downloading the  JavaMail API from: https://java.net/projects/javamail/pages/Home

// This code WORKS!


import java.util.*;
import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.*;
import javax.mail.internet.*;

public class MailSenderApp
{

    public static void main(String[] args)
    {
        final String username="steve.swett";
        final String password="com3cast4";
        Properties prop=new Properties();
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.host", "smtp.comcast.net");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getDefaultInstance(prop,
                new javax.mail.Authenticator()
                {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try
        {
            // String htmlBody = "<strong>This is an HTML Message</strong>";
            String textBody = "From Steve Swett: this is an e-mail generated text message.  " +
                    "If you need to reply click here: sms:6162042664 .";

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("steve.swett@comcast.net"));

            // Try sending from somebody else (even on a different domain):
            // message.setFrom(new InternetAddress("gardenpeg@gmail.com"));   // Yeah, this works!

            // message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("steve.swett@comcast.net"));
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("6162042664@text.att.net"));

            // message.setSubject("Testing Subject");
            MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
            mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
            mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
            mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
            mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
            mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
            CommandMap.setDefaultCommandMap(mc);
            // message.setText(htmlBody);
            message.setContent(textBody, "text/html");
            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e)
        {
            e.printStackTrace();
        }

    }

}