package email;
// Note: this requires downloading the  JavaMail API from: https://java.net/projects/javamail/pages/Home

// This code didn't work.

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class SendEmailApp
{
   public static void main(String [] args)
   {    
      // Recipient's email ID needs to be mentioned.
      String to = "steve.swett@comcast.net";

      // Sender's email ID needs to be mentioned
      String from = "steve.swett@comcast.net";

      // Assuming you are sending email from localhost
      String host = "smtp.comcast.net";

      // Get system properties
      Properties properties = System.getProperties();

      // Setup mail server
      properties.setProperty("mail.smtp.host", host);

      // Get the default Session object.
      Session session = Session.getDefaultInstance(properties);

      try{
         // Create a default MimeMessage object.
         MimeMessage message = new MimeMessage(session);

         // Set From: header field of the header.
         message.setFrom(new InternetAddress(from));

         // Set To: header field of the header.
         message.addRecipient(Message.RecipientType.TO,
                                  new InternetAddress(to));

         // Set Subject: header field
         message.setSubject("This is the Subject Line!");

         // Now set the actual message
         message.setText("This is actual message");

         // Send message
         Transport.send(message);
         System.out.println("Sent message successfully....");
      }catch (MessagingException mex) {
         mex.printStackTrace();
      }
   }
}