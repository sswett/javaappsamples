package regex;

public class TestRegexFindWholeWordApp
{
    final static String[] phrases =
            {
                    "demons are one thing",
                    "demo school",
                    "school that is a demo",
                    "this is a demo too",
                    "what a demonstration",
                    "that one is a real demon",
                    "tests are one thing",
                    "test school",
                    "school that is a test",
                    "this is a test too",
                    "what a testimony",
                    "that one is a real tester"
            };


    public static void main(String[] args)
    {
    	System.out.println("Find whole word occurrences of \"demo\" or \"test\"\n");
    	
        for (String phrase : phrases)
        {
            boolean found = phrase.matches("(.*)(\\bdemo\\b|\\btest\\b)(.*)");

            System.out.println("for phrase " + phrase + ", found = " + found);
        }
    }
}
