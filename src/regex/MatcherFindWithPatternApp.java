package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatcherFindWithPatternApp {

	public static void main(String[] args) {

		// Unlike "someString.matches", a matcher can find more than one substring within a String.
		
		String bigString =
				"M Tanner 22\n" +
				"M Andy 20\n" +
				"M Steve 51\n" +
				"M Billy 2x is a big boy\n" +
				"F Peggy 50";
		
		System.out.printf("Big string:\n%s\n\n", bigString);
		
		// If a regex is going to be used more than once, it is more efficient to compile it.
		
		// Should pick Tanner and Andy
		Pattern regex = Pattern.compile("M\\s.*\\s2\\d");   // . is required in order to match when \n is present in bigString   
		
		System.out.printf("regex: %s\n\n", regex);
		
		// Run the matcher:
		Matcher matcher = regex.matcher(bigString);

		while (matcher.find())
		{
			System.out.printf("find: %s, start: %s, end: %s\n", matcher.group(), matcher.start(), matcher.end());
		}
		
		// Alternative way:

		/*
		while (matcher.find())
		{
			for (int x = 0; x <= matcher.groupCount(); x++)
			{
				System.out.printf("find: %s, group: %s\n", matcher.group(x), x);
			}
		}
		*/
		
		System.out.println("\nTest 3x 'lookingAt':\n");
		
		for (int x = 0; x < 3; x++)
		{
			// Looking at will only return true if very beginning of bigString is a match (Tanner IS a match)
			
			if (matcher.lookingAt())
			{
				System.out.printf("lookingAt: %s, start: %s, end: %s\n", matcher.group(), matcher.start(), matcher.end());
			}
		}
		
	}

}
