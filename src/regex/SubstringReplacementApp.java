package regex;

public class SubstringReplacementApp {
	
	
	private static String s = "Once upon a time in 2015 there was a sentence with a few special characters in it: ***";

	
	public static void main(String[] args) {

		printStringAndAction("original");
		
		s = s.replaceFirst("\\w+", "Twice");   // The \w character will continue to be found until a non-word character is reached
		printStringAndAction("replaced first word");
			
		s = s.replaceFirst("\\*", "-");
		printStringAndAction("replaced first *");
		
		s = s.replaceAll("\\*", "-");
		printStringAndAction("replaced all *");
		
		s = s.replaceAll("\\s", "_");
		printStringAndAction("replaced all whitespace");
		
		s = s.replaceAll("_", " ");
		printStringAndAction("returned all whitespace");
		
		s = s.replaceAll("\\d{3}", "999");
		printStringAndAction("replaced all 3-digit sequences");
		
		s = s.replaceAll("\\sa\\s", " a[n] ");
		printStringAndAction("replaced all 'a' with 'a[n]'");
	}
	
	
	private static void printStringAndAction(String actionTaken)
	{
		System.out.printf("%s (%s)\n", s, actionTaken);
	}

}
