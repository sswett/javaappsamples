package regex;

public class ValidationTestsApp {
	
	
	// First index is "row", second is "column"  
	// (Could also have done a List of String[] but did it this way for fun and literal construction.) 
	
	private static final String[][] addresses = {
		
			// Each of the inner arrays has this format:
		
			//	First Name, Last Name, Address, City, State, Zip, Phone
			
			{
				"Steve", "O'Donnell", "123 Some Street", "Belmont", "MI", "49306", "616-555-3664"
			}
			,
			{
				"John", "[Doe", "367 Hollywood Blvd", "Grand Rapids", "MI", "49305", "616-555-4862"
			}
			,
			{
				"Jane", "4Doe", "3487 Mulholland Dr", "Kentwood", "Michigan", "49305", "616-555-4862"
			}
			,
			{
				"Max", "O'-Headroom", "55 Pineapple Way", "Plainfield", "MO", "68215", "888-055-1444"
			}
			,
			{
				"Mary", "O'BoyO'Boy", "874 Main Street", "Onaway", "Mich", "49765", "088-555-2234"
			}
			,
			{
				"Spacey", "O Spaceman", "54 Broadway", "Salt Lake City", "Utah", "84117", "801-467-1983"
			}
			
	};
			

	public static void main(String[] args) {

		for (int r = 0; r < addresses.length; r++)
		{
			for (int c = 0; c < 7; c++)
			{
				validate(addresses[r][c], c);
			}
			
			System.out.println();
		}
	}
	
	
	private static void validate(String data, int colNo)
	{
		boolean isOk = false;
		
		switch (colNo)
		{
		case 0: 
			isOk =	ValidationUtils.isFirstNameValid(data);
			break;
		case 1: 
			isOk = ValidationUtils.isLastNameValid(data);
			break;
		case 2: 
			isOk = ValidationUtils.isAddressValid(data);
			break;
		case 3: 
			isOk = ValidationUtils.isCityValid(data);
			break;
		case 4: 
			isOk = ValidationUtils.isStateValid(data);
			break;
		case 5: 
			isOk = ValidationUtils.isZipValid(data);
			break;
		case 6: 
			isOk = ValidationUtils.isPhoneValid(data);
			break;
		default: 
			isOk = false;
			break;
		}
		
		// System.out.println(String.format("%s is %svalid.", data, isOk ? "" : "in" ));
		
		// "printf" is shorter way than above.

		System.out.printf("%s is %svalid.\n", data, isOk ? "" : "in" );
	
	}

}
