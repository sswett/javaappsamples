package regex;

public class ValidationUtils {
	
	
	/*
		For more regex stuff, including an online regex tester, see http://www.rubular.com/

 		Character classes (that represent a type of single character):
 		
 		\d = any digit
 		\w = any word character (letter, digit, underscore)
 		\s = any white space character
 		\D = any non-digit
 		\W = any non-word character
 		\S = any non-white space character
 		.  = any single character
 
 
 		Stuff within brackets [] represents a single character
 		
 		Quantifiers:
 		
 		* = matches zero or more occurrences of the pattern
 		+ = matches one or more ...
 		? = matches zero or one ...
 		{n} = matches exactly n occurrences
 		{n,} = matches at least n occurrences
 		{n,m} = matches between n and m (inclusive) occurrences
 		
 		When ranges are used, the integer representation of the ASCII code is used for range selection. [A-z], therefore, includes not just letters.
 		
 		| = or
 		^ = when in brackets, matches any character OTHER THAN those listed
 
	 */
	
	
	public static boolean isFirstNameValid(String firstName)
	{
		// With "matches" the entire String must conform to the regular expression in order for true to be returned.
		
		return firstName.matches("[A-Z][a-zA-Z]*");
		
		// First letter must be upper case.
		// * indicates all remaining characters must each be [a-zA-Z] (upper/lower case letters)
	}
	
	
	public static boolean isLastNameValid(String lastName)
	{
		return lastName.matches("[a-zA-z]+([ '-][a-zA-Z]+)*");
		
		// Beginning of name must be one or more of a-z or A-z (int 65 - 122 -- which includes [ \ ] ^ _ ,
		// * indicates that the entire pattern within () may be repeated 0 or more times.  Call this the "inner" pattern. 
		// Examples of good inner patterns: "'Malley", " der Weighe", "-Jones"
	}
	
	
	public static boolean isAddressValid(String address)
	{
		return address.matches("\\d+\\s+([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)");
		
		// One or more digits
		// Followed by one or more white space characters
		// Followed by an "inner" pattern within the ()
		//   Inner pattern:
		//      one or more mixed case letters
		//      or "one or more mixed case letters + a white space character + one or more mixed case letters
	}
	
	
	public static boolean isCityValid(String city)
	{
		return city.matches("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)");
		
		// This is the same "inner" pattern as described above for address.
	}
	
	
	public static boolean isStateValid(String state)
	{
		return state.matches("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)");
		
		// This is the same pattern as the city
	}
	
	
	public static boolean isZipValid(String zip)
	{
		return zip.matches("\\d{5}");
		
		// Exactly 5 digits
	}
	
	
	public static boolean isPhoneValid(String phone)
	{
		return phone.matches("[1-9]\\d{2}-[1-9]\\d{2}-\\d{4}");
		
		// Format is: 123-456-7890
		// In "123" part, can't start with 0
		// in "456" part, can't start with 0
		// Hyphens required
	}

}
