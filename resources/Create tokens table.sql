create table tokens 
(
id integer not null,
player_id integer not null,
token_digest varchar not null,
primary key(id),
foreign key(player_id) references dadapp_person(id)
)
