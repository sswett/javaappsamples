# Intro #

This repository holds a rather miscellaneous collection of small Java applications written by Steve Swett over the years.  Many are small, exploratory, tinkering apps.  Some are solutions to small puzzles or coding challenges.  The source code is organized into categorical packages.

Steve has written many more apps and classes over the years, but those cannot be shared because they are intellectual property of former employers.

This repository is not intended for collaborative work or improvement.